﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace DoConnect.Models.Enum
{
    public class EnumModel
    {
        public string Value { get; set; }

        public string Description { get; set; }
    }
}