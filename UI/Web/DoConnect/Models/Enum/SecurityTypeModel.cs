﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DoConnect.BL.Entities.SecurityData;

namespace DoConnect.Models.Enum
{
    public class SecurityTypeModel
    {
        private PrivilegeType _priv;

        public SecurityTypeModel(PrivilegeType priv)
        {
            _priv = priv;
        }

        public int OrdinalValue { get { return (int)_priv; } }

        public string Name
        {
            get { return _priv.ToString(); }
        }
    }
}