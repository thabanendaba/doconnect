﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DoConnect.BL.Entities.Types;

namespace DoConnect.Models.Inventory
{
    public class InventoryViewModel
    {

        public long Id { get; set; }

        public long UserIdentityId { get; set; }

        public string Name { get; set; }

        public double Quantity { get; set; }

        public DateTime DateStamp { get; set; }


        public InvenoryType InvenoryType { get; set; }
    }
}