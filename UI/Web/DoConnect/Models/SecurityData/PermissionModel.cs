﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DoConnect.BL.Entities.SecurityData;
using EXO.Lib.Utility;

namespace DoConnect.Models.SecurityData
{
    public class PermissionModel
    {
        public PrivilegeType Privilege { get; set; }

        public string Description
        {
            get { return NameSplitting.SplitCamelCase(Privilege); }
        }

        [Display(Name = "Selected")]
        public bool Selected { get; set; }

    }
}