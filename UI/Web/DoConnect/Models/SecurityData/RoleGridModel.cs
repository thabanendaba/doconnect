﻿using DoConnect.BL.Entities.SecurityData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DoConnect.BL.Types;
using EXO.Lib.Utility;

namespace DoConnect.Models.SecurityData
{
    public class RoleGridModel
    {

        public long? Id { get; set; }

   
        public string RoleName { get; set; }

        public string Description { get; set; }

        public StatusType StatusTypes { get; set; }

        public string Status { get { return NameSplitting.SplitCamelCase(StatusTypes); } }

    }
}