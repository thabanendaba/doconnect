﻿using DoConnect.BL.Entities.SecurityData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DoConnect.BL.Types;
using EXO.Lib.Utility;

namespace DoConnect.Models.SecurityData
{
    public class RoleViewModel
    {
        public long? Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string RoleName { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        public StatusType StatusType { get; set; }

        public string Status
        {
            get
            {
                return StatusType.ToString();
            }
        }


        public List<PermissionModel> Permissions { get; set; }

    }
}