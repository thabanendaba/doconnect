﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoConnect.Models.SecurityData
{
    public class UserRoleModel
    {

        public long RoleId { get; set; }

        public string RoleName { get; set; }

        [Display(Name = "Selected")]
        public bool Selected { get; set; }

    }
}