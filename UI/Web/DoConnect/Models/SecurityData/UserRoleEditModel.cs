﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DoConnect.BL.Entities.SecurityData;

namespace DoConnect.Models.SecurityData
{
    public class UserEditModel
    {
        public long? UserId { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Telephone { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public string Title { get; set; }
                    
        public string IdOrPassportNumber { get; set; }
         
        public GenderType Gender { get; set; }
          
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }
        
        public bool IsAdmin { get; set; }

        public List<UserRoleModel> RoleList { get; set; }
    }

    
}