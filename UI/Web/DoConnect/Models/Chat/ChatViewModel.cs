﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace DoConnect.Models.Chat
{
    public class ChatViewModel
    {
        public long SenderId { get; set; }

        [Required]
        public string Subject { get; set; }

        public long? RecieverId { get; set; }

        public string UserName { get; set; }

        public DateTime DateTime { get; set; }
    }
}