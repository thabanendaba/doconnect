﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoConnect.Models.Chat
{
    public class ChatRequestResponse
    {
        public bool AcceptChat { get; internal set; }
        public long RecieverId { get; internal set; }
        public long SendUserId { get; internal set; }
    }
}