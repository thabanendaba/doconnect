﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoConnect.Models.Chat
{
    public class ChatMessageViewModel
    {

        public long? Id { get; set; }

        [Required]
        public long DoctorId { get; set; }

        [Required]
        public long patientId { get; set; }

        [Required]
        public long ChatId { get; set; }
    }
}