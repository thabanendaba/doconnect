﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DoConnect.BL.Entities.Types;

namespace DoConnect.Models.Chat
{
    public class NotificationModel
    {
        public long? SendUserId { get; set; }

        public string SenderName { get; set; }

        public string NotificationMessage { get; set; }

        public long? RecieverId { get; set; }

        public string ReceiverName { get; set; }

        public NotificationType NotificationType { get; set; }

        public string NotificationTypeStr
        {
            get
            {
                return NotificationType.ToString();
            }
        }
    }
}