﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DoConnect.BL.Entities.SecurityData;

namespace DoConnect.Models.Account
{
    public class ProfileViewModel
    {
        public long? UserId { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string UserName { get; set; }

        
        public GenderType GenderType { get; set; }

        [Required]
        public string Gender
        {
            get
            {
                return GenderType.ToString();
            }
        }

        [Required]
        public string IdPassportNum { get; set; }

        [Required]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "Telephone")]
        public string Telephone { get; set; }

        [Required]
        public string PostalCode { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }


    }
}