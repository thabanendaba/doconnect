﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DoConnect.BL.Entities.SecurityData;

namespace DoConnect.Models.Account
{
    public class DoctorIdentityGridModel
    {
        public long? Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Telephone { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public string Title { get; set; }

        public string IdOrPassportNumber { get; set; }

        public GenderType Gender { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }
    }
}