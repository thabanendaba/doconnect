﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoConnect.Models.Doctor
{
    public class PatientChatResponseModel
    {
        public long DoctorUserIdentityId { get; set; }
        public bool AcceptChat { get; set; }
    }
}