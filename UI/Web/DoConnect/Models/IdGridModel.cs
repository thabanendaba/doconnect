﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoConnect.Models
{
    public class IdGridModel
    {
        public int? Id { get; set; }

        public GridModel GridModel { get; set; }
    }
}