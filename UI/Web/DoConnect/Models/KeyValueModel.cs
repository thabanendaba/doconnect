﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoConnect.Models
{
    public class KeyValueModel
    {
        public long Id { get; set; }

        public string Description { get; set; }
    }
}