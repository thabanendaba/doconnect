﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DoConnect.BL.Entities.MasterData;
using EXO.Lib.Utility;

namespace DoConnect.Models.PatientBiometric
{
    public class MyBiometricGrid
    {
        public long BiometricId { get; set; }

        public string BiometricName { get; set; }

        public decimal? MinValue { get; set; }

        public decimal? MaxValue { get; set; }

        public bool TriggerAlert { get; set; }

        public string BiometricValue
        {
            get
            {
                if (NumericValue != null)
                    return NumericValue.Value.ToString("###,###.00");

                if(YesNoValue != null)
                    return YesNoValue.Value ? "Yes" : "No";

                if (StringValue != null)
                    return StringValue.FirstWords(5);

                return null;
            }
        }

        public decimal? NumericValue { get; set; }

        public string StringValue { get; set; }

        public bool? YesNoValue { get; set; }

        public BiometricType BiometricType { get; set; }

        public string BiometricTypeStr
        {
            get
            {
                return BiometricType.ToString();
            }
        }
    }
}