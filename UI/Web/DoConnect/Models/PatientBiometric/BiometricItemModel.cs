﻿using DoConnect.BL.Entities.PatientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DoConnect.BL.Entities.MasterData;

namespace DoConnect.Models.PatientBiometric
{
    public class BiometricItemModel
    {

        public long BiometricId { get; set; }

        public string BiometricName { get; set; }

        public decimal? MinValue { get; set; }

        public decimal? MaxValue { get; set; }

        public bool TriggerAlert { get;set; }

        public decimal? NumericValue { get; set; }

        public string StringValue { get; set; }

        public bool? YesNoValue { get; set; }

        public BiometricType BiometricType { get; set; }
        public string BiometricTypeStr
        {
            get
            {
                return BiometricType.ToString();
            }
        }
    }
}