﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DoConnect.BL.Entities.SecurityData;

namespace DoConnect.Models.PatientBiometric
{
    public class PatientsWithProblemsGridModel
    {
        public long? UserIdentityId { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Telephone { get; set; }

        public string EmailAddress { get; set; }

        public DateTime LastUpdate { get;  set; }
    }
}