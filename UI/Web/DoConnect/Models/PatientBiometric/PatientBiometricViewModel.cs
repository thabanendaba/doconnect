﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoConnect.Models.PatientBiometric
{
    public class PatientBiometricViewModel
    {
        public long UserIdentityId { get; set; }

        public List<BiometricItemModel> Items { get; set; }
    }
}