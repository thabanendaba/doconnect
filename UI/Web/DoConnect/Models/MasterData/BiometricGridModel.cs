﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DoConnect.BL.Entities.MasterData;

namespace DoConnect.Models.MasterData
{
    public class BiometricGridModel
    {
        public long Id { get; set; }
        
        public string Name { get; set; }

        public decimal? MaxValue { get; set; }

        public decimal? MinValue { get; set; }

        public bool TriggerAlert { get; set; }

        public BiometricType BiometricType { get; set; }

        public string BiometricTypeStr
        {
            get
            {
                return BiometricType.ToString();
            }
        }
    }
}