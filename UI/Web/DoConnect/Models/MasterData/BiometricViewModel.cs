﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DoConnect.BL.Entities.MasterData;

namespace DoConnect.Models.MasterData
{
    public class BiometricViewModel
    {
        public long? Id { get; set; }

        [MaxLength(200)]
        [Required]
        public string Name { get; set; }

        public decimal? MaxValue { get; set; }

        public decimal? MinValue { get; set; }

        public bool TriggerAlert { get; set; }

        public BiometricType BiometricType { get; set; }

    }
}