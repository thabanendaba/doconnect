﻿AngularApp.factory('PatientBiometricService',
    ['$http',
    function ($http) {
        var patientBiometricGet = function (userIdentityId) {
            return $http.get("/PatientBiometric/PatientBiometricGet/" + userIdentityId);
        };


        var biometricGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("/Biometric/MyBiometricGrid", req);
        }

        var patientbiometricGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor, userIdentityId) {
            var format = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            var req = {
                Id: userIdentityId,
                GridModel: format
            }
            return $http.post("/PatientBiometric/GridPatients", req);
        }

        var patientBiometricSave = function (userIdentityId, patientBiometrics)
        {
            var items = [];
            for (x = 0; x < patientBiometrics.length; x++) {
                var item = {
                    BiometricId:   patientBiometrics[x].biometricId,
                    BiometricName: patientBiometrics[x].biometricName,
                    MinValue: patientBiometrics[x].minValue,
                    MaxValue: patientBiometrics[x].maxValue,
                    BiometricType: patientBiometrics[x].biometricType,
                    NumericValue: patientBiometrics[x].numericValue,
                    StringValue: patientBiometrics[x].stringValue,
                    YesNoValue: patientBiometrics[x].yesNoValue
                }
                items.push(item);
            }
            return $http.post("/PatientBiometric/PatientBiometricSave", {
                UserIdentityId: userIdentityId,
                Items: items
            });
        };

        return {
            PatientBiometricSave: patientBiometricSave,
            PatientBiometricGet: patientBiometricGet,
            BiometricGrid: biometricGrid,
            GridPatients: patientbiometricGrid
        }
    }]);

