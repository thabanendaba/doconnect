﻿AngularApp.factory('EnumsService',
  ['$http', '$rootScope', function ($http, $rootScope) {

       //status type
       var _statusType = [];

       var loadStatusType = function () {
           return $http.get("/Enums/StatusTypeEnum").then(function (result) {
               _statusType = result.data;
               checkAllEnumsLoaded();
           });
       };

       var getStatusTypes = function () {
           return _statusType;
       }
     
       loadStatusType();

       //gender type
       var _genderType = [];

       var loadGenderType = function () {
           return $http.get("/Enums/GenderTypeEnum").then(function (result) {
               _genderType = result.data;
               checkAllEnumsLoaded();
           });
       };

       var getGenderTypes = function () {
           return _genderType;
       }

       loadGenderType();

       //Biometric Type 
       var _biometricType = [];

       var loadBiometricType = function () {
           return $http.get("/Enums/BiometricTypeEnum").then(function (result) {
               _biometricType = result.data;
               checkAllEnumsLoaded();
           });
       };

       var getBiometricTypes = function () {
           return _biometricType;
       }

       loadBiometricType();

      //Notification Type 
       var _notificationType = [];

       var loadNotificationType = function () {
           return $http.get("/Enums/NotificationTypeEnum").then(function (result) {
               _notificationType = result.data;
               checkAllEnumsLoaded();
           });
       };

       var getNotificationTypes = function () {
           return _notificationType;
       }

       loadNotificationType();


      //UserTypes Type 
       var _userType = [];

       var loadUserType = function () {
           return $http.get("/Enums/UserTypeEnum").then(function (result) {
               _userType = result.data;
               checkAllEnumsLoaded();
           });
       };

       var getUserTypes = function () {
           return _userType;
       }

       loadUserType();

         //securites
       var _securityType = [];

       var loadSecurityType = function () {
           return $http.get("/Enums/SecurityEnum").then(function (result) {
               _securityType = result.data;
               checkAllEnumsLoaded();
           });
       };

       var getSecurityTypes = function () {
           return _securityType;
       }

       loadSecurityType();



       var _enumsLoadedEvent = 'enums-loaded';
       var _allEnumsLoaded = false;
       var checkAllEnumsLoaded = function () {
           if (_statusType.length > 0
               && _genderType.length > 0
               && _biometricType.length > 0
               && _notificationType.length > 0
               && _userType.length > 0
               && _securityType.length > 0) {
               _allEnumsLoaded = true;
               $rootScope.$emit(_enumsLoadedEvent, _allEnumsLoaded);
           }
       };

    

       var _getAllEnumsLoaded = function () {
           return _allEnumsLoaded;
       }
       
       //interface
       return {
           statusTypes: getStatusTypes,
           genderTypes: getGenderTypes,
           biometricTypes: getBiometricTypes,
           notificationTypes: getNotificationTypes,
           userTypes: getUserTypes,
           securityTypes: getSecurityTypes,
           enumsLoadedEvent: _enumsLoadedEvent,
           enumsLoaded: _getAllEnumsLoaded
       }


   }
]);