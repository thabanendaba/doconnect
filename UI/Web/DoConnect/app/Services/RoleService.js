﻿AngularApp.factory('RoleService',
    ['$http',
    function ($http) {
        var roleSave = function (id, roleName, description, status, privs) {
            var permissions = [];

            for (x = 0; x < privs.length; x++)
            {
                if(privs[x].selected)
                {
                    var permission = {
                        Privilege: privs[x].privilege,
                        Selected: privs[x].selected
                    }

                    permissions.push(permission);
                }
            }

            return $http.post("/Role/SaveRole", {
                Id: id,
                RoleName: roleName,
                Description: description,
                StatusType: status,
                Permissions: permissions
            });
        };
        
        var roleList = function () {
            return $http.get("/Role/RoleList");
        };


        var roleGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("/Role/RoleGrid", req);
        }

        var roleGet = function (roleId) {
            return $http.get("/Role/RoleGet/" + roleId);
        }

        return {
            RoleSave: roleSave,
            RoleList: roleList,
            RoleGrid: roleGrid,
            RoleGet: roleGet
        }

        
    }]);
