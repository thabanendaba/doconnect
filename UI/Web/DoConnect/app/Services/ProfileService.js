﻿AngularApp.factory('ProfileService',
    ['$http',
    function ($http) {

        var profileSave = function (userName, gender, idPassportNum, surname, firstName, title,
           emailAddress, telephone, postalCode, city, addressLine1, addressLine2) {

            return $http.post("/Profile/ProfileEdit", {
                UserName: userName,
                GenderType: gender,
                IdPassportNum: idPassportNum,
                Surname: surname,
                FirstName: firstName,
                Title: title,
                EmailAddress: emailAddress,
                Telephone: telephone,
                PostalCode: postalCode,
                City: city,
                AddressLine1: addressLine1,
                AddressLine2: addressLine2
            });
        };

        var doctorSave = function (userId, userName, gender, idPassportNum, surname, firstName, title,
           emailAddress, telephone, postalCode, city, addressLine1, addressLine2) {

            return $http.post("/Profile/DoctorEdit", {
                UserId: userId,
                UserName: userName,
                GenderType: gender,
                IdPassportNum: idPassportNum,
                Surname: surname,
                FirstName: firstName,
                Title: title,
                EmailAddress: emailAddress,
                Telephone: telephone,
                PostalCode: postalCode,
                City: city,
                AddressLine1: addressLine1,
                AddressLine2: addressLine2
            });
        };

        var getProfile = function () {
            return $http.get("/Profile/GetProfile");
        };

        var _doctorGet = function (userId) {
            return $http.get("/Doctor/GetDoctorProfile/" + userId);
        }

        return {
            ProfileSave: profileSave,
            GetProfile: getProfile,
            DoctorGet: _doctorGet,
            DoctorSave: doctorSave
        };

    }]);