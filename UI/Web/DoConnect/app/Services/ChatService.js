﻿AngularApp.factory('ChatService',
    ['$http', 'chat', '$uibModal', '$log', '$rootScope',
    function ($http, chat, $uibModal, $log, $rootScope) {

        var _chatMessages = [];

        var _sendChat = function (message) {
            chat.server.sendMessage(message);
        };

        var alert = new Audio();
        alert.src = "/Assets/Sounds/notification.mp3";

        var _onChatEvent = 'on-chat-event';
        chat.client.newMessage = function onNewMessage(message) {
            _chatMessages.push(message);
            alert.play();
            while (_chatMessages.length > 5) {
                _chatMessages.shift();
            }

            $rootScope.$emit(_onChatEvent, message);
        };

        var _doctorChatRequestModal = function (message) {
            var modalInstance = $modal.open({
                animation: false,
                templateUrl: 'app/Views/Chat/DoctorChatNotificationModal.html',
                controller: 'NotificationController',
                resolve: {
                    notification: function () {
                        return message;
                    }
                }
            });

            modalInstance.result.then(function (result) {
                $log.info(' button ok reached');
            }, function () {
                $log.info(' button cancel reached');
            });
        }

      
        var _onNewDoctorEvent = 'on-doctor-event';
        chat.client.newDoctor = function newDoctor(doctor) {
            $rootScope.$emit(_onNewDoctorEvent, doctor);
        };

        var _sendNotification = function (message) {
            notification.server.sendNotification(message);
        };

        var _onNotificationEvent = 'on-notification-event';
        chat.client.newNotification = function onNewNotification(message) {
            $rootScope.$emit(_onNotificationEvent, message);
        };

        var _onChatRequestResponseEvent = 'chat-Request-Response';
        chat.client.chatRequestResponse = function onChatRequestResponse(patientChatResponseModel) {

            _chatMessages = [];

            $rootScope.$emit(_onChatRequestResponseEvent, patientChatResponseModel);
        }
       
        var _StartHub = function () {
            $.connection.hub.start();
        }

        var _getDoctors = function () {
            return $http.get("/Chat/GetDoctors");
        };
        
        var _getAllChats = function () {
            return _chatMessages;
        }
      

        $.connection.hub.disconnected(function () {
            setTimeout(function () {
                $.connection.hub.start();
            }, 5000); // Restart connection after 5 seconds.
        });

        var _getSenderDisplayName = function (senderId) {
            return $http.get("/Doctor/GetSenderName/" + senderId);
        }

        return {
            SendChat: _sendChat,
            OnChatEvent: _onChatEvent,
            DoctorChatRequestModal: _doctorChatRequestModal,
            OnNewDoctorEvent: _onNewDoctorEvent,
            SendNotification: _sendNotification,
            OnNotificationEvent: _onNotificationEvent,
            StartHub: _StartHub,
            GetDoctors: _getDoctors,
            GetAllChats: _getAllChats,
            OnChatRequestResponseEvent: _onChatRequestResponseEvent,
            GetSenderDisplayName: _getSenderDisplayName
        };

    }]);