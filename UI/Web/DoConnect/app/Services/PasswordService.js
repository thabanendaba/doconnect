﻿AngularApp.factory('PasswordService',
    ['$http',
    function ($http) {

        var _changePassword = function (oldPassword, newPassword, confirmPassword) {
            return $http.post("Password/ChangePassword", {
                OldPassword: oldPassword,
                NewPassword: newPassword,
                ConfirmPassword: confirmPassword
            });
        };


        return {

            ChangePassword: _changePassword
    };
}]);