﻿AngularApp.factory('DoctorService',
    ['$http', '$log', '$modal',
    function ($http, $log, $modal) {


        var _registerDoctor = function (userName, genderType, idOrPassport, surname, firstName,
                                 title, email, telephone, password, confirmPassword, postalCode,
                                 city, addressLine1, addressLine2) {
            return $http.post("/Doctor/RegisterDoctor", {
                UserName: userName,
                GenderType: genderType,
                IDOrPassport: idOrPassport,
                Surname: surname,
                FirstName: firstName,
                Title: title,
                Email: email,
                Telephone: telephone,
                Password: password,
                ConfirmPassword: confirmPassword,
                PostalCode: postalCode,
                City: city,
                AddressLine1: addressLine1,
                AddressLine2: addressLine2
            });
        };


        var _doctorGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("/Doctor/DoctorListGrid", req);
        }

        var _patientsWithProblemsGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("/Doctor/PatientsWithProblemsGrid", req);
        }

        var _doctorGet = function (userId) {
            return $http.get("/Doctor/GetDoctorProfile/" + userId);
        }

        var _notifyPatient = function (userId) {
            var req = {
                UserIdentityId: userId
            };

            return $http.post("/Doctor/NotifyPatient", req);
        }

        var _NotificationResponse = function (doctorUserIdentityId, acceptChat) {
            var req = {
                DoctorUserIdentityId: doctorUserIdentityId,
                AcceptChat : acceptChat
            };
            return $http.post("/Doctor/NotificationResponse", req);
        }

        var _PatientNotificationResponse = function (doctorUserIdentityId, acceptChat) {
            var req = {
                DoctorUserIdentityId: doctorUserIdentityId,
                AcceptChat: acceptChat
            };
            return $http.post("/Doctor/PatientNotificationResponse", req);
        }




       
        var _openChatWaitingModal = function (userId) {
            var modalInstance = $modal.open({
                animation: false,
                templateUrl: 'app/Views/Chat/DoctorTimerModal.html',
                controller: 'NotificationController',
                resolve: {
                    notification: function () {
                        UserIdentityId: userId
                        return userId;
                    }
                }
            });

            modalInstance.result.then(function (result) {
                $log.info(' button ok reached');
            }, function () {
                $log.info(' button cancel reached');
            });
        }

        return {

            RegisterDoctor: _registerDoctor,
            DoctorGrid: _doctorGrid,
            DoctorGet: _doctorGet,
            PatientsWithProblemsGrid: _patientsWithProblemsGrid,
            NotifyPatient: _notifyPatient,
            NotificationResponse: _NotificationResponse,
            OpenChatWaitingModal: _openChatWaitingModal,
            PatientNotificationResponse: _PatientNotificationResponse
        }
    }
    ]);