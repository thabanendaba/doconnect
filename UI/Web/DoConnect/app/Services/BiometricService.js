﻿AngularApp.factory( 'BiometricService',
    ['$http',
    function ($http){

        var biometricSave = function (id, name, maxValue, minValue, triggerAlert, biometricType) {
            return $http.post("/Biometric/BiometricSave", {
                Id: id,
                Name: name,
                MaxValue: maxValue,
                MinValue: minValue,
                TriggerAlert: triggerAlert,
                BiometricType: biometricType
            });
        };

        var getBiometric = function (id) {
            return $http.get("/Biometric/GetBiometric/" + id);

        };

        var getAllBiometrics = function () {
            return $http.get("/Biometric/GetAllBiomterics");
        };

        var biometricGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            };
            return $http.post("/Biometric/BiometricGrid", req);
        }

        

        return {
            BiometricSave: biometricSave,
            GetBiometric: getBiometric,
            GetAllBiometrics: getAllBiometrics,
            BiometricGrid: biometricGrid
        };
    }

    ]);