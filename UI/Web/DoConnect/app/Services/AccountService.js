﻿AngularApp.factory('AccountService',
    ['$http',
    function ($http) {
        var login = function (userName, password, rememberMe) {
            return $http.post("/Account/Login", {
                UserName: userName,
                Password: password,
                RememberMe: rememberMe
            });
        };

        var register = function (userName, genderType, idOrPassport, surname,firstName,
                                 title,email,telephone, password, confirmPassword, postalCode,
                                 city,addressLine1, addressLine2) {
            return $http.post("/Account/Register", {
                            UserName : userName,
                            GenderType: genderType,
                            IDOrPassport:idOrPassport,
                            Surname: surname,
                            FirstName: firstName,
                            Title:title,
                            Email:email,
                            Telephone: telephone,
                            Password: password,
                            ConfirmPassword: confirmPassword,
                            PostalCode:postalCode,
                            City:city,
                            AddressLine1:addressLine1,
                            AddressLine2: addressLine2
            });
        };

        var currentUser = function () {
            return $http.get('/Account/GetCurrentUser');
        }

        var singOut = function () {
            return $http.post('/Account/SignOut', { Id: 1 });
        }

        var _panic = function(lat,lon){
            return $http.post('/Account/Panic', { Lat: lat,Lon:lon });
        }

        return {
            login: login,
            register: register,
            currentUser: currentUser,
            singOut: singOut,
            panic: _panic
        }

       
    }

    ]);

