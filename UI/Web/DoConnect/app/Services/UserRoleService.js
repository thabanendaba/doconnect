﻿AngularApp.factory('UserRoleService',
    ['$http',
    function ($http) {


        var _userRoleSave = function(id, userName, password, emailAddress, title, firstName, surname,
                                    idOrPassportNumber, gender, telephone, addressLine1, addressLine2, city,
                                    postalCode, roles, isAdmin)
        {
            var roleList = [];
            for (x = 0; x < roles.length; x++) {
                if (roles[x].selected) {
                    var role = {
                        RoleId: roles[x].roleId,
                        RoleName: roles[x].roleName,
                        Selected: roles[x].selected
                    }

                    roleList.push(role);
                }
            }
            return $http.post("/UserRole/UserRoleSave", {
                UserId: id,
                UserName: userName,
                Password: password,
                EmailAddress: emailAddress,
                Title: title,
                FirstName: firstName,
                Surname: surname,
                IdOrPassportNumber: idOrPassportNumber,
                Gender: gender,
                Telephone: telephone,
                AddressLine1: addressLine1,
                AddressLine2: addressLine2,
                City: city,
                PostalCode: postalCode,
                RoleList: roleList,
                IsAdmin: isAdmin
            });
        }

        var _userRoleList = function () {
            $http.get("/UserRole/UserRoleList");
        }

        var _userRoleGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("/UserRole/UserRoleGrid", req);
        }

        var _userRoleGet = function (userId) {
            return $http.get("/UserRole/UserRoleGet/" + userId);
        }

        return {

            UserRoleSave: _userRoleSave,
            UserRoleGrid: _userRoleGrid,
            UserRoleGet: _userRoleGet,
            UserRoleList: _userRoleList
            
        }
    }]);