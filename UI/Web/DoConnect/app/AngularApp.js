﻿var AngularApp = angular.module('AngularApp',
   ['ngRoute', 'ui.bootstrap', 'validation.match', 'ui.bootstrap.showErrors',
    'toaster', 'angular-loading-bar', 'ngAnimate']);

AngularApp.config(
    ['$routeProvider','$httpProvider', '$locationProvider',
    function ($routeProvider, $httpProvider, $locationProvider) {
        $locationProvider.hashPrefix('');

        $routeProvider
        .when("/", {
            templateUrl: "app/Views/Home/Home.html",
            controller: "HomeController"
        })
        .when("/home", {
            templateUrl: "app/Views/Home/Home.html",
            controller: "HomeController"
        })
        .when('/login', {
            templateUrl: 'app/Views/Account/Login.html',
            controller: 'LoginController'
        })
        .when('/register', {
            templateUrl: 'app/Views/Account/Register.html',
              controller: 'RegisterController'
        })
        .when('/biometric', {
            templateUrl: 'app/Views/Biometric/BiometricGrid.html',
            controller: 'BiometricController'
        })
        .when('/biometricAdd', {
            templateUrl: 'app/Views/Biometric/BiometricSave.html',
            controller: 'BiometricController'
        })
        .when('/biometricEdit/:id', {
            templateUrl: 'app/Views/Biometric/BiometricSave.html',
            controller: 'BiometricController'
        })
        .when('/roleEdit/:id', {
            templateUrl: 'app/Views/Security/SaveRole.html',
            controller: 'RoleController'
        })
        .when('/roleMaintenance', {
            templateUrl: 'app/Views/Security/RoleMaintenance.html',
            controller: 'RoleController'
        })
        .when('/userEdit/:id', {
            templateUrl: 'app/Views/Security/SaveUserRole.html',
            controller: 'UserRoleController'
        })
        .when('/userMaintenance', {
            templateUrl: 'app/Views/Security/UserMaintenance.html',
            controller: 'UserRoleController'
        })
        .when('/myProfile', {
            templateUrl: 'app/Views/Account/Profile.html',
            controller: 'ProfileController'
        })
        .when('/profile/:id', {
                templateUrl: 'app/Views/Account/Profile.html',
                controller: 'ProfileController'
            })
         .when('/patientBiometric/:id', {
             templateUrl: 'app/Views/Patient/PatientBiometricView.html',
             controller: 'PatientBiometricController'
                 
         })
         .when('/chat/:id', {
                templateUrl: 'app/Views/Chat/Chat.html',
                controller: 'ChatController'
         })
            .when('/doctorChat/:id', {
                templateUrl: 'app/Views/Chat/DoctorChat.html',
                controller: 'DoctorChatController'
            })
         .when('/newDoctor/:id', {
             templateUrl: 'app/Views/Account/DoctorSave.html',
                controller: 'DoctorController'
         })
           .when('/doctorMaintenance', {
               templateUrl: 'app/Views/Account/DoctorMaintenance.html',
               controller: 'DoctorController'
            })
         .when('/patientMaintenance', {
                templateUrl: 'app/Views/Security/PatientMaintenance.html',
                controller: 'UserRoleController'
         })
            .when('/doctorHome', {
                templateUrl: 'app/Views/Home/DoctorHome.html',
                controller: 'DoctorHomeController'
            })
            .when('/patientHome', {
                templateUrl: 'app/Views/Home/PatientHome.html',
                controller: 'PatientHomeController'
            })
            .when('/patientGrid/:id', {
                templateUrl: 'app/Views/Patient/PatientGrid.html',
                controller: 'PatientBiometricController'
            })
        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true);// it will remove the #

    $httpProvider.interceptors.push('AuthHttpResponseInterceptor');

        //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }

    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

    }]);




(function () {

    angular.module('AngularApp').value('chat', $.connection.chat);

    //setup SingalR
    $.connection.hub.logging = true;


    $.connection.hub.error(function (err) {
        console.log('An error occurred: ' + err);
    });


    //setup bootstrap menu auto collapse
   


})();
