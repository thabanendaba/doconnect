﻿AngularApp.controller('RoleController',
    ['$scope', '$location', '$window','$timeout','$routeParams', 'RoleService','EnumsService',
function ($scope, $location, $window, $timeout, $routeParams, RoleService, EnumsService) {

    $scope.roleModel = {
        id: null,
        roleName: '',
        description: '',
        status: '',
        permissions: ''
    }

    $scope.roleModel.id = $routeParams.id;

    if ($scope.roleModel.id > 0) {
        $scope.updateStatus = function () { };
    }

    RoleService.RoleGet($scope.roleModel.id).then(
        function (result) {


            $scope.roleModel.id = result.data.id;
            $scope.roleModel.roleName = result.data.roleName;
            $scope.roleModel.description = result.data.description;
            $scope.roleModel.status = result.data.status;
            $scope.roleModel.permissions = result.data.permissions;
        },

        function (error) {
            handleError(error);
        });

    $scope.statusTypes = EnumsService.statusTypes();

    $scope.successMessage = "Saved Successfully";

    $scope.submitForm = function () {
        $scope.$broadcast('show-errors-check-validity');

        if ($scope.NewRoleForm.$invalid)
            return;


        var result = RoleService.RoleSave($scope.roleModel.id,
                                          $scope.roleModel.roleName,
                                          $scope.roleModel.description,
                                          $scope.roleModel.status,
                                          $scope.roleModel.permissions);

        result.then(function (result) {


            $scope.handleSuccess = function () { };

            $timeout(function () { $window.history.back(); }, 2000);
            
        },
        function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.statusText;
        });
    };

    $scope.resetForm = function () {
        $scope.$broadcast('show-errors-reset');
    }

    $scope.cancelForm = function () {
        $scope.$broadcast('show-errors-reset');
        $window.history.back();
    }

    $scope.newRole = function () {
        $location.path("/roleEdit/0");
    };

    var handleError = function (error) {
        $scope.hasError = true;
        $scope.errorMessage = error.statusText;
    }

    $scope.sortKeyOrder = {
        key: 'roleName',
        order: 'ASC',
    };

    $scope.data = RoleService.roles;

    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.recordsPerPage = 20;
    $scope.numberOfPageButtons = 5;

    $scope.sort = function (col) {
        if ($scope.sortKeyOrder.key === col) {
            if ($scope.sortKeyOrder.order == 'ASC')
                $scope.sortKeyOrder.order = 'DESC';
            else
                $scope.sortKeyOrder.order = 'ASC';
        } else {
            $scope.sortKeyOrder.key = col;
            $scope.sortKeyOrder.order = 'ASC';
        }
        loadGrid();
    };

    $scope.searchFor = '';
    $scope.search = function () {
        loadGrid();
    }


    var loadGrid = function () {
        var searchFor = '';

        RoleService.RoleGrid($scope.currentPage, $scope.recordsPerPage,
                                  $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
            function (result) {
                $scope.roleGrid = result.data.results;
                $scope.totalItems = result.data.recordCount;
            },
            function (error) {
                alert("an error occured: unable to get data");
            });


    };


    $scope.pageChanged = function () {
        loadGrid();
    };




    loadGrid();


}]);