﻿AngularApp.controller('LoginController',
            ['$scope', '$location', 'AccountService', 'Security', 'EnumsService',
    function ($scope, $location, AccountService, Security, EnumsService) {

        $scope.loginModel = {
                        userName: '',
                        password: '',
                        rememberMe: false
                    };
       

        $scope.userTypes = EnumsService.userTypes();

        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.loginForm.$valid) {

                AccountService.login($scope.loginModel.userName,
                                     $scope.loginModel.password,
                                     $scope.loginModel.rememberMe).then(

                    function (result) {

                        Security.login(result.data.id, result.data.userName, result.data.displayName, result.data.allowedPrivileges, result.data.userTypeString);


                        if ($scope.userTypes[0].description == result.data.userTypeString) {
                            $location.path('/home');
                        }
                        else if ($scope.userTypes[1].description == result.data.userTypeString) {
                            $location.path('/doctorHome');
                        }
                        else if ($scope.userTypes[2].description == result.data.userTypeString) {
                            $location.path('/patientHome');
                        }

                        
                    },

                    function (error) {
                        $scope.hasError = true;
                        $scope.errorMessage = error.statusText;
                        Security.logout();
                    }

                    );
            }

        }

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }
    }

    ]);

