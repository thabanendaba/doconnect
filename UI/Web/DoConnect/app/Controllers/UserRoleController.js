﻿AngularApp.controller('UserRoleController',
    ['$scope', '$location', '$window', '$routeParams', 'UserRoleService', 'DoctorService',
    function ($scope, $location, $window, $routeParams, UserRoleService, DoctorService) {

        $scope.model = {
            id: null,
            userName: '',
            password: '', 
            emailAddress: '', 
            title: '', 
            firstName: '', 
            surname: '',
            idOrPassportNumber: '', 
            gender: '', 
            telephone: '', 
            addressLine1: '', 
            addressLine2: '', 
            city: '',
            postalCode: '', 
            roleList: '', 
            isAdmin: ''
        };

        $scope.model.id = $routeParams.id;

        if ($scope.model.id > 0) {
            UserRoleService.UserRoleGet($scope.model.id).then(
                function (result) {

                    $scope.model.id = result.data.id;
                    $scope.model.userName = result.data.userName;
                    $scope.model.password = result.data.password;
                    $scope.model.emailAddress = result.data.emailAddress;
                    $scope.model.title = result.data.title;
                    $scope.model.firstName = result.data.firstName;
                    $scope.model.surname = result.data.surname;
                    $scope.model.idOrPassportNumber = result.data.idOrPassportNumber;
                    $scope.model.gender = result.data.gender;
                    $scope.model.telephone = result.data.telephone;
                    $scope.model.addressLine1 = result.data.addressLine1;
                    $scope.model.addressLine2 = result.data.addressLine2;
                    $scope.model.city = result.data.city;
                    $scope.model.postalCode = result.data.postalCode;
                    $scope.model.roleList = result.data.roleList;
                    $scope.model.isAdmin = result.data.isAdmin;

                },

                function (error) {
                    handleError(error);
                });
        };

        

        $scope.saveUserForm = function () {
            $scope.$broadcast('show-errors-check-validity');

            if ($scope.UserForm.$invalid)
                return;


            var result = UserRoleService.UserRoleSave($scope.model.id, 
                                                       $scope.model.userName,
                                                       $scope.model.password,
                                                       $scope.model.emailAddress,
                                                       $scope.model.title,
                                                       $scope.model.firstName,
                                                       $scope.model.surname,
                                                       $scope.model.idOrPassportNumber,
                                                       $scope.model.gender,
                                                       $scope.model.telephone,
                                                       $scope.model.addressLine1,
                                                       $scope.model.addressLine2,
                                                       $scope.model.city,
                                                       $scope.model.postalCode,
                                                       $scope.model.roleList,
                                                       $scope.model.isAdmin);

            result.then(function (result) {
                $window.history.back();
            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.statusText;
            });


        }

        $scope.newUser = function () {
            $location.path("/userEdit/0");
        };


        $scope.chatRequest = function (n) {

            DoctorService.OpenChatWaitingModal(n);

            //var modalInstance = $modal.open({
            //    animation: false,
            //    templateUrl: 'app/Views/Chat/DoctorTimerModal.html',
            //    controller: 'NotificationController',
            //    resolve: {
            //        notification: function () {
            //            return n;
            //        }
            //    }
            //});

            //modalInstance.result.then(function (result) {
            //    $log.info(' button ok reached');
            //}, function () {
            //    $log.info(' button cancel reached');
            //});


            DoctorService.NotifyPatient(n).then(
                function (result) {
                    $scope.errorMessage = result.data;
                },
                function (error) {
                    handleError(error);
                });
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.statusText;
        }

        $scope.sortKeyOrder = {
            key: 'userName',
            order: 'ASC',
        };

        $scope.data = UserRoleService.UserRoleList;
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.maxSize = 5;
        $scope.recordsPerPage = 20;
        $scope.numberOfPageButtons = 5;

        $scope.sort = function (col) {
            if ($scope.sortKeyOrder.key === col) {
                if ($scope.sortKeyOrder.order == 'ASC')
                    $scope.sortKeyOrder.order = 'DESC';
                else
                    $scope.sortKeyOrder.order = 'ASC';
            } else {
                $scope.sortKeyOrder.key = col;
                $scope.sortKeyOrder.order = 'ASC';
            }
            loadGrid();
        };

        $scope.searchFor = '';
        $scope.search = function () {
            loadGrid();
        }


        
        var loadGrid = function () {
            UserRoleService.UserRoleGrid($scope.currentPage, $scope.recordsPerPage,
                                      $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                function (result) {
                    $scope.userRoleGrid = result.data.results;
                    $scope.totalItems = result.data.recordCount;
                },
                function (error) {
                    alert("an error occured: unable to get data");
                });
        };


        $scope.pageChanged = function () {
            loadGrid();
        };


        loadGrid();
    }
    
    ]);