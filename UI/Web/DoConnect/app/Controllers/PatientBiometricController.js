﻿AngularApp.controller('PatientBiometricController',
    ['$scope', '$location', '$window', '$timeout', '$routeParams', 'PatientBiometricService', 'Security',
    function ($scope, $location, $window, $timeout, $routeParams, PatientBiometricService, Security) {
        $scope.model = {
            userIdentityId: null,
            items:null
        };

        $scope.options = [
                { value: null, label: 'Select' },
                 { value: true, label: 'Yes' },
                { value: false, label: 'No' }        
        ];

        $scope.model.id = $routeParams.id;
        if ($scope.model.id == null || $scope.model.id <= 0) {
            $scope.model.id = Security.currentUser.id;
        }

        if ($scope.model.id > 0) {
            PatientBiometricService.PatientBiometricGet($scope.model.id).then(
                function (result) {
                    $scope.model.userIdentityId = result.data.userIdentityId;
                    $scope.model.items = result.data.items;
                    $scope.$apply();
                },

                function (error) {
                    handleError(error);
                });
        };

        $scope.successMessage = "Saved successfully";

        $scope.submitForm = function () {
            $scope.$broadcast('show-errors-check-validity');

            if ($scope.patientBiometricForm.$invalid)
                return;

            var result = PatientBiometricService.PatientBiometricSave($scope.model.userIdentityId, $scope.model.items);

            result.then(function(result) {

                $scope.handleSuccess = function() {};

                $timeout(function() {
                    $window.history.back();
                }, 1000);
            },

            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.statusText;
                $scope.$broadcast('show-errors-reset');
            });
        };

       $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
       }

       $scope.sortKeyOrder = {
           key: 'biometricName',
           order: 'ASC',
       };


       $scope.totalItems = 0;
       $scope.currentPage = 1;
       $scope.maxSize = 5;
       $scope.recordsPerPage = 20;
       $scope.numberOfPageButtons = 5;

       $scope.sort = function (col) {
           if ($scope.sortKeyOrder.key === col) {
               if ($scope.sortKeyOrder.order == 'ASC')
                   $scope.sortKeyOrder.order = 'DESC';
               else
                   $scope.sortKeyOrder.order = 'ASC';
           } else {
               $scope.sortKeyOrder.key = col;
               $scope.sortKeyOrder.order = 'ASC';
           }
           loadGrid();
       };

       $scope.searchFor = '';
       $scope.search = function () {
           loadGrid();
       }


       var loadGrid = function () {
           var searchFor = '';

           PatientBiometricService.GridPatients($scope.currentPage, $scope.recordsPerPage,
                                     $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor, $scope.model.id).then(
               function (result) {
                   $scope.patientbiometricGrid = result.data.results;
                   $scope.totalItems = result.data.recordCount;
               },
               function (error) {
                   alert("an error occured: unable to get data");
               });
       };


       $scope.pageChanged = function () {
           loadGrid();
       };

       loadGrid();
    }

    ]);


