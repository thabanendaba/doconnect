﻿AngularApp.controller('NotificationController',
[
    '$scope', '$modalInstance', 'Security', '$location', '$timeout', 'notification', '$rootScope', 'ChatService', 'DoctorService',
     function ($scope, $modalInstance, Security, $location, $timeout, notification, $rootScope, ChatService, DoctorService) {

        $scope.notificationDetails = {
            senderUserId: notification.SendUserId,
            senderName: notification.SenderName,
            notificationMessage: notification.NotificationMessage,
            notificationType: notification.NotificationType
        };

        $scope.details = {
            SenderId: notification.SenderId,
            RecieverId: notification.RecieverId,
            UserName: notification.UserName,
            Subject: notification.Subject
            
        };

        $scope.noClick = function () {
            DoctorService.NotificationResponse($scope.notificationDetails.senderUserId, false).then(function(data) {
                    $modalInstance.dismiss('cancel');
                },
                function(error) {
                    $modalInstance.dismiss('cancel');
                    alert(error.statusText);
                });

        };

        $scope.successMessage = "initiating chat";
       

        $scope.yesClick = function() {

            DoctorService.NotificationResponse($scope.notificationDetails.senderUserId, true).then(function(data) {
                    $scope.handleSuccess = function() {}; //?check if required

                    $timeout(function() {
                        $modalInstance.close($location.path('/chat/' + $scope.notificationDetails.senderUserId));
                    }, 1000);

                },
                function(error) {
                    $modalInstance.dismiss('cancel');
                    $scope.errorMessage = error.statusText;
                    alert(error.statusText);
                });


        };

        $scope.declineClick = function () {
            DoctorService.NotificationResponse($scope.notificationDetails.senderUserId, false).then(function (data) {
                $modalInstance.dismiss('cancel');
            },
                function (error) {
                    $modalInstance.dismiss('cancel');
                    alert(error.statusText);
                });

        };

        $scope.successMessage = "initiating chat";


        $scope.acceptClick = function () {

            DoctorService.PatientNotificationResponse($scope.details.SenderId, true).then(function (data) {
                $scope.handleSuccess = function () { }; //?check if required

                $timeout(function () {
                    $modalInstance.close($location.path('/doctorChat/' + $scope.details.SenderId));
                }, 1000);

            },
                function (error) {
                    $modalInstance.dismiss('cancel');
                    $scope.errorMessage = error.statusText;
                    alert(error.statusText);
                });


        };

        $scope.successM = "nitiating chat";
        $scope.FailM = "Chat Request not Accepted";

        $scope.counter = 30;

        $scope.onTimeOut = function () {
            
            $scope.counter--;
            myTimeOut = $timeout($scope.onTimeOut, 1000);

            if ($scope.counter === 0) {
                $timeout.cancel(myTimeOut);
                //go red
                $scope.Fail = function () { };
                $timeout(function () {
                    $modalInstance.close();
                }, 1000);
                
            }
        };

        var myTimeOut = $timeout($scope.onTimeOut, 1000);
         $scope.stop = function() {
             $timeout.cancel(myTimeOut);
         };

         $modalInstance.opened.then(function () {       
             $scope.Success = function () { };
            
         });


         $rootScope.$on(ChatService.OnChatRequestResponseEvent, function (event, patientChatResponseModel) {
            if (patientChatResponseModel.AcceptChat) {
                $scope.stop($modalInstance.close());   
                $timeout(function () {
                    $location.path('/doctorChat/' + patientChatResponseModel.SendUserId);
                }, 10);
            }
        });

}]);