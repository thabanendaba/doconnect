﻿AngularApp.controller('PasswordController',
    ['$scope', '$uibModalInstance', '$timeout', '$location','PasswordService',
    function ($scope, $uibModalInstance, $timeout, $location, PasswordService) {
        $scope.changePasswordModel = {
            oldPassword: '',
            newPassword: '',
            confirmPassword: ''
        };

        $scope.successMessage = "Password Changed";

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.statusText;
            
        };

        $scope.ok = function () {

            if ($scope.passwordForm.$invalid)
                return;
            
            var result = PasswordService.ChangePassword($scope.changePasswordModel.oldPassword,
                                            $scope.changePasswordModel.newPassword,
                                            $scope.changePasswordModel.confirmPassword);
            
            result.then(function (result) {

                $scope.handleSuccess = function() { };

                $timeout(function () {
                    $uibModalInstance.close($location.path('/myProfile'));
                }, 1000);
            },
            function(error) {
                $scope.hasError = true;
                $scope.errorMessage = error.statusText;
            });
    };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
            
        };
        
        }
    ]);