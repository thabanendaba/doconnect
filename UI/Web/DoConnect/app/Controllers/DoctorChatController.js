﻿AngularApp.controller('DoctorChatController',
    ['$scope', '$http', 'Security', '$routeParams', 'ChatService', '$rootScope', '$filter', 'toaster', 'PatientBiometricService',
function ($scope, $http, Security, $routeParams, ChatService, $rootScope, $filter, toaster, PatientBiometricService) {

    $scope.messages = [];

    $scope.chatDoctor = null;

    $scope.chatDoctorOption = {

    };

    $scope.chatDetails = {
        senderId: Security.currentUser.id,
        recieverId: $routeParams.id,
        subject: '',
        userName: Security.currentUser.displayName,
        dateTime: null
    };

    //$scope.senderDisplayName = $routeParams.senderDisplayName;

    $scope.patientUserIdentityId = $routeParams.id;

    if ($scope.patientUserIdentityId != 0) {
        ChatService.GetSenderDisplayName($scope.patientUserIdentityId).then(
        function (result) {
            $scope.patientDisplayName = result.data.firstName + " " + result.data.surname;
        }
    );
    }


    if ($scope.patientUserIdentityId == null || $scope.patientUserIdentityId <= 0) {
        $scope.patientUserIdentityId = $scope.RecieverId;
    }
    //fetch patient biometric and render html grid
    $scope.sortKeyOrder = {
        key: 'biometricName',
        order: 'ASC',
    };


    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.recordsPerPage = 20;
    $scope.numberOfPageButtons = 5;

    $scope.sort = function (col) {
        if ($scope.sortKeyOrder.key === col) {
            if ($scope.sortKeyOrder.order == 'ASC')
                $scope.sortKeyOrder.order = 'DESC';
            else
                $scope.sortKeyOrder.order = 'ASC';
        } else {
            $scope.sortKeyOrder.key = col;
            $scope.sortKeyOrder.order = 'ASC';
        }
        loadGrid();
    };

    $scope.searchFor = '';
    $scope.search = function () {
        loadGrid();
    }


    var loadGrid = function () {
        var searchFor = '';

        PatientBiometricService.GridPatients($scope.currentPage, $scope.recordsPerPage,
                                  $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor, $scope.patientUserIdentityId).then(
            function (result) {
                $scope.patientbiometricGrid = result.data.results;
                $scope.totalItems = result.data.recordCount;
            },
            function (error) {
                alert("an error occured: unable to get data");
            });
    };


    $scope.pageChanged = function () {
        loadGrid();
    };

    loadGrid();
    
    //fetch patient biometric and render html grid

    


    $scope.clear = function () {
        ChatForm.elements.txtChat.value = "";
        $scope.chatDetails.subject = '';
    };

    $scope.sendMessage = function () {

        $scope.chatDetails.dateTime = new Date();
        $scope.$broadcast('show-errors-check-validity');

        if ($scope.ChatForm.$invalid)
            return;

        $scope.chatDetails.recieverId = $scope.patientUserIdentityId;

        ChatService.SendChat($scope.chatDetails);

        $scope.messages.push({
            message:
                 {
                     senderId: $scope.chatDetails.senderId,
                     subject: $scope.chatDetails.subject,
                     recieverId: $scope.patientUserIdentityId,
                     userName: 'Me',
                     dateTime: $scope.chatDetails.dateTime


                 }
        });

        while ($scope.messages.length > 5) {
            $scope.messages.shift();
        }


        ChatForm.elements.txtChat.value = "";
        $scope.chatDetails.subject = '';
        $scope.newMessage = "";
    };

    var pushMessage = function (message) {
        $scope.messages.push({
            message:
                        {
                            senderId: message.SenderId,
                            subject: message.Subject,
                            recieverId: message.RecieverId,
                            userName: message.UserName,
                            dateTime: message.DateTime
                        }
        });


        while ($scope.messages.length > 5) {
            $scope.messages.shift();
        }
    }

    $rootScope.$on(ChatService.OnChatEvent, function (event, message) {
        pushMessage(message);
        $scope.$apply();
    });

    

    var init = function () {
        var chatHistory = ChatService.GetAllChats();

        for (x = 0; x < chatHistory.length ; x++) {
            pushMessage(chatHistory[x]);
        }

        $scope.$apply();
    }

    init();

    var handleError = function (error) {
        $scope.hasError = true;
        $scope.errorMessage = error.statusText;
        Security.logout();
    };


}
    ]);
