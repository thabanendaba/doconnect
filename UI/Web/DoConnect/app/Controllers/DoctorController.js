﻿AngularApp.controller('DoctorController',
    ['$scope', '$location', '$window', '$timeout', '$routeParams', 'EnumsService', 'DoctorService', 'ProfileService',
    function ($scope, $location, $window, $timeout, $routeParams, EnumsService, DoctorService, ProfileService) {

        $scope.model = {
            id: null,
            userName: '',
            password: '',
            emailAddress: '',
            title: '',
            firstName: '',
            surname: '',
            idOrPassportNumber: '',
            gender: '',
            telephone: '',
            addressLine1: '',
            addressLine2: '',
            city: '',
            postalCode: ''
        };

        if ($routeParams.id > 0) {
            $scope.model.id = $routeParams.id;
        }

        if ($scope.model.id > 0) {
            DoctorService.DoctorGet($scope.model.id).then(
                function (result) {

                    //$scope.model.id = result.data.id;
                    $scope.model.userName = result.data.userName;
                    $scope.model.password = result.data.password;
                    $scope.model.emailAddress = result.data.emailAddress;
                    $scope.model.title = result.data.title;
                    $scope.model.firstName = result.data.firstName;
                    $scope.model.surname = result.data.surname;
                    $scope.model.idOrPassportNumber = result.data.idOrPassportNumber;
                    $scope.model.gender = result.data.gender;
                    $scope.model.telephone = result.data.telephone;
                    $scope.model.addressLine1 = result.data.addressLine1;
                    $scope.model.addressLine2 = result.data.addressLine2;
                    $scope.model.city = result.data.city;
                    $scope.model.postalCode = result.data.postalCode;

                },

                function (error) {
                    handleError(error);
                });
        };

        $scope.genderTypes = EnumsService.genderTypes();

        var loadGrid = function () {
            DoctorService.DoctorGrid($scope.currentPage, $scope.recordsPerPage,
                                      $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                function (result) {
                    $scope.doctorGrid = result.data.results;
                    $scope.totalItems = result.data.recordCount;
                },
                function (error) {
                    alert("an error occured: unable to get data");
                });
        };

        $scope.successMessage = "Saved Successfully";


        $scope.submitForm = function() {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.doctorRegisterForm.$invalid)
                return;

            if ($scope.model.id > 0) {

                var result = ProfileService.ProfileSave($scope.model.userName, $scope.model.gender, $scope.model.idPassportNum,
                    $scope.model.surname, $scope.model.firstName, $scope.model.title,
                    $scope.model.emailAddress, $scope.model.telephone, $scope.model.postalCode,
                    $scope.model.city, $scope.model.addressLine1, $scope.model.addressLine2);
                result.then(function (result) {

                    $scope.handleSuccess = function () { };

                    $timeout(function () { $window.history.back(); }, 2000);

                },
                function (error) {
                    $scope.hasError(error);

                });

            }
            else {

                var result = DoctorService.RegisterDoctor($scope.model.userName,
                $scope.model.genderType,
                $scope.model.idOrPassport,
                $scope.model.surname,
                $scope.model.firstName,
                $scope.model.title,
                $scope.model.email,
                $scope.model.telephone,
                $scope.model.password,
                $scope.model.confirmPassword,
                $scope.model.postalCode,
                $scope.model.city,
                $scope.model.addressLine1,
                $scope.model.addressLine2
            );
                result.then(function (result) {

                    $scope.handleSuccess = function () { };

                    $timeout(function () { $window.history.back(); }, 2000);

                },
                    function (error) {
                        $scope.hasError = true;
                        $scope.errorMessage = error.statusText;
                        Security.logout();
                    });

            }
            
        };

        

        $scope.newDoctor = function () {
            $location.path("/newDoctor/0");
        };

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.statusText;
        }

        $scope.sortKeyOrder = {
            key: 'userName',
            order: 'ASC',
        };

        //$scope.data = DoctorService.userRoleList;
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.maxSize = 5;
        $scope.recordsPerPage = 20;
        $scope.numberOfPageButtons = 5;

        $scope.sort = function (col) {
            if ($scope.sortKeyOrder.key === col) {
                if ($scope.sortKeyOrder.order == 'ASC')
                    $scope.sortKeyOrder.order = 'DESC';
                else
                    $scope.sortKeyOrder.order = 'ASC';
            } else {
                $scope.sortKeyOrder.key = col;
                $scope.sortKeyOrder.order = 'ASC';
            }
            loadGrid();
        };

        $scope.searchFor = '';
        $scope.search = function () {
            loadGrid();
        }

        $scope.pageChanged = function () {
            loadGrid();
        };


        loadGrid();
    }

    ]);