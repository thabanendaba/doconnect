﻿AngularApp.controller('RegisterController',
    ['$scope', '$location', '$window', '$timeout', 'AccountService','Security', 'EnumsService',

    function ($scope, $location, $window, $timeout, AccountService, Security, EnumsService) {

        $scope.registerModel = {
            userName:'',
            gender: '',
            idOrPassport: '',
            surname: '',
            firstName: '',
            title: '',
            email: '',
            telephone: '',
            password: '',
            confirmPassword: '',
            postalCode: '',
            city: '',
            addressLine1: '',
            addressLine2:''
        };

        $scope.genderTypes = EnumsService.genderTypes();

        $scope.successMessage = "Registered successfully";

        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.registerForm.$invalid)
                return;

            var result = AccountService.register($scope.registerModel.userName,
                                                $scope.registerModel.genderType,
                                                $scope.registerModel.idOrPassport,
                                                $scope.registerModel.surname,
                                                $scope.registerModel.firstName,
                                                $scope.registerModel.title,
                                                $scope.registerModel.email,
                                                $scope.registerModel.telephone,
                                                $scope.registerModel.password,
                                                $scope.registerModel.confirmPassword,
                                                $scope.registerModel.postalCode,
                                                $scope.registerModel.city,
                                                $scope.registerModel.addressLine1,
                                                $scope.registerModel.addressLine2

                );
            result.then(function (result) {
                
                $scope.handleSuccess = function () { };

                $timeout(function () {
                    Security.login(result.data.id, result.data.userName, result.data.displayName, result.data.allowedPrivileges, result.data.userTypeString);
                    $location.path('/patientHome');
                }, 2000);

                
            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.statusText;
                Security.logout();
            });
        }

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }
    }

]);


