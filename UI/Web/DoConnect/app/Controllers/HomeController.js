﻿AngularApp.controller('HomeController',
    ['$scope', '$rootScope','EnumsService','ChatService','$location', 'Security',
    function ($scope, $rootScope, EnumsService,ChatService,$location,Security) {
        $scope.applicationReady = EnumsService.enumsLoaded();

        $rootScope.$on(EnumsService.enumsLoadedEvent, function (event) {
            $scope.applicationReady = true;
            $scope.$apply();
        });


        $rootScope.$on(Security.scopeUpdateEvent, function (event, currentUser) {
            var currentUser = Security.currentUser;
            if (currentUser.loggedIn) {
               var userTypes = EnumsService.userTypes();

               if (userTypes[1].description == currentUser.userType) {
                    $location.path('/doctorHome');
                }
               else if (userTypes[2].description == currentUser.userType) {
                    $location.path('/patientHome');
                }
            }
        });


      

    }

]);

