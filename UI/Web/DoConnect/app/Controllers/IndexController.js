﻿AngularApp.controller('IndexController',
    ['$scope', 'HomeService', 'Security', '$rootScope', 'EnumsService', 'ChatService',
     '$uibModal', '$location', 'toaster', '$log', '$timeout', 'AccountService',
     '$window',
function ($scope, HomeService, Security, $rootScope, EnumsService, ChatService,
      $modal, $location, toaster, $log, $timeout, AccountService,$window) {

    $scope.panicEnabled = false;
    $scope.applicationReady = false;
    $scope.navbarProperties = {
        isCollapsed: true
    };

    $scope.currentUser = Security.currentUser;

    $rootScope.$on(Security.scopeUpdateEvent, function (event, currentUser) {
        $scope.currentUser = Security.currentUser;
        if ($scope.currentUser.loggedIn) {
            ChatService.StartHub();
        }
    });

    $rootScope.$on(EnumsService.enumsLoadedEvent, function (event) {
        $scope.applicationReady = true;
       
    });

    $rootScope.$on(ChatService.OnChatEvent, function (event, message) {

        //$scope.doctorChatNotification(message);

        //this is where our modal will open 
        //check if we have to toast something only if we are not in the chat window
        if ($location.path().indexOf('/chat/') < 0 && $location.path().indexOf('/doctorChat/') < 0) {

                    AccountService.currentUser().then(
                     function (result) {
                         Security.login(result.data.id, result.data.userName, result.data.displayName, result.data.allowedPrivileges, result.data.userTypeString);

                         $scope.userTypes = EnumsService.userTypes();

                         if ($scope.userTypes[1].description == result.data.userTypeString) {
                             ChatService.DoctorChatRequestModal(message);
                         }
                         else if ($scope.userTypes[2].description == result.data.userTypeString) {

                             toaster.warning("Chat message from: " + message.UserName, message.Subject);
                         }

                     }

                     );
                }
    });

    //$scope.doctorChatNotification = function (message) {
    //    $scope.modalInstance = $modal.open({
    //        animation: false,
    //        templateUrl: 'app/Views/Chat/DoctorChatNotificationModal.html',
    //        controller: 'NotificationController',
    //        resolve: {
    //            notification: function () {
    //                return message;
    //            }
    //        }
    //    });

    //    $scope.modalInstance.result.then(function (result) {
    //        $log.info(' button ok reached');
    //    }, function () {
    //        $log.info(' button cancel reached');
    //    });

    //}

    $scope.showNotification = function (message) {

        $scope.modalInstance = $modal.open({
            animation: false,
            templateUrl: 'app/Views/Chat/Notification.html',
            controller: 'NotificationController',
            resolve: {
                notification: function () {
                    return message;
                }
            }
        });

        $scope.modalInstance.result.then(function (result) {
            $log.info(' button ok reached');
        }, function () {
            $log.info(' button cancel reached');
        });
      
    }

    $rootScope.$on(ChatService.OnNotificationEvent, function (event, message) {
        $scope.showNotification(message);
    });

    //Login state
    $scope.initHome = function (loggedIn, currentUser) {
        if (loggedIn) {
            AccountService.currentUser().then(
             function (result) {
                 Security.login(result.data.id, result.data.userName, result.data.displayName, result.data.allowedPrivileges,result.data.userTypeString);
                
                 $scope.userTypes = EnumsService.userTypes();

                 if ($scope.userTypes[0].description == result.data.userTypeString) {
                     $location.path('/home');
                 }
                 else if ($scope.userTypes[1].description == result.data.userTypeString) {
                     $location.path('/doctorHome');
                 }
                 else if ($scope.userTypes[2].description == result.data.userTypeString) {
                     $location.path('/patientHome');
                 }

             },

             function (error) {
                 $scope.hasError = true;
                 $scope.errorMessage = error.statusText;
                 Security.logout();
             }

             );
        }
    }

    $scope.logout = function () {
        AccountService.singOut().then(function (data) {
            $scope.currentUser.loggedIn = false;
            $scope.$apply();

            Security.logout();
            $location.path('/login');
        },
        function (error) {
            $scope.currentUser.loggedIn = false;
            $scope.$apply();

            Security.logout();
            $location.path('/login');
        });
    }

    $scope.isAllowed = function (privilegeType) {
        return Security.isAllowed(privilegeType);
    }

    $scope.userHasPrivileges = function () {
        return Security.userHasPrivileges();
    }


    $scope.collapseMenu = function () {
        $scope.navbarProperties.isCollapsed = true;
        $scope.$apply();

    }

    $(".navbar a").click(function (element) {
        $scope.collapseMenu();
    });


    var geoLocator = null;
    $scope.location = null;

    var sendPanicLocation = function () {
        if ($scope.location != null) {
            AccountService.panic($scope.location.latitude, $scope.location.longitude)
                            .then(function () {
                                alert('Your panic was sent!');
                            },
                            function (error) {
                                alert('Unable to send panic');
                            })
        } else {
            alert('Unable to send panic - location not supported');
        }
    };
    
    $scope.panic = function () {
        var nav = $window.navigator;
        if(nav != null)
            geoLocator = nav.geolocation;
        if (geoLocator != null) {
            geoLocator.getCurrentPosition(
             function (position) {
                 $scope.location = position.coords;
                 sendPanicLocation();
             },
             function (error) {
                 console.log("Geolocation not found");
                 sendPanicLocation();
             },
             {
                 maximumAge: 1000,
                 enableHighAccuracy: true
             });
        } else {
            alert('Unable to send panic - location not supported');
        }
    }


    var initLocation = function () {
        $scope.panicEnabled = false;
        $scope.location = null;
        var nav = $window.navigator;
        if (nav != null) {
            geoLocator = nav.geolocation;
            if (geoLocator != null) {
                geoLocator.getCurrentPosition(
                    function (position) {
                        $scope.panicEnabled = true;
                        $scope.location = position.coords;
                    }, 
                    function (error) {
                        $scope.panicEnabled = false;
                    },
                     {
                         maximumAge: 1000,
                         enableHighAccuracy: true
                     });
            }
            else {
                console.log("Geolocation not supported");
            }
        }
        else {
            console.log("Navigator not found");
        }
    }

    initLocation();
}

]);

