﻿AngularApp.controller('ProfileController',
    ['$scope', '$location', '$window', '$timeout', '$uibModal', '$log', '$routeParams', 'EnumsService', 'ProfileService',
        function ($scope, $location, $window, $timeout, $uibModal, $log, $routeParams, EnumsService, ProfileService) {

            $scope.profileModel = {
                userName: '',
                gender: '',
                //genderType:'',
                idPassportNum: '',
                surname: '',
                firstName: '',
                title: '',
                emailAddress: '',
                telephone: '',
                postalCode: '',
                city: '',
                addressLine1: '',
                addressLine2: ''

            };

            ProfileService.GetProfile().then(
                function (result) {

                    $scope.profileModel.userName = result.data.userName;
                    $scope.profileModel.gender = result.data.gender;
                    $scope.profileModel.idPassportNum = result.data.idPassportNum;
                    $scope.profileModel.surname = result.data.surname;
                    $scope.profileModel.firstName = result.data.firstName;
                    $scope.profileModel.title = result.data.title;
                    $scope.profileModel.emailAddress = result.data.emailAddress;
                    $scope.profileModel.telephone = result.data.telephone;
                    $scope.profileModel.postalCode = result.data.postalCode;
                    $scope.profileModel.city = result.data.city;
                    $scope.profileModel.addressLine1 = result.data.addressLine1;
                    $scope.profileModel.addressLine2 = result.data.addressLine2;
                },
                function (error) {
                    handleError(error);

                });

            $scope.genderTypes = EnumsService.genderTypes();

            if ($routeParams.id > 0) {
                $scope.profileModel.id = $routeParams.id;
            }
            

            if ($scope.profileModel.id > 0) {
                ProfileService.DoctorGet($scope.profileModel.id).then(
                    function (result) {

                        //$scope.model.id = result.data.id;
                        $scope.profileModel.userName = result.data.userName;
                        $scope.profileModel.password = result.data.password;
                        $scope.profileModel.emailAddress = result.data.emailAddress;
                        $scope.profileModel.title = result.data.title;
                        $scope.profileModel.firstName = result.data.firstName;
                        $scope.profileModel.surname = result.data.surname;
                        $scope.profileModel.idPassportNum = result.data.idPassportNum;
                        $scope.profileModel.gender = result.data.gender;
                        $scope.profileModel.telephone = result.data.telephone;
                        $scope.profileModel.addressLine1 = result.data.addressLine1;
                        $scope.profileModel.addressLine2 = result.data.addressLine2;
                        $scope.profileModel.city = result.data.city;
                        $scope.profileModel.postalCode = result.data.postalCode;

                    },

                    function (error) {
                        handleError(error);
                    });
            };


            $scope.editProfile = function () {
                $location.path('/myProfile');
            };

            $scope.successMessage = "Profile Changes Saved";

            $scope.submitForm = function () {

                $scope.$broadcast('show-errors-check-validity');

                if ($scope.profileEditForm.$invalid)
                    return;

                if ($scope.profileModel.id > 0) {
                    var result = ProfileService.DoctorSave($scope.profileModel.id, $scope.profileModel.userName, $scope.profileModel.gender, $scope.profileModel.idPassportNum,
                    $scope.profileModel.surname, $scope.profileModel.firstName, $scope.profileModel.title,
                    $scope.profileModel.emailAddress, $scope.profileModel.telephone, $scope.profileModel.postalCode,
                    $scope.profileModel.city, $scope.profileModel.addressLine1, $scope.profileModel.addressLine2);
                }
                else {
                    var result = ProfileService.ProfileSave($scope.profileModel.userName, $scope.profileModel.gender, $scope.profileModel.idPassportNum,
                    $scope.profileModel.surname, $scope.profileModel.firstName, $scope.profileModel.title,
                    $scope.profileModel.emailAddress, $scope.profileModel.telephone, $scope.profileModel.postalCode,
                    $scope.profileModel.city, $scope.profileModel.addressLine1, $scope.profileModel.addressLine2);
                }
                

                result.then(function (result) {

                    $scope.handleSuccess = function () { };

                    $timeout(function () { $window.history.back(); }, 2000);

                },
                function (error) {
                    $scope.hasError(error);

                });

            };

            var handleError = function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.statusText;
                Security.logout();
            };


            $scope.cancelForm = function () {
                $scope.$broadcast('show-errors-reset');
                $window.history.back();
            };


            $scope.changePassword = function () {
                $scope.$broadcast('show-errors-check-validity');
                if ($scope.profileEditForm.$invalid)
                    return;

                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'app/Views/Account/PasswordView.html',
                    controller: 'PasswordController'
                });
                modalInstance.result.then(function (result) {
                    $log.info(' button ok reached');
                }, function () {
                    $log.info(' button cancel reached');
                });
                
            };
            
            }

        

    ]);