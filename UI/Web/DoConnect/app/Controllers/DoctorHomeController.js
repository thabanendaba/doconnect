﻿AngularApp.controller('DoctorHomeController',
    ['$scope', '$location', '$window', 'DoctorService', '$rootScope', '$uibModal', '$routeParams', 'ChatService','$interval',
    function ($scope, $location, $window, DoctorService, $rootScope, $modal, $routeParams, ChatService, $interval) {

        $scope.model = {
            id: null,
            userName: '',
            password: '',
            emailAddress: '',
            title: '',
            firstName: '',
            surname: '',
            idOrPassportNumber: '',
            gender: '',
            telephone: '',
            addressLine1: '',
            addressLine2: '',
            city: '',
            postalCode: ''
        };

        $scope.model.id = $routeParams.id;

        $scope.chatRequest = function (n) {

            DoctorService.OpenChatWaitingModal(n);


            DoctorService.NotifyPatient(n).then(
                function (result) {
                    $scope.errorMessage = result.data;
                },
                function (error) {
                    handleError(error);
                });
        }

        $scope.showNotification = function (message) {

            var modalInstance = $modal.open({
                animation: false,
                templateUrl: 'app/Views/Chat/Notification.html',
                controller: 'NotificationController',
                resolve: {
                    notification: function () {
                        return message;
                    }
                }
            });

            modalInstance.result.then(function (result) {
                $log.info(' button ok reached');
            }, function () {
                $log.info(' button cancel reached');
            });

        }

        $rootScope.$on(ChatService.OnNotificationEvent, function (event, message) {
            $scope.showNotification(message);
        });

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.statusText;
        }

        $scope.sortKeyOrder = {
            key: 'userName',
            order: 'ASC',
        };

        //$scope.data = UserRoleService.UserRoleList;
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.maxSize = 5;
        $scope.recordsPerPage = 20;
        $scope.numberOfPageButtons = 5;

        $scope.sort = function (col) {
            if ($scope.sortKeyOrder.key === col) {
                if ($scope.sortKeyOrder.order == 'ASC')
                    $scope.sortKeyOrder.order = 'DESC';
                else
                    $scope.sortKeyOrder.order = 'ASC';
            } else {
                $scope.sortKeyOrder.key = col;
                $scope.sortKeyOrder.order = 'ASC';
            }
            loadGrid();
        };

        $scope.searchFor = '';
        $scope.search = function () {
            loadGrid();
        }

        var loadGrid = function () {
            DoctorService.PatientsWithProblemsGrid($scope.currentPage, $scope.recordsPerPage,
                                      $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                function (result) {
                    $scope.patientsWithProblemsGrid = result.data.results;
                    $scope.totalItems = result.data.recordCount;
                },
                function (error) {
                    alert("an error occured: unable to get data");
                });
        };


        $scope.pageChanged = function () {
            loadGrid();
        };


        loadGrid();

        $interval(function () { loadGrid();}, 60000);

    }
    ]);