﻿AngularApp.controller('BiometricController',
    ['$scope', '$location', '$window', '$routeParams', 'BiometricService','EnumsService',
        function ($scope, $location, $window, $routeParams, BiometricService, EnumsService) {
                $scope.biometricModel = {
                    Id: null,
                    name: '',
                    maxValue: '',
                    minValue: '',
                    triggerAlert: '',
                    biometricType: '',
                    biometricTypeStr: ''
                }

                $scope.biometricTypes = EnumsService.biometricTypes();

            if ($routeParams.id != null) {
                $scope.biometricModel.id = $routeParams.id;
            } else {
                $scope.biometricModel.biometricType = ' ';
            };
            

                $scope.options = [
                    { value: ' ', label: 'Select' },
                 { value: 0, label: $scope.biometricTypes[0].description },
                  { value: 1, label: $scope.biometricTypes[1].description },
                 { value: 2, label: $scope.biometricTypes[2].description }
                ];

            

                if ($scope.biometricModel.id > 0) {
                    BiometricService.GetBiometric($scope.biometricModel.id).then(
                        function (result) {

                            $scope.biometricModel.id = result.data.id;
                            $scope.biometricModel.name = result.data.name;
                            $scope.biometricModel.maxValue = result.data.maxValue;
                            $scope.biometricModel.minValue = result.data.minValue;
                            $scope.biometricModel.triggerAlert = result.data.triggerAlert;
                            $scope.biometricModel.biometricType = result.data.biometricType;

                            

                        },

                        function (error) {
                            handleError(error);
                        });
                };

                $scope.addBiometric = function () {

                    $location.path('/biometricAdd');
            
        }

        $scope.editBiometric = function (id) {

            $location.path('/biometricEdit/' + id);

        }

        $scope.saveForm = function () {


            $scope.$broadcast('show-errors-check-validity');

            if ($scope.biometricSaveForm.$invalid)
                return;

            var result = BiometricService.BiometricSave($scope.biometricModel.id,
                                                        $scope.biometricModel.name,
                                                        $scope.biometricModel.maxValue,
                                                        $scope.biometricModel.minValue,
                                                        $scope.biometricModel.triggerAlert,
                                                        $scope.biometricModel.biometricType
                                                

                );
            result.then(function (result) {
                $window.history.back();
            },
            function (error) {
                $scope.hasError(error);
            });
        }

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.error
 = error.statusText;
            Security.logout();
        }


        $scope.sortKeyOrder = {
            key: 'name',
            order: 'ASC'
        };

        //$scope.data = BiometricService.biometric;

        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.maxSize = 5;
        $scope.recordsPerPage = 20;
        $scope.numberOfPageButtons = 5;





        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }


        $scope.sort = function (col) {
            if ($scope.sortKeyOrder.key === col) {
                if ($scope.sortKeyOrder.order == 'ASC')
                    $scope.sortKeyOrder.order = 'DESC';
                else
                    $scope.sortKeyOrder.order = 'ASC';
            } else {
                $scope.sortKeyOrder.key = col;
                $scope.sortKeyOrder.order = 'ASC';
            }
            loadGrid();
        };

        $scope.searchFor = '';
        $scope.search = function () {
            loadGrid();
        }

        var loadGrid = function () {
            var searchFor = '';


            BiometricService.BiometricGrid($scope.currentPage, $scope.recordsPerPage,
                                      $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                function (result) {
                    $scope.biometricGrid = result.data.results;
                    $scope.totalItems = result.data.recordCount;
                },
                function (error) {
                    alert("an error occured: unable to get data");
                });


        };

        $scope.pageChanged = function () {
            loadGrid();
        };


        loadGrid();
    }

    ]);