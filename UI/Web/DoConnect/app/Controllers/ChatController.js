﻿AngularApp.controller('ChatController',
    ['$scope', '$http', 'Security','ChatService','$rootScope', '$filter','toaster','$routeParams',
function ($scope, $http, Security, ChatService, $rootScope, $filter, toaster, $routeParams) {

    $scope.messages = [];

    $scope.chatDoctor = null;
    $scope.patientDisplayName = null;

    $scope.chatDoctorOption = {

    };

    $scope.selectedDoctorUserIdentityId = null;

    $scope.showDoctorList = true;
    if ($routeParams.id != null && $routeParams.id > 0) {
        $scope.chatDoctor = $routeParams.id;
        $scope.showDoctorList = false;

        ChatService.GetSenderDisplayName($scope.chatDoctor).then(
            function(result) {
                $scope.patientDisplayName = result.data.firstName + " " + result.data.surname;
            });
    };

    $scope.chatDetails = {
        senderId: Security.currentUser.id,
        recieverId: '',
        subject: '',
        userName: Security.currentUser.displayName,
        dateTime: null
    };

    $scope.onlineDoctors = [];

    ChatService.GetDoctors().then(
        function (result) {
            $scope.onlineDoctors = result.data;


            $scope.onlineDoctors = $filter('filter')($scope.onlineDoctors, function (onlineDoctors) {
                return !(onlineDoctors.userIdentityId == Security.currentUser.id);
            });

        },
        function (error) {
            handleError(error);
        });

      
    $scope.clear = function () {
        ChatForm.elements.txtChat.value = "";
        $scope.chatDetails.subject = '';
    };

    $scope.sendMessage = function () {

        $scope.chatDetails.dateTime = new Date();

        if ($scope.chatDoctor == null) {
            toaster.pop('warning', "Error", "Select a doctor");
            //alert('no doctor selected');
        }

           
        $scope.$broadcast('show-errors-check-validity');

        if ($scope.ChatForm.$invalid)
            return;

        $scope.chatDetails.recieverId = $scope.chatDoctor;//.userIdentityId;
        ChatService.SendChat($scope.chatDetails);

        $scope.messages.push({

            message:
                    {
                        senderId: $scope.chatDetails.senderId,
                        subject: $scope.chatDetails.subject,
                        recieverId: $scope.chatDetails.newId,
                        userName: 'Me',
                        dateTime: $scope.chatDetails.dateTime
                        
                           
                    }
        });

        while ($scope.messages.length > 5) {
            $scope.messages.shift();
        }


        ChatForm.elements.txtChat.value = "";
        $scope.chatDetails.subject = '';
        $scope.newMessage = "";
    };

    var pushMessage = function (message) {
        $scope.messages.push({
            message:
                        {
                            senderId: message.SenderId,
                            subject: message.Subject,
                            recieverId: message.RecieverId,
                            userName: message.UserName,
                            dateTime: message.DateTime
                        }
        });


        while ($scope.messages.length > 5) {
            $scope.messages.shift();
        }
    }

    

    $rootScope.$on(ChatService.OnChatEvent, function (event, message) {

        
        pushMessage(message);
        $scope.$apply();
    });

    $rootScope.$on(ChatService.OnNewDoctorEvent, function (event, doctor) {

        var isInList = false;

        for (x = 0; x < $scope.onlineDoctors.length; x++)
        {
            if ($scope.onlineDoctors[x].userIdentityId == doctor.userIdentityId)
            {
                isInList = true;
                break;
            }
        }
        if (!isInList && doctor.userIdentityId != Security.currentUser.id) {
            $scope.onlineDoctors.push(doctor);
        }


        $scope.$apply();
    });

    var init = function () {
        var chatHistory = ChatService.GetAllChats();

        for (x = 0; x < chatHistory.length ; x++) {
            pushMessage(chatHistory[x]);
        }

        $scope.$apply();
    }

    init();

    var handleError = function (error) {
        $scope.hasError = true;
        $scope.errorMessage = error.statusText;
        Security.logout();
    };


    }
    ]);
