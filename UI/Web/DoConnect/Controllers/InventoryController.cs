﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DoConnect.BL.Context;
using DoConnect.BL.Provider.DoctorData;
using DoConnect.BL.Provider.Inventory;
using DoConnect.BL.Provider.PatientData;
using DoConnect.BL.Provider.Security;
using DoConnect.Models.Inventory;

namespace DoConnect.Controllers
{
    public class InventoryController : TCRControllerBase
    {

        #region Ctor

        public ISecurityProvider SecurityProvider { get; set; }
        public IInventory InventoryProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public InventoryController()
        {
            DataContext = new DataContext();
            SecurityProvider = new SecurityProvider(DataContext, CurrentUser);
            InventoryProvider = new Inventory(DataContext, CurrentUser);

        }

        public InventoryController(ISecurityProvider securityProvider, DoctorProvider doctorProvider)
        {
            _MustDisposeContext = false;
            SecurityProvider = securityProvider;
        }

        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }
        #endregion

        // GET: InventoryViewModel
        public ActionResult Index()
        {
            var lsInventories =  InventoryProvider.GetInventories();

            List<InventoryViewModel> lsInventory = new List<InventoryViewModel>();

            foreach (BL.Entities.Inventory.Inventory inventory in lsInventories)
            {
                lsInventory.Add(new InventoryViewModel() {Id = inventory.Id,DateStamp = inventory.DateStamp,InvenoryType = inventory.InvenoryType,Name = inventory.Name,Quantity = inventory.Quantity,UserIdentityId = inventory.UserIdentityId});
            }
            return SerializeToAngular(lsInventory);
        }
    }
}