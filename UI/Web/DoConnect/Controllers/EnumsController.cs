﻿using DoConnect.BL.Entities.PatientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DoConnect.BL.Entities.MasterData;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Entities.Types;
using DoConnect.BL.Types;
using DoConnect.Models.Enum;
using EXO.Lib.Utility;

namespace DoConnect.Controllers
{
    [Filters.NoCache]
    public class EnumsController : TCRControllerBase
    {
        public ActionResult StatusTypeEnum()
        {
            return EnumToModel<StatusType>();
        }

        public ActionResult GenderTypeEnum()
        {
            return EnumToModel<GenderType>();
        }

        public ActionResult PrivilegeTypeEnum()
        {
            return EnumToModel<PrivilegeType>();
        }

        public ActionResult BiometricTypeEnum()
        {
            return EnumToModel<BiometricType>();
        }

        public ActionResult NotificationTypeEnum()
        {
            return EnumToModel<NotificationType>();
        }

        public ActionResult UserTypeEnum()
        {
            return EnumToModel<UserType>();
        }

        public ActionResult SecurityEnum()
        {
            List<SecurityTypeModel> result = new List<SecurityTypeModel>();
            foreach (PrivilegeType t in Enum.GetValues(typeof(PrivilegeType)))
                result.Add(new SecurityTypeModel(t));

            return SerializeToAngular(result);
        }



        private ContentResult EnumToModel<T>(bool sort = true)
        {
            List<EnumModel> e = new List<EnumModel>();

            foreach (T eValue in Enum.GetValues(typeof(T)))
            {
                e.Add(new EnumModel() { Description = NameSplitting.SplitCamelCase(eValue), Value = eValue.ToString() });
            }

            if (sort)
            {
                e = e.OrderBy(a => a.Description).ToList();
            }
            return SerializeToAngular(e);
        }
    }

   
}