﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.MasterData;
using DoConnect.BL.Provider.PatientData;
using DoConnect.Controllers.Filters;
using DoConnect.Models;
using DoConnect.Models.MasterData;
using DoConnect.Models.PatientBiometric;
using DoConnect.BL.Entities.MasterData;
using DoConnect.BL.Provider;

namespace DoConnect.Controllers
{
    [Authorize]
    [Filters.NoCache]
    public class BiometricController : TCRControllerBase
    {
        #region Ctor

        public IMasterDataProvider MasterDataProvider { get; set; }
        public IPatientProvider PatientProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public BiometricController()
        {
            DataContext = new DataContext();
            MasterDataProvider = new MasterDataProvider(DataContext, CurrentUser);
            PatientProvider = new PatientProvider(DataContext, CurrentUser);
        }

        public BiometricController(IMasterDataProvider masterProvider, IPatientProvider patientProvider)
        {
            _MustDisposeContext = false;
            MasterDataProvider = masterProvider;
            PatientProvider = patientProvider;
        }

        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }

        #endregion

        [HttpPost]
        [RequiredPrivilege(PrivilegeType.BiometricMaintenance)]
        public ActionResult BiometricSave(BiometricViewModel model)
        {

            try
            {
                var biometric = MasterDataProvider.SaveBiometric(model.Id, model.Name, model.MaxValue, model.MinValue, model.TriggerAlert, model.BiometricType);
                model.Id = biometric.Id;
                return SerializeToAngular(model);
            }
            catch (MasterDataException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpGet]
        public ActionResult GetBiometric(long? id)
        {
            if (id == null)
                id = 0;

            try
            {
                var model = MasterDataProvider.GetBiometric().Where(a => a.Id == id).Select(a => new BiometricViewModel()
                {
                    Id = a.Id,
                    Name = a.Name,
                    MinValue = a.MinValue,
                    MaxValue = a.MaxValue,
                    TriggerAlert = a.TriggerAlert,
                    BiometricType = a.BiometricType

                }).Single();

                return SerializeToAngular(model);
            }
            catch (MasterDataException e)
            {

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }

        public ActionResult GetAllBiomterics()
        {
            var items = MasterDataProvider.GetBiometric()
                .Select(b => new KeyValueModel { Id = b.Id, Description = b.Name })
                .OrderBy(b => b.Description)
                .ToList();
            return SerializeToAngular(items);
        }

        [HttpPost]
        public ActionResult MyBiometricGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            var filteredQuery = PatientProvider.GetMyBiometric()//Patient provider
                                    .Select(a => new MyBiometricGrid()
                                    {

                                        BiometricId = a.Id,
                                        BiometricName = a.Biometric.Name,
                                        BiometricType = a.Biometric.BiometricType,
                                        MinValue = a.Biometric.MinValue,
                                        MaxValue = a.Biometric.MaxValue,
                                        NumericValue = a.NumericValue,
                                        StringValue = a.StringValue,
                                        YesNoValue = a.YesNoValue

                                    });



            if (model.Searchfor != "null")
            {
                filteredQuery = filteredQuery.Where(r => r.BiometricName.Contains(model.Searchfor) || r.BiometricType.ToString().Contains(model.Searchfor) || r.NumericValue.ToString().Contains(model.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.BiometricName); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "biometricname":
                            filteredQuery = filteredQuery.OrderBy(r => r.BiometricName);
                            break;


                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "biometricname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.BiometricName);
                            break;


                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return SerializeToAngular(new GridResultModel<MyBiometricGrid>(filteredQuery.ToList(), totalNumberOfRecords));
        }

        [HttpPost]
        public ActionResult BiometricGrid(GridModel model)
        {

            int begin = SetupGridParams(model);

            var filteredQuery = MasterDataProvider.GetBiometric()//Patient provider
                                                    .Select(a => new BiometricGridModel()
                                                    {
                                                        Id = a.Id,
                                                        Name = a.Name,
                                                        MinValue = a.MinValue,
                                                        MaxValue = a.MaxValue,
                                                        TriggerAlert = a.TriggerAlert,
                                                        BiometricType = a.BiometricType
                                                    });

            if (model.Searchfor != "null")
            {
                filteredQuery = filteredQuery.Where(r => r.Name.Contains(model.Searchfor) || r.MinValue.ToString().Contains(model.Searchfor) || r.MaxValue.ToString().Contains(model.Searchfor));

            }

            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.Name);

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Name);
                            break;
                        case "minValue":
                            filteredQuery = filteredQuery.OrderBy(r => r.MinValue);
                            break;
                        case "maxValue":
                            filteredQuery = filteredQuery.OrderBy(r => r.MaxValue);
                            break;
                        case "biometrictype":
                            filteredQuery = filteredQuery.OrderBy(r => r.BiometricType);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Name);
                            break;
                        case "minValue":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.MinValue);
                            break;
                        case "maxValue":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.MaxValue);
                            break;
                        case "biometrictype":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.BiometricType);
                            break;
                    }
                    break;

            }

            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);


            return SerializeToAngular(new GridResultModel<BiometricGridModel>(filteredQuery.ToList(), totalNumberOfRecords));

        }
    }
}