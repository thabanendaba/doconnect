﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.PatientData;
using DoConnect.BL.Provider.Security;
using DoConnect.Controllers.Filters;
using DoConnect.Models;
using DoConnect.Models.SecurityData;
using DoConnect.BL.Types;
using DoConnect.BL.Provider;

namespace DoConnect.Controllers
{
    [Authorize]
    [Filters.NoCache]
    public class UserRoleController : TCRControllerBase
    {
        #region Ctor

        public ISecurityProvider SecurityProvider { get; set; }
        public IPatientProvider PatientProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public UserRoleController()
        {
            DataContext = new DataContext();
            SecurityProvider = new SecurityProvider(DataContext, CurrentUser);
            PatientProvider = new PatientProvider(DataContext, CurrentUser);
            
        }

        public UserRoleController(ISecurityProvider securityProvider)
        {
            _MustDisposeContext = false;
            SecurityProvider = securityProvider;
        }

        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }
        #endregion

        
        [HttpPost]
        [RequiredPrivilege(PrivilegeType.UserMaintenance)]
        public ActionResult UserRoleSave(UserEditModel model)
        {
            try
            {
                
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");

                List<long> roles = new List<long>();
                if (model.RoleList != null)
                    roles = model.RoleList.Select(a => a.RoleId).ToList();
                
                 var UserRole = SecurityProvider.SaveUser(model.UserId, model.UserName, model.Password, model.EmailAddress, model.Title, model.FirstName, model.Surname,
                                    model.IdOrPassportNumber, model.Gender, model.Telephone, model.AddressLine1, model.AddressLine2, model.City,
                                    model.PostalCode,roles, model.IsAdmin);

                 model.UserId = UserRole.Id;

                return SerializeToAngular(model.UserId);
            }
            catch (SecurityException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpPost]
        public ActionResult UserRoleGrid(GridModel model)
        {

            int begin = SetupGridParams(model);


            var filteredQuery = SecurityProvider.GetUserList().Select(a => new UserRoleGridModel()
                                                        {
                                                            Id = a.Id,
                                                            UserName = a.UserName,
                                                            FirstName = a.FirstName,
                                                            Surname = a.Surname,
                                                            Telephone = a.Telephone,
                                                            EmailAddress = a.EmailAddress
                                                        });

            //SecurityProvider.GetUserList()

            if (model.Searchfor != "null")
            {
                filteredQuery = filteredQuery.Where(r => r.UserName.Contains(model.Searchfor)
                                                    || r.FirstName.Contains(model.Searchfor)
                                                    || r.Surname.Contains(model.Searchfor)
                                                    || r.Telephone.Contains(model.Searchfor)
                                                    || r.EmailAddress.Contains(model.Searchfor)
                                                    );
            }
            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.UserName); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            //Setup sort order
            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "username":
                            filteredQuery = filteredQuery.OrderBy(r => r.UserName);
                            break;
                        case "firstname":
                            filteredQuery = filteredQuery.OrderBy(r => r.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderBy(r => r.Surname);
                            break;
                        case "telephone":
                            filteredQuery = filteredQuery.OrderBy(r => r.Telephone);
                            break;
                        case "emailaddress":
                            filteredQuery = filteredQuery.OrderBy(r => r.EmailAddress);
                            break;
                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "username":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.UserName);
                            break;
                        case "firstname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Surname);
                            break;
                        case "telephone":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Telephone);
                            break;
                        case "emailaddress":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.EmailAddress);
                            break;
                    }
                    break;
            }

            //setup paging
            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);


            return SerializeToAngular(new GridResultModel<UserRoleGridModel>(filteredQuery.ToList(), totalNumberOfRecords));
        }

        public ActionResult UserRoleList()
        {
            var itms = SecurityProvider.GetUserList()
                                     .Where(a => a.Active == true)
                                     .Select(b => new KeyValueModel { Id = b.Id, Description = b.UserName })
                                     .OrderBy(b => b.Description)
                                     .ToList();
            return SerializeToAngular(itms);
        }

        public ActionResult UserRoleGet(long? id)
        {
            if (id == null)
                id = 0;
            try
            {

                var filteredQuery = SecurityProvider.GetUserList().Where(a => a.Id == id)
                    .Select(a => new UserRoleGridModel()
                                    {
                                        Id = a.Id,
                                        UserName = a.UserName,
                                        FirstName = a.FirstName,
                                        Surname = a.Surname,
                                        Telephone = a.Telephone,
                                        EmailAddress = a.EmailAddress,
                                        Password = a.PasswordHash,
                                        Title = a.Title,
                                        IdOrPassportNumber = a.IdPassportNum, 
                                        Gender = a.Gender,
                                        AddressLine1 = a.AddressLine1, 
                                        AddressLine2 = a.AddressLine2, 
                                        City = a.City,
                                        PostalCode = a.PostalCode,
                                        IsAdmin = a.IsSystemAdmin
                                        
                                    });

                var model = filteredQuery.Single();

                model.RoleList = SecurityProvider.GetRoles().Select(a => new UserRoleModel()
                {
                    RoleId = a.Id,
                    RoleName = a.RoleName,
                    Selected = false
                }).ToList();

                var selectedRoles = SecurityProvider.GetUserList().Where(a => a.Id == id)
                                                    .Select(a => a.Roles)
                                                    .Single()
                                                    .Select(a => a.Id).ToArray();

                foreach (var itm in model.RoleList.Where(a => selectedRoles.Contains(a.RoleId)).ToList())
                {
                    itm.Selected = true;
                }


                return SerializeToAngular(model);
            }
            catch (SecurityException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}