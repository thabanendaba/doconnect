﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.Security;
using DoConnect.Controllers.Filters;
using DoConnect.Models;
using DoConnect.Models.SecurityData;

namespace DoConnect.Controllers
{
    [Authorize]
    [RequiredPrivilege(PrivilegeType.RoleMaintenance)]
    [Filters.NoCache]
    public class RoleController : TCRControllerBase
    {
        #region Ctor


        public ISecurityProvider SecurityProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext { get; set; }

        public RoleController()
        {
            DataContext = new DataContext();
            SecurityProvider = new SecurityProvider(DataContext, CurrentUser);

        }

        public RoleController(ISecurityProvider securityProvider)
        {
            _MustDisposeContext = false;
            SecurityProvider = securityProvider;

        }

        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }



        #endregion

        [HttpPost]
        public ActionResult SaveRole(RoleViewModel model)
        {

            try
            {

                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");
                
                SecurityProvider.SaveRole(model.Id, model.RoleName, model.Description,model.StatusType, model.Permissions.Select(a => a.Privilege).ToList());

                return RoleGet(model.Id);


            }
            catch (SecurityException e)
            {

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }


        public ActionResult Rolelist()
        {
            var items = SecurityProvider.GetRoles()
                                .Select(b => new KeyValueModel { Id = b.Id, Description = b.RoleName })
                                .OrderBy(b => b.Description)
                                .ToList();

            return SerializeToAngular(items);
        }


        [HttpPost]
        public ActionResult RoleGrid(GridModel model)
        {
            int begin = SetupGridParams(model);



            var filteredQuery = SecurityProvider.GetRoles()
                                    .Select(a => new RoleGridModel()
                                    {

                                        Id = a.Id,
                                        RoleName = a.RoleName,
                                        Description = a.Description,
                                        StatusTypes = a.Status
                                    });

            

            if (model.Searchfor != "null")
            {
                filteredQuery = filteredQuery.Where(r => r.RoleName.Contains(model.Searchfor) || r.Description.Contains(model.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.RoleName); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "rolename":
                            filteredQuery = filteredQuery.OrderBy(r => r.RoleName);
                            break;
                        case "description":
                            filteredQuery = filteredQuery.OrderBy(r => r.Description);
                            break;
                        case "status":
                            filteredQuery = filteredQuery.OrderBy(r => r.StatusTypes);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "rolename":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.RoleName);
                            break;
                        case "description":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Description);
                            break;
                        case "status":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.StatusTypes);
                            break;
                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return SerializeToAngular(new GridResultModel<RoleGridModel>(filteredQuery.ToList(), totalNumberOfRecords));
        }

        public ActionResult RoleGet(long? id)
        {
            if (id == null)
                id = 0;

            try
            {
                RoleViewModel model = new RoleViewModel();

                if (id > 0)
                {
                    model = SecurityProvider.GetRoles().Where(a => a.Id == id).Select(a => new RoleViewModel()
                        {
                            Id = a.Id,
                            RoleName = a.RoleName,
                            Description = a.Description,
                            StatusType = a.Status,
                        }).Single();
                }

                model.Permissions = new List<PermissionModel>();

                foreach(PrivilegeType privType in Enum.GetValues(typeof(PrivilegeType)))
                {
                    model.Permissions.Add(new PermissionModel()
                    {
                        Privilege = privType,
                        Selected = false
                    });
                }

                if (id > 0)
                {
                    var privs = SecurityProvider.GetRoles().Where(a => a.Id == id).Single().Privileges;//Gets all the privlege assigned to the role found using the ID
                    foreach (var p in privs)
                    {
                        var priv = model.Permissions.Where(a => a.Privilege == p.Security).SingleOrDefault();//Gets the single privlege assigned to the role
                        if (priv != null)
                        {
                            priv.Selected = true;
                        }
                        else
                        {
                            model.Permissions.Add(new PermissionModel()
                            {
                                Privilege = p.Security,
                                Selected = true
                            });
                        }
                    }
                }
                return SerializeToAngular(model);
            }
            catch (SecurityException e)
            {

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }
    }
}