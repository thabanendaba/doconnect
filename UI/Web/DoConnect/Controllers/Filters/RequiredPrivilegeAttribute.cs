﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DoConnect.BL.Entities.SecurityData;

namespace DoConnect.Controllers.Filters
{
    [System.AttributeUsage(System.AttributeTargets.All, AllowMultiple = false, Inherited = true)]
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public sealed class RequiredPrivilegeAttribute : AuthorizeAttribute
    {
        private PrivilegeType _RequiredPrivilege;

        public RequiredPrivilegeAttribute(PrivilegeType security)
        {
            _RequiredPrivilege = security;
            Roles = security.ToString();
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            TCRControllerBase controller = filterContext.Controller as TCRControllerBase;
            if (controller.CurrentUser != null && controller.CurrentUser.AllowedPrivileges != null && !controller.CurrentUser.AllowedPrivileges.Contains(_RequiredPrivilege))
                filterContext.Result = new ViewResult { ViewName = "AccessDenied" };
        }


    }
}