﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System;
using System.Net;
using System.Collections.Generic;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider;
using System.Threading;
using DoConnect.BL.Context;
using DoConnect.BL.Provider.PanicData;
using DoConnect.BL.Provider.PatientData;
using DoConnect.BL.Provider.Security;
using DoConnect.Models.Account;

namespace DoConnect.Controllers
{
    [Authorize]
    [Filters.NoCache]
    public class AccountController : TCRControllerBase
    {
        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private ISecurityProvider SecurityProvider { get; set; }

        public AccountController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            SecurityProvider = new SecurityProvider(Context,CurrentUser);
        }

        public AccountController(ISecurityProvider securityProvider)
        {
            SecurityProvider = securityProvider;
        }

        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }


    
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");

                var user = SecurityProvider.UserLogin(model.UserName, model.Password);
                CurrentUser = new Models.Account.CurrentUserModel(user);

                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);

                return SerializeToAngular(CurrentUser);

            }
            catch (SecurityException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(RegisterViewModel model)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    var user = SecurityProvider.SignUp(model.UserName, model.Password, model.Email,
                        model.Title, model.FirstName, model.Surname,
                        model.IDOrPassport, model.GenderType, model.Telephone,
                        model.AddressLine1, model.AddressLine2, model.City,
                        model.PostalCode);

                    var aCurrentUser = SecurityProvider.UserIdentityToCurrentUser(user);

                    IPatientProvider patientProvider = new PatientProvider(Context, CurrentUser);
                    patientProvider.SavePatient(null, aCurrentUser.Id);

                    var aUser = SecurityProvider.GetCurrentUser(user.UserName);
                    CurrentUser = new Models.Account.CurrentUserModel(aUser);

                    FormsAuthentication.SetAuthCookie(model.UserName, false);

                    return SerializeToAngular(CurrentUser);

                }
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Validation Error");
            }
            catch (SecurityException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Authorize]
        public ActionResult GetCurrentUser()
        {
            var aUser = SecurityProvider.GetCurrentUser(Thread.CurrentPrincipal.Identity.Name);
            CurrentUser = new Models.Account.CurrentUserModel(aUser);

            return SerializeToAngular(CurrentUser);
        }


        [HttpPost]
        public ActionResult SignOut(int? Id)
        {
            var response = new HttpStatusCodeResult(HttpStatusCode.Created);
            FormsAuthentication.SignOut();
            Session.Abandon();
            CurrentUser = null;
            return response;
        }

        [HttpPost]
        [Authorize]
        public ActionResult Panic(PanicModel model)
        {
            IPanicProvider provider = new PanicProvider(Context, CurrentUser);
            provider.SavePanic(model.Lat, model.Lon);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

    }

 
}
