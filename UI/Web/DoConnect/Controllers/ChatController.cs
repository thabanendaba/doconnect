﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DoConnect.BL.Context;
using DoConnect.BL.Provider.Security;
using DoConnect.Models.Chat;

namespace DoConnect.Controllers
{
    [Authorize]
    [Filters.NoCache]
    public class ChatController : TCRControllerBase
    {

        #region Ctor

        public ISecurityProvider SecurityProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public ChatController()
        {
            DataContext = new DataContext();
            SecurityProvider = new SecurityProvider(DataContext, CurrentUser);
        }

        public ChatController(ISecurityProvider securityProvider)
        {
            _MustDisposeContext = false;
            SecurityProvider = securityProvider;

        }


        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }


        #endregion

        // GET: Chat
        [HttpGet]
        public ActionResult GetDoctors()
        {
            try
            {
                var Doctors = SecurityProvider.GetAllDoctors().Select(d => new DoctorsViewModel
                {
                    userIdentityId = d.UserId,
                    doctorDisplayName = d.DisplayName
                }).ToList();


                return SerializeToAngular(Doctors);
            }
            catch (SecurityException e)
            {

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}