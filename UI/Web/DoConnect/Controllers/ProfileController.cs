﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using DoConnect.Models;
using System.Web.Mvc;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.Security;
using DoConnect.Controllers.Filters;
using DoConnect.Models.Account;

namespace DoConnect.Controllers
{
    [Authorize]
    [Filters.NoCache]
    public class ProfileController : TCRControllerBase
    {

        #region Ctor

        public ISecurityProvider SecurityProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public ProfileController()
        {
            DataContext = new DataContext();
            SecurityProvider = new SecurityProvider(DataContext, CurrentUser);
        }

        public ProfileController(ISecurityProvider securityProvider)
        {
            _MustDisposeContext = false;
            SecurityProvider = securityProvider;

        }


        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }


        #endregion


        [HttpPost]
        public ActionResult ProfileEdit(ProfileViewModel model)
        {

            try
            {
                var profile = SecurityProvider.SaveMyProfile(model.GenderType, model.EmailAddress, model.Telephone,  model.Title, model.FirstName, model.Surname, model.Telephone, model.AddressLine1, model.AddressLine2, model.City, model.PostalCode);

                var model1 = new ProfileViewModel()
                {
                    UserName = profile.UserName,
                    GenderType = profile.Gender,
                    IdPassportNum = profile.IdPassportNum,
                    Surname = profile.Surname,
                    FirstName = profile.FirstName,
                    Title = profile.Title,
                    Telephone = profile.Telephone,
                    EmailAddress = profile.EmailAddress,
                    PostalCode = profile.PostalCode,
                    City = profile.City,
                    AddressLine1 = profile.AddressLine1,
                    AddressLine2 = profile.AddressLine2
                };

                return SerializeToAngular(model1);
            }
            catch (SecurityException e)
            {

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }


        [HttpPost]
        [RequiredPrivilege(PrivilegeType.DoctorMaintenance)]
        public ActionResult DoctorEdit(ProfileViewModel model)
        {

            try
            {
                var profile = SecurityProvider.DoctorEdit(model.UserId, model.UserName, model.EmailAddress, model.Title, model.FirstName, model.Surname, model.IdPassportNum, model.GenderType, model.Telephone, model.AddressLine1, model.AddressLine2, model.City, model.PostalCode);

                var model1 = new ProfileViewModel()
                {
                    UserName = profile.UserName,
                    GenderType = profile.Gender,
                    IdPassportNum = profile.IdPassportNum,
                    Surname = profile.Surname,
                    FirstName = profile.FirstName,
                    Title = profile.Title,
                    Telephone = profile.Telephone,
                    EmailAddress = profile.EmailAddress,
                    PostalCode = profile.PostalCode,
                    City = profile.City,
                    AddressLine1 = profile.AddressLine1,
                    AddressLine2 = profile.AddressLine2
                };

                return SerializeToAngular(model1);
            }
            catch (SecurityException e)
            {

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }


        [HttpGet]
        public ActionResult GetProfile()
        {
            try
            {
                var profile = SecurityProvider.GetMyProfile();
                var model = new ProfileViewModel()
                {
                    UserName = profile.UserName,
                    GenderType = profile.Gender,
                    IdPassportNum = profile.IdPassportNum,
                    Surname = profile.Surname,
                    FirstName = profile.FirstName,
                    Title = profile.Title,
                    Telephone = profile.Telephone,
                    EmailAddress = profile.EmailAddress,
                    PostalCode = profile.PostalCode,
                    City = profile.City,
                    AddressLine1 = profile.AddressLine1,
                    AddressLine2 = profile.AddressLine2
                };
                return SerializeToAngular(model);
            }
            catch (SecurityException e)
            {

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }




        }
    }
}