﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DoConnect.BL.Context;
using DoConnect.BL.Provider.Security;
using DoConnect.Models.Account;

namespace DoConnect.Controllers
{
    [Authorize]
    [Filters.NoCache]
    public class PasswordController: TCRControllerBase
    {
        public ISecurityProvider SecurityProvider { get; set; }
        public DataContext Context { get; set; }
        private bool _MustDisposeContext = true;

        public PasswordController()
        {
            Context = new DataContext();
            SecurityProvider = new SecurityProvider(Context,CurrentUser);
        }

        public PasswordController(ISecurityProvider securityProvider)
        {
            _MustDisposeContext = false;
            SecurityProvider = securityProvider;
        }

        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            try
            {

                var changePassword = SecurityProvider.ChangeMyPassword(model.OldPassword, model.NewPassword);

                var model1 = new ChangePasswordModel()
                {
                    NewPassword = changePassword.PasswordHash
                };

                return SerializeToAngular(model1);

            }
            catch (SecurityException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}