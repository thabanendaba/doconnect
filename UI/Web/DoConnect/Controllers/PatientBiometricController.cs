﻿using DoConnect.BL.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DoConnect.BL.Context;
using DoConnect.BL.Provider.MasterData;
using DoConnect.BL.Provider.PatientData;
using DoConnect.Models;
using DoConnect.Models.PatientBiometric;
using DoConnect.BL.Entities.MasterData;

namespace DoConnect.Controllers
{
    [Authorize]
    [Filters.NoCache]
    public class PatientBiometricController : TCRControllerBase
    {
        #region Ctor

        private IPatientProvider PatientProvider { get; set; }

        private IMasterDataProvider MasterDataProvider { get; set; }

        private DataContext DataContext { get; set; }

        private bool _MustDisposeContext = true;

        public PatientBiometricController()
        {
            DataContext = new DataContext();
            PatientProvider = new PatientProvider(DataContext, CurrentUser);
            MasterDataProvider = new MasterDataProvider(DataContext, CurrentUser);
        }

        public PatientBiometricController(IPatientProvider patientProvider, IMasterDataProvider masterDataProvider)
        {
            _MustDisposeContext = false;
            PatientProvider = patientProvider;
            MasterDataProvider = masterDataProvider;
        }

        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }

        #endregion

  
        [HttpPost]
        public ActionResult PatientBiometricSave(PatientBiometricViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error details");

                foreach (var bioItem in model.Items)
                {
                    //if (bioItem.Value == null)
                    //    bioItem.Value = 0;

                    PatientProvider.SavePatientBiometric(model.UserIdentityId, bioItem.BiometricId, bioItem.NumericValue , bioItem.StringValue, bioItem.YesNoValue);

                }
                return PatientBiometricGet(model.UserIdentityId);

            }
            catch (PatientBiometricException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpPost]
        public ActionResult GridPatients(IdGridModel model)
        {

            if (model.Id == null || model.Id == 0) 
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "No patient was selected...");

            int begin = SetupGridParams(model.GridModel);

            var filteredQuery = PatientProvider.GetBiometricsOutOfRangeForPatient(model.Id.Value)
                                    .Select(a => new MyBiometricGrid()
                                    {

                                        BiometricId = a.Id,
                                        BiometricName = a.Biometric.Name,
                                        BiometricType = a.Biometric.BiometricType,
                                        NumericValue = a.NumericValue,
                                        StringValue = a.StringValue,
                                        YesNoValue = a.YesNoValue,
                                        MaxValue = a.Biometric.MaxValue,
                                        MinValue = a.Biometric.MinValue
                                    });

            if (model.GridModel.Searchfor != "null")
            {
                filteredQuery = filteredQuery.Where(r => r.BiometricName.Contains(model.GridModel.Searchfor)
                                || r.BiometricType.ToString().Contains(model.GridModel.Searchfor)
                                || r.NumericValue.ToString().Contains(model.GridModel.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.GridModel.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.BiometricName); //default sort order

            if (!String.IsNullOrWhiteSpace(model.GridModel.SortKey))
                model.GridModel.SortKey = model.GridModel.SortKey.ToLower();

            switch (model.GridModel.SortOrder)
            {
                case "ASC":
                    switch (model.GridModel.SortKey)
                    {
                        case "biometricname":
                            filteredQuery = filteredQuery.OrderBy(r => r.BiometricName);
                            break;
                    }
                    break;
                case "DESC":
                    switch (model.GridModel.SortKey)
                    {
                        case "biometricname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.BiometricName);
                            break;
                       
                    }
                    break;
            }

            filteredQuery = filteredQuery.Skip(begin).Take(model.GridModel.RecordsPerPage.Value);

            return SerializeToAngular(new GridResultModel<MyBiometricGrid>(filteredQuery.ToList(), totalNumberOfRecords));
           
        }

        //HTTP GET /PatienBiometric/PatientBiometricGet/userIdentityId
        public ActionResult PatientBiometricGet(long? id)
        {
            if (id == null || id == 0)
                id = CurrentUser.Id;

            var model = new PatientBiometricViewModel();
            model.UserIdentityId = id.Value;

            model.Items = MasterDataProvider.GetBiometric().Where(a => a.Status == BL.Types.StatusType.Active)
                .Select(a => new BiometricItemModel()
                {
                    BiometricId = a.Id,
                    BiometricName = a.Name,
                    MinValue = a.MinValue,
                    MaxValue = a.MaxValue,
                    BiometricType = a.BiometricType
                    
                   
                }).ToList();

            var patientValues = PatientProvider.GetPatientBiometric().Where(a => a.Patient.UserIdentityId == model.UserIdentityId).Select(
                a => new
                {
                    a.BiometricId,
                    a.NumericValue,
                    a.StringValue,
                    a.YesNoValue
                }).ToList();

            foreach(var itm in patientValues)
            {
                var modelItem = model.Items.SingleOrDefault(a => a.BiometricId == itm.BiometricId);
                if (modelItem != null)
                {
                    modelItem.StringValue = itm.StringValue;
                    modelItem.NumericValue = itm.NumericValue;
                    modelItem.YesNoValue = itm.YesNoValue;
                }


            }

            return SerializeToAngular(model);
        }
       
    }
}