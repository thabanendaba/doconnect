﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Provider.ChatData;
using DoConnect.BL.Provider.Security;
using DoConnect.Models.Chat;

namespace DoConnect.Controllers.SignalR
{
    [HubName("chat")]
    public class MultiUserHub : Hub
    {
        public void SendMessage(ChatViewModel chat)
        {
            if(chat.RecieverId != null)
            {
                using (var db = new DataContext())
                {
                    IChatProvider chatProvider = new ChatProvider(db);
                    chatProvider.SaveChat(chat.SenderId, chat.RecieverId, chat.Subject);

                    ISecurityProvider provider = new SecurityProvider(db);
                    var receiverConnectionIds = provider.SignalRConnectionIds(chat.RecieverId.Value);
                    foreach (var connectionId in receiverConnectionIds)
                    {
                        if(connectionId != Context.ConnectionId) //dont send to myself
                            Clients.Client(connectionId).newMessage(chat);
                    }
                }
               
            }
          
        }

        public void SendNotification(NotificationModel notification)
        {
            if (notification.RecieverId != null)
            {
                using (var db = new DataContext())
                {
                    ISecurityProvider provider = new SecurityProvider(db);
                    var receiverConnectionIds = provider.SignalRConnectionIds(notification.RecieverId.Value);
                    foreach (var connectionId in receiverConnectionIds)
                    {
                        if (connectionId != Context.ConnectionId) //dont send to myself
                            Clients.Client(connectionId).addNotification(notification);
                    }
                }
                Clients.Others.newNotification(notification);
            }

        }

        public override Task OnConnected()
        {
            UpdateConnetionStatus(true);
            return base.OnConnected();
        }

        private bool UpdateConnetionStatus(bool connected)
        {
            var name = Context.User.Identity.Name;
            var connectionId = Context.ConnectionId;
            var userAgent = Context.Request.Headers["User-Agent"];
            using (var db = new DataContext())
            {
                ISecurityProvider provider = new SecurityProvider(db);
                var user = provider.SignalRConnected(name, connectionId, userAgent, connected);

                //if (user.Doctors.Count > 0)
                //    Clients.Others.newDoctor(new ChatUser {
                //        ConnectionId = connectionId,
                //        UserId = user.Id,
                //        UserName = name,
                //        DisplayName = user.FirstName + " " + user.Surname
                //    });

                if (connected)
                {
                    Clients.Others.newDoctor(
                        new DoctorsViewModel
                            {
                                userIdentityId = user.Id,
                                doctorDisplayName = user.FirstName + " " + user.Surname
                            }
                   );
                }
            }
            return true;// false;
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            UpdateConnetionStatus(false);

            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            UpdateConnetionStatus(true);

            return base.OnReconnected();
        }

        public static IHubContext GetInstance()
        {
            var result = GlobalHost.ConnectionManager.GetHubContext<MultiUserHub>();
            return result;
        }
    }
}
