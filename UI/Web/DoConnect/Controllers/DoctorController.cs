﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Web.Security;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.DoctorData;
using DoConnect.BL.Provider.PatientData;
using DoConnect.BL.Provider.Security;
using DoConnect.Controllers.Filters;
using DoConnect.Controllers.SignalR;
using DoConnect.Models;
using DoConnect.Models.Account;
using DoConnect.Models.Doctor;
using DoConnect.Models.PatientBiometric;
using DoConnect.BL.Entities.PatientData;
using DoConnect.BL.Provider;
using DoConnect.Models.SecurityData;
using Microsoft.AspNet.SignalR;

namespace DoConnect.Controllers
{
    [System.Web.Mvc.Authorize]
    [Filters.NoCache]
    public class DoctorController : TCRControllerBase
    {
        #region Ctor

        public ISecurityProvider SecurityProvider { get; set; }
        public IPatientProvider PatientProvider { get; set; }
        public IDoctorProvider DoctorProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public DoctorController()
        {
            DataContext = new DataContext();
            SecurityProvider = new SecurityProvider(DataContext, CurrentUser);
            DoctorProvider = new DoctorProvider(DataContext, CurrentUser);
            PatientProvider = new PatientProvider(DataContext, CurrentUser);

        }

        public DoctorController(ISecurityProvider securityProvider, DoctorProvider doctorProvider)
        {
            _MustDisposeContext = false;
            SecurityProvider = securityProvider;
            DoctorProvider = doctorProvider;
        }

        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }
        #endregion


        [HttpPost]
        public ActionResult DoctorListGrid(GridModel model)
        {

            int begin = SetupGridParams(model);


            var filteredQuery = DoctorProvider.DoctorIdentityList().Select(a => new DoctorIdentityGridModel()
            {
                Id = a.Id,
                UserName = a.UserName,
                FirstName = a.FirstName,
                Surname = a.Surname,
                Telephone = a.Telephone,
                EmailAddress = a.EmailAddress
            });

            //SecurityProvider.GetUserList()

            if (model.Searchfor != "null")
            {
                filteredQuery = filteredQuery.Where(r => r.UserName.Contains(model.Searchfor)
                                                    || r.FirstName.Contains(model.Searchfor)
                                                    );
            }
            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.UserName); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            //Setup sort order
            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "username":
                            filteredQuery = filteredQuery.OrderBy(r => r.UserName);
                            break;
                        case "firstname":
                            filteredQuery = filteredQuery.OrderBy(r => r.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderBy(r => r.Surname);
                            break;
                        case "telephone":
                            filteredQuery = filteredQuery.OrderBy(r => r.Telephone);
                            break;
                        case "emailaddress":
                            filteredQuery = filteredQuery.OrderBy(r => r.EmailAddress);
                            break;
                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "username":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.UserName);
                            break;
                        case "firstname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Surname);
                            break;
                        case "telephone":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Telephone);
                            break;
                        case "emailaddress":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.EmailAddress);
                            break;
                    }
                    break;
            }

            //setup paging
            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);


            return SerializeToAngular(new GridResultModel<DoctorIdentityGridModel>(filteredQuery.ToList(), totalNumberOfRecords));
        }

        [HttpPost]
        public ActionResult PatientsWithProblemsGrid(GridModel model)
        {

            int begin = SetupGridParams(model);

            var filteredQuery = PatientProvider.GetAllPatientsWithAlerts().Select(a => new PatientsWithProblemsGridModel()
            {
                UserIdentityId = a.UserIdentityId,
                FirstName = a.FirstName,
                Surname = a.Surname,
                Telephone = a.Telephone,
                EmailAddress = a.EmailAddress,
                LastUpdate = a.LastUpdate
            });

            if (model.Searchfor != "null")
            {
                filteredQuery = filteredQuery.Where(r => r.Surname.Contains(model.Searchfor)
                                                    || r.FirstName.Contains(model.Searchfor)
                                                    );
            }
            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.LastUpdate); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            //Setup sort order
            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "firstname":
                            filteredQuery = filteredQuery.OrderBy(r => r.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderBy(r => r.Surname);
                            break;
                        case "telephone":
                            filteredQuery = filteredQuery.OrderBy(r => r.Telephone);
                            break;
                        case "emailaddress":
                            filteredQuery = filteredQuery.OrderBy(r => r.EmailAddress);
                            break;
                        case "lastupdate":
                            filteredQuery = filteredQuery.OrderBy(r => r.LastUpdate);
                            break;
                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "firstname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Surname);
                            break;
                        case "telephone":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Telephone);
                            break;
                        case "emailaddress":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.EmailAddress);
                            break;
                        case "lastupdate":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.LastUpdate);
                            break;
                    }
                    break;
            }

            //setup paging
            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return SerializeToAngular(new GridResultModel<PatientsWithProblemsGridModel>(filteredQuery.ToList(), totalNumberOfRecords));
        }

        [HttpPost]
        public ActionResult DoctorProfileEdit(ProfileViewModel model)
        {

            try
            {
                var profile = SecurityProvider.SaveMyProfile(model.GenderType, model.EmailAddress, model.Telephone, model.Title, model.FirstName, model.Surname, model.Telephone, model.AddressLine1, model.AddressLine2, model.City, model.PostalCode);

                var model1 = new ProfileViewModel()
                {
                    UserName = profile.UserName,
                    GenderType = profile.Gender,
                    IdPassportNum = profile.IdPassportNum,
                    Surname = profile.Surname,
                    FirstName = profile.FirstName,
                    Title = profile.Title,
                    Telephone = profile.Telephone,
                    EmailAddress = profile.EmailAddress,
                    PostalCode = profile.PostalCode,
                    City = profile.City,
                    AddressLine1 = profile.AddressLine1,
                    AddressLine2 = profile.AddressLine2
                };

                return SerializeToAngular(model1);
            }
            catch (SecurityException e)
            {

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }


        [HttpGet]
        public ActionResult GetDoctorProfile(long? id)
        {
            try
            {
                var profile = SecurityProvider.GetUser(id.Value);
                ProfileViewModel model = new ProfileViewModel()
                {
                    UserName = profile.UserName,
                    GenderType = profile.Gender,
                    IdPassportNum = profile.IdPassportNum,
                    Surname = profile.Surname,
                    FirstName = profile.FirstName,
                    Title = profile.Title,
                    Telephone = profile.Telephone,
                    EmailAddress = profile.EmailAddress,
                    PostalCode = profile.PostalCode,
                    City = profile.City,
                    AddressLine1 = profile.AddressLine1,
                    AddressLine2 = profile.AddressLine2
                };
                return SerializeToAngular(model);
            }
            catch (SecurityException e)
            {

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [RequiredPrivilege(PrivilegeType.DoctorMaintenance)]
        public ActionResult RegisterDoctor(RegisterViewModel model)
        {
            try
            {

                if (ModelState.IsValid)
                {

                    var user = SecurityProvider.SignUp(model.UserName, model.Password, model.Email,
                        model.Title, model.FirstName, model.Surname,
                        model.IDOrPassport, model.GenderType, model.Telephone,
                        model.AddressLine1, model.AddressLine2, model.City,
                        model.PostalCode);



                    var doctor = DoctorProvider.SaveDoctor(null, user.Id);

                    return SerializeToAngular(CurrentUser);

                }
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Validation Error");
            }
            catch (SecurityException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpPost]
        public ActionResult NotifyPatient(PatientChatRequestModel model)
        {
            try
            {

                var signalRHub = MultiUserHub.GetInstance();
                var connections = SecurityProvider.SignalRConnectionIds(model.UserIdentityId);

                var notification = new Models.Chat.NotificationModel()
                {
                    NotificationMessage = "You are at risk!",
                    NotificationType = BL.Entities.Types.NotificationType.Alert,
                    SenderName = CurrentUser.DisplayName,
                    SendUserId = CurrentUser.Id,
                    RecieverId = model.UserIdentityId
                };

                foreach (var connectionId in connections)
                {
                    signalRHub.Clients.Client(connectionId).newNotification(notification);
                }


                return new HttpStatusCodeResult(HttpStatusCode.OK);

            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }


        }

        [HttpPost]
        public ActionResult NotificationResponse(PatientChatResponseModel model)
        {
            try
            {

                var signalRHub = MultiUserHub.GetInstance();
                var connections = SecurityProvider.SignalRConnectionIds(model.DoctorUserIdentityId);

                var response = new Models.Chat.ChatRequestResponse()
                {
                    SendUserId = CurrentUser.Id,
                    RecieverId = model.DoctorUserIdentityId,
                    AcceptChat = model.AcceptChat
                };

                foreach (var connectionId in connections)
                {
                    signalRHub.Clients.Client(connectionId).chatRequestResponse(response);
                }


                if(model.AcceptChat)
                {
                    PatientProvider.ResetMyBiometrics(CurrentUser.Id);
                }

                return new HttpStatusCodeResult(HttpStatusCode.OK);

            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }

        [HttpPost]
        public ActionResult PatientNotificationResponse(PatientChatResponseModel model)
        {
            try
            {

                var signalRHub = MultiUserHub.GetInstance();
                var connections = SecurityProvider.SignalRConnectionIds(model.DoctorUserIdentityId);

                var response = new Models.Chat.ChatRequestResponse()
                {
                    SendUserId = CurrentUser.Id,
                    RecieverId = model.DoctorUserIdentityId,
                    AcceptChat = model.AcceptChat
                };

                foreach (var connectionId in connections)
                {
                    signalRHub.Clients.Client(connectionId).chatRequestResponse(response);
                }


                return new HttpStatusCodeResult(HttpStatusCode.OK);

            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }

        }

        [HttpGet]
        public ActionResult GetSenderName(long id)
        {
            try
            {
                var profile = SecurityProvider.GetUser(id);

                var model = new ProfileViewModel()
                {
                    
                    Surname = profile.Surname,
                    FirstName = profile.FirstName,
                    
                };
                return SerializeToAngular(model);
            }
            catch (SecurityException e)
            {

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            };
        }
    }
}