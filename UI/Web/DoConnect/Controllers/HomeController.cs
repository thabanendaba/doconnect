﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace DoConnect.Controllers
{
    [Filters.NoCache]
    public class HomeController : TCRControllerBase
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Medi Well - Docu Chat";

            ViewBag.Authenticated = Request.IsAuthenticated ? "true" : "false";
            ViewBag.UserName = string.Empty;

            if (Request.IsAuthenticated)
                ViewBag.UserName = Thread.CurrentPrincipal.Identity.Name;
            

            return View();
        }

       


    }
}