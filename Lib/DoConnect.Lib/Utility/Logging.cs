﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Xml;
namespace EXO.Lib.Utility
{
    public class Logging
    {

        private System.Diagnostics.EventLog eventLog1 = new EventLog();
        private string logFileName = "";
        private string errorlogFileName = "";
        private string errorlogFileFolder = "";
        private bool logToTextFile = false;

        public string EventLogSource = "AttachmentImport Event Log";
        public string EventLogName = "AttachmentImport";

        public enum LogTypes
        {
            Progress,
            Status,
            FTP,
            FileProcess
        }

        public Logging()
        {
            readConfiguration();
            readOwnValues();
            try
            {
                if (!System.Diagnostics.EventLog.SourceExists(EventLogSource))
                {
                    System.Diagnostics.EventLog.CreateEventSource(EventLogSource, EventLogName);
                }
                //now assign this new eventlog to the local event logger:
                eventLog1.Source = EventLogSource;
                eventLog1.Log = EventLogName;
            }
            catch (Exception ex)
            {
                string _t = ex.ToString();
            }
            
        }

        private void readOwnValues()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            
            string settings = ReadConfig(asm, "LogFileName");
        }

        private string readConfigValue(string _skeyName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();

            return ReadConfig(asm, _skeyName);
        }

        private void readConfiguration()
        {
            try
            {

                EventLogSource = readConfigValue("EventLogSource");
                EventLogName = readConfigValue("EventLogName");
                logFileName = readConfigValue("LogFileName");
                errorlogFileName = readConfigValue("ErrorLogFileName");
                errorlogFileFolder = readConfigValue("ErrorLogFileFolder");
                logToTextFile = Convert.ToBoolean(readConfigValue("LogToTextFile"));

                //EventLogSource = readConfigurationValue("EventLogSource");
                //EventLogName = readConfigurationValue("EventLogName");
                //logFileName = readConfigurationValue("LogFileName");
                //errorlogFileName = readConfigurationValue("ErrorLogFileName");
                //errorlogFileFolder = readConfigurationValue("ErrorLogFileFolder");
                //logToTextFile = Convert.ToBoolean(readConfigurationValue("LogToTextFile"));

            }
            catch (Exception)
            {

            }

        }

        private string readConfigurationValue(string _skeyName)
        {
            string _sValue = "";
            // Get the AppSettings section.
            NameValueCollection appSettings = System.Configuration.ConfigurationManager.AppSettings;
            for (int _i = 0; _i < appSettings.Count; _i++)
            {
                if (appSettings.GetKey(_i) == _skeyName)
                {
                    _sValue = appSettings[_i];
                    break;
                }
            }
            return _sValue;
        }

        public static string ReadConfig(Assembly asm, string keyName)
        {
            string settingValue = "";
            try
            {
                //string to hold the name of the 
                //config file for the assembly
                string cfgFile = asm.CodeBase + ".config";

                //create a new XML Document
                XmlDocument doc = new XmlDocument();
                //load an XML document by using the
                //XMLTextReader class of the XML Namespace
                //yo open the sfgFile
                doc.Load(new XmlTextReader(cfgFile));
                //retrieve a list of nodes in the document
                XmlNodeList nodes = doc.GetElementsByTagName("appSettings");
                //now we need to loop through all the
                //nodes in the XML document
                foreach (XmlNode node in nodes)
                {
                    bool found = false;
                    foreach (XmlNode item in node.ChildNodes)
                    {
                        foreach (XmlAttribute att in item.Attributes)
                        {
                            if (found)
                            {
                                settingValue = att.Value;
                                break;
                            }
                            if (att.Value == keyName)
                            {

                                found = true;
                            }
                        }
                        if (found)
                            break;
                    }
                }
                return settingValue;

            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return settingValue;
            }

        }

        public void Record(string message, EventLogEntryType eventLogEntryType, int eventid)
        {
            try
            {
                //event log
                if (message.Length > 32766)
                {
                    message = message.Substring(0, 32766);
                }
                try
                {
                    eventLog1.WriteEntry(message, eventLogEntryType, eventid);
                }
                catch  
                {
 
                }
                

                //text file
                if (logToTextFile)
                {
                    try
                    {
                        WriteTextLogEvent(eventLogEntryType, message);
                    }
                    catch { }
                   

                }
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                throw;
            }

        }

        public void Record(string message, EventLogEntryType eventLogEntryType, int eventid, short category)
        {
            try
            {
                //event log
                if (message.Length > 32766)
                {
                    message = message.Substring(0, 32766);
                }
                eventLog1.WriteEntry(message, eventLogEntryType, eventid, category);
                //text file
                if (logToTextFile)
                {
                    WriteTextLogEvent(eventLogEntryType, message);
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public void Record(string message, EventLogEntryType eventLogEntryType, int eventid, LogTypes logType)
        {
            try
            {
                //event log
                if (message.Length > 32766)
                {
                    message = message.Substring(0, 32766);
                }
                eventLog1.WriteEntry(message, eventLogEntryType, eventid);

                //text file
                if (logToTextFile)
                {
                    WriteTextLogEvent(eventLogEntryType, message);
                }
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                throw;
            }
        }

        private void WriteTextLogEvent(EventLogEntryType eventLogEntryType, string msg)
        {
            try
            {
                string path = @"C:"; // System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                path = errorlogFileFolder;
                string _sLogFile = "";

                string _sReplace = "";

                _sReplace = eventLogEntryType.ToString();

                switch (eventLogEntryType)
                {
                    case EventLogEntryType.Error:
                        _sReplace = "_Error.";
                        break;
                    case EventLogEntryType.FailureAudit:
                        _sReplace = "_Failure.";
                        break;
                    case EventLogEntryType.Information:
                        _sReplace = "_Information.";
                        break;
                    case EventLogEntryType.SuccessAudit:
                        _sReplace = "_Success.";
                        break;
                    case EventLogEntryType.Warning:
                        _sReplace = "_Warning.";
                        break;
                    default:
                        _sReplace = ".";
                        break;
                }

                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                _sLogFile = path + @"\" + logFileName;
                _sLogFile = _sLogFile.Replace(".", _sReplace);

                checkLogFileSize(_sLogFile);

                FileMode fm = FileMode.OpenOrCreate;
                //if (eventLogEntryType == EventLogEntryType.Warning)
                //    if (File.Exists(_sLogFile))
                //        try
                //        {
                //            File.Delete(_sLogFile);
                //        }
                //        catch { }

                FileStream fs = new FileStream(_sLogFile, fm, FileAccess.Write);
                StreamWriter m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                m_streamWriter.WriteLine(DateTime.Now.ToString() + ": " + msg);
                m_streamWriter.Flush();
                m_streamWriter.Close();
            }
            catch
            {
                //TODO: to avoid contention when two threads are trying to update this file at the same time - maybe retry?
            }
        }

        private void checkLogFileSize(string _sFilename)
        {
            try
            {

                // string _sFilename = @"C:" + @"\" + logFileName;
                if (File.Exists(_sFilename))
                {
                    FileInfo f = new FileInfo(_sFilename);
                    long s1 = f.Length;

                    if (s1 / 1024 / 1024 > 10)
                    {
                        backupLogTextFile(_sFilename);
                        clearTextLogFile(_sFilename);
                    }
                }
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
            }
        }

        private void backupLogTextFile(string _sFilename)
        {
            try
            {
                //string fileName = @"C:" + @"\" + logFileName ;
                string backup = _sFilename.Replace(".", ".bak");
                //if (File.Exists(backup))
                //{
                //    File.Delete(backup);
                //}
                File.Copy(_sFilename, backup, true);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string ReadTextLogFile()
        {
            try
            {
                string path = @"C:"; ; // System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                FileStream fs = new FileStream(path + @"\" + logFileName, FileMode.Open, FileAccess.Read);
                using (StreamReader m_streamReader = new StreamReader(fs))
                {
                    return m_streamReader.ReadToEnd();
                }
            }
            catch
            {
                return "";

            }
        }

        public void clearTextLogFile(string _sFilename = "")
        {
            try
            {
                //string path = @"C:"; // System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                //FileInfo f = new FileInfo(path + @"\" + logFileName);

                if (_sFilename == "")
                    _sFilename = readConfigurationValue("LogFileName");

                FileInfo f = new FileInfo(_sFilename);
                FileStream fs = f.Open(FileMode.Truncate, FileAccess.ReadWrite);
                fs.Close();

            }
            catch
            {


            }
        }

        //public string GetStackMethodAndError(Exception ex)
        //{
        //    StringBuilder _sBuilder = new StringBuilder();

        //    _sBuilder.AppendLine(ex.StackTrace.

        //    return _sBuilder.ToString();
        //}

    }
}