﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace EXO.Lib.Utility
{
    public class DateUtilities
    {
        #region ENUMS

        public enum Quarter
        {
            First = 1,
            Second = 2,
            Third = 3,
            Fourth = 4
        }

        public enum Month
        {
            January = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12
        }

        public enum MonthAbbrev
        {
            Jan = 1,
            Feb = 2,
            Mar = 3,
            Apr = 4,
            May = 5,
            Jun = 6,
            Jul = 7,
            Aug = 8,
            Sep = 9,
            Oct = 10,
            Nov = 11,
            Dec = 12
        }

        public enum DateInterval
        {
            Year,
            //BiAnnual,
            //Quarters,
            Months,
            Weekday,
            Day,
            Hour,
            Minute,
            Second
        }

        private enum TimeSpanElement
        {
            Millisecond,
            Second,
            Minute,
            Hour,
            Day
        }


        #endregion

        public static List<PublicHolidays> PublicHolidayList = new List<PublicHolidays>();

        #region Quarters

        public static DateTime GetStartOfQuarter(int Year, Quarter Qtr)
        {
            DateTime _dt = DateTime.Now;
            switch (Qtr)
            {
                case Quarter.First:    // 1st Quarter = January 1 to March 31
                    _dt = new DateTime(Year, 1, 1, 0, 0, 0, 0);
                    break;
                case Quarter.Second: // 2nd Quarter = April 1 to June 30
                    _dt = new DateTime(Year, 4, 1, 0, 0, 0, 0);
                    break;
                case Quarter.Third: // 3rd Quarter = July 1 to September 30
                    _dt = new DateTime(Year, 7, 1, 0, 0, 0, 0);
                    break;
                case Quarter.Fourth: // 4th Quarter = October 1 to December 31
                    _dt = new DateTime(Year, 10, 1, 0, 0, 0, 0);
                    break;
            }
            return _dt;
        }

        public static DateTime GetEndOfQuarter(int Year, Quarter Qtr)
        {
            DateTime _dt = DateTime.Now;
            switch (Qtr)
            {
                case Quarter.First:  // 1st Quarter = January 1 to March 31
                    _dt = new DateTime(Year, 3, DateTime.DaysInMonth(Year, 3), 23, 59, 59);
                    break;
                case Quarter.Second: // 2nd Quarter = April 1 to June 30
                    _dt = new DateTime(Year, 6, DateTime.DaysInMonth(Year, 6), 23, 59, 59);
                    break;
                case Quarter.Third: // 3rd Quarter = July 1 to September 30
                    _dt = new DateTime(Year, 9, DateTime.DaysInMonth(Year, 9), 23, 59, 59);
                    break;
                case Quarter.Fourth: // 4th Quarter = October 1 to December 31
                    _dt = new DateTime(Year, 12, DateTime.DaysInMonth(Year, 12), 23, 59, 59);
                    break;
            }
            return _dt;
        }

        public static Quarter GetQuarter(Month Month)
        {
            if (Month <= Month.March)
                // 1st Quarter = January 1 to March 31
                return Quarter.First;
            else if ((Month >= Month.April) && (Month <= Month.June))
                // 2nd Quarter = April 1 to June 30
                return Quarter.Second;
            else if ((Month >= Month.July) && (Month <= Month.September))
                // 3rd Quarter = July 1 to September 30
                return Quarter.Third;
            else // 4th Quarter = October 1 to December 31
                return Quarter.Fourth;
        }

        public static int GetQuarter(int Month)
        {
            if (Month <= 3)
                // 1st Quarter = January 1 to March 31
                return 1;
            else if ((Month >= 4) && (Month <= 6))
                // 2nd Quarter = April 1 to June 30
                return 2;
            else if ((Month >= 7) && (Month <= 9))
                // 3rd Quarter = July 1 to September 30
                return 3;
            else // 4th Quarter = October 1 to December 31
                return 4;
        }

        public static int GetQuarterStartMonth(int Month)
        {
            int quarter = GetQuarter(Month);
            switch (quarter)
            {
                case 1:
                    Month = 1;
                    break;
                case 2:
                    Month = 4;
                    break;
                case 3:
                    Month = 7;
                    break;
                case 4:
                    Month = 10;
                    break;
            }
            return Month;
        }

        public static int GetQuarterEndMonth(int Month)
        {
            int quarter = GetQuarter(Month);
            switch (quarter)
            {
                case 1:
                    Month = 3;
                    break;
                case 2:
                    Month = 6;
                    break;
                case 3:
                    Month = 9;
                    break;
                case 4:
                    Month = 12;
                    break;
            }
            return Month;
        }

        public static DateTime GetEndOfLastQuarter()
        {
            if ((Month)DateTime.Now.Month <= Month.March)
                //go to last quarter of previous year
                return GetEndOfQuarter(DateTime.Now.Year - 1, Quarter.Fourth);
            else //return last quarter of current year
                return GetEndOfQuarter(DateTime.Now.Year,
                  GetQuarter((Month)DateTime.Now.Month));
        }

        public static DateTime GetStartOfLastQuarter()
        {
            if ((Month)DateTime.Now.Month <= Month.March)
                //go to last quarter of previous year
                return GetStartOfQuarter(DateTime.Now.Year - 1, Quarter.Fourth);
            else //return last quarter of current year
                return GetStartOfQuarter(DateTime.Now.Year,
                  GetQuarter((Month)DateTime.Now.Month));
        }

        public static DateTime GetStartOfCurrentQuarter()
        {
            return GetStartOfQuarter(DateTime.Now.Year,
                   GetQuarter((Month)DateTime.Now.Month));
        }

        public static DateTime GetEndOfCurrentQuarter()
        {
            return GetEndOfQuarter(DateTime.Now.Year,
                   GetQuarter((Month)DateTime.Now.Month));
        }

        #endregion

        #region Bi-Annual

        public static int GetBiAnnualStartMonth(int month)
        {
            if (month <= 6)
                return 1;
            else
                return 7;
        }

        public static int GetBiAnnualEndMonth(int month)
        {
            if (month <= 6)
                return 6;
            else
                return 12;
        }

        #endregion

        #region Weeks

        public static DateTime GetStartOfLastWeek()
        {
            int DaysToSubtract = (int)DateTime.Now.DayOfWeek + 7;
            DateTime dt =
              DateTime.Now.Subtract(System.TimeSpan.FromDays(DaysToSubtract));
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfLastWeek()
        {
            DateTime dt = GetStartOfLastWeek().AddDays(6);
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
        }

        public static DateTime GetStartOfCurrentWeek()
        {
            int DaysToSubtract = (int)DateTime.Now.DayOfWeek;
            DateTime dt =
              DateTime.Now.Subtract(System.TimeSpan.FromDays(DaysToSubtract));
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfCurrentWeek()
        {
            DateTime dt = GetStartOfCurrentWeek().AddDays(6);
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
        }

        #endregion

        #region Months

        public static string GetMonthNameFull(DateTime dt)
        {
            return dt.ToString("MMMM");
        }

        public static string GetMonthNameShort(DateTime dt)
        {
            return dt.ToString("MMM");
        }

        public static DateTime GetStartOfMonth(int Month, int Year)
        {
            return new DateTime(Year, Month, 1, 0, 0, 0, 0);
        }

        public static DateTime GetStartOfMonth(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, 1, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfMonth(int Month, int Year)
        {
            return new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month), 23, 59, 59, 0);
        }

        public static DateTime GetEndOfMonth(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month), 23, 59, 59, 0);
        }

        public static DateTime GetStartOfLastMonth()
        {
            if (DateTime.Now.Month == 1)
                return GetStartOfMonth(12, DateTime.Now.Year - 1);
            else
                return GetStartOfMonth(DateTime.Now.Month - 1, DateTime.Now.Year);
        }

        public static DateTime GetEndOfLastMonth()
        {
            if (DateTime.Now.Month == 1)
                return GetEndOfMonth(12, DateTime.Now.Year - 1);
            else
                return GetEndOfMonth(DateTime.Now.Month - 1, DateTime.Now.Year);
        }

        public static DateTime GetStartOfCurrentMonth()
        {
            return GetStartOfMonth(DateTime.Now.Month, DateTime.Now.Year);
        }

        public static DateTime GetEndOfCurrentMonth(int year, int month)
        {
            return GetEndOfMonth(month, year);
        }

        #endregion

        #region Years

        public static DateTime GetStartOfYear(int Year)
        {
            return new DateTime(Year, 1, 1, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfYear(int Year)
        {
            return new DateTime(Year, 12,
              DateTime.DaysInMonth(Year, 12), 23, 59, 59);
        }

        public static DateTime GetStartOfLastYear()
        {
            return GetStartOfYear(DateTime.Now.Year - 1);
        }

        public static DateTime GetEndOfLastYear()
        {
            return GetEndOfYear(DateTime.Now.Year - 1);
        }

        public static DateTime GetStartOfCurrentYear()
        {
            return GetStartOfYear(DateTime.Now.Year);
        }

        public static DateTime GetEndOfCurrentYear()
        {
            return GetEndOfYear(DateTime.Now.Year);
        }
        #endregion

        #region Days

        public static DateTime GetStartOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }

        public static int GetWeekDayNumber(DateTime dt)
        {
            return (int)dt.DayOfWeek;
        }

        public static string GetDayNameFull(DateTime dt)
        {
            return dt.ToString("dddd");
        }

        public static string GetDayNameShort(DateTime dt)
        {
            return dt.ToString("ddd");
        }

        public static long DateDiff(DateInterval interval, DateTime date1, DateTime date2)
        {

            TimeSpan ts = date2 - date1;

            switch (interval)
            {
                case DateInterval.Year:
                    return date2.Year - date1.Year;
                case DateInterval.Months:
                    return (date2.Month - date1.Month) + (12 * (date2.Year - date1.Year));
                case DateInterval.Weekday:
                    return fix(ts.TotalDays) / 7;
                case DateInterval.Day:
                    return fix(ts.TotalDays);
                case DateInterval.Hour:
                    return fix(ts.TotalHours);
                case DateInterval.Minute:
                    return fix(ts.TotalMinutes);
                default:
                    return fix(ts.TotalSeconds);
            }
        }

        public static string DateDiffFriendly(DateTime date1, DateTime date2)
        {
            string _sReturn = string.Empty;

            TimeSpan ts = date2 - date1;

            if (date2.Year - date1.Year >= 100)
                _sReturn = "Centuries";
            else
                if (date2.Year - date1.Year >= 10)
                    _sReturn = "Decades";
                else
                    if (date2.Year - date1.Year >= 1)
                        _sReturn = "Years";
                    else
                        if (date2.Month - date1.Month > 1)
                            _sReturn = "Months";
                        else
                            if (ts.TotalDays <= 1)
                                _sReturn = "Hours";
                            else
                                if (ts.TotalDays <= 31)
                                    _sReturn = "Days";
                                else
                                    if (date2.Day - date1.Day >= 1)
                                        _sReturn = "Hours";
                                    else
                                        if (date2.Hour - date1.Hour >= 1)
                                            _sReturn = "Hours";
                                        else
                                            _sReturn = "Minutes";

            return _sReturn;
        }



        public static int GetDayInWeek(DayOfWeek dayOfWeek, int _iWeek, int _iMonth)
        {
            int year = DateTime.Now.Year;
            CultureInfo ciNewFormat = new CultureInfo(CultureInfo.CurrentCulture.ToString());
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = (int)CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek - (int)jan1.DayOfWeek;
            DateTime beginningOfMonth = GetStartOfCurrentMonth();
            // ciNewFormat.Calendar.wee
            // beginningOfMonth = (date.Subtract(beginningOfMonth).Days / 7) + 1;

            int _iCountWeeks = 1;

            for (int i = 1; i <= ciNewFormat.Calendar.GetDaysInMonth(year, _iMonth); i++)
            {
                if (new DateTime(year, _iMonth, i).DayOfWeek == beginningOfMonth.DayOfWeek)
                {
                    _iCountWeeks++;
                }
                if (new DateTime(year, _iMonth, i).DayOfWeek == dayOfWeek)
                {
                    if (_iCountWeeks == _iWeek)
                    {
                        return new DateTime(year, _iMonth, i).Day;
                    }
                }
                //if (new DateTime(year, _iMonth, i). == _iWeek)
                //{
                //    if (new DateTime(DateTime.Now.Year, _iMonth, i).DayOfWeek.ToString() == _iDay)
                //    {

                //    }
                //}
            }

            return 0;
        }

        #endregion

        #region Time

        public static DateTime GetRoundUpTime(int _iRounded, DateTime dt)
        {
            DateTime _dt = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);

            int iMinutes = dt.Minute;



            return _dt;
        }

        public static DateTime GetNextWorkDay()
        {
            DateTime _date = DateTime.Now.AddDays(1);

            while (_date.DayOfWeek == DayOfWeek.Saturday || _date.DayOfWeek == DayOfWeek.Sunday)
            {
                _date = _date.AddDays(1);
            }

            for (int i = 0; i < 6; i++)
            {
                if (!isPublicHoliday(_date))
                    break;
                else
                    _date = _date.AddDays(1);
            }

            return _date;
        }

        private static bool isPublicHoliday(DateTime _date)
        {
            if (PublicHolidayList.Count == 0)
                return false;

            var query = from x in PublicHolidayList
                        where x.Date.Date == _date.Date
                        select x;

            return query.ToList().Count > 0;
        }

        #endregion

        #region Friendly Diff

        public static string FriendlyDiff(DateTime dt1, DateTime dt2)
        {

            TimeSpan ts = dt2.Subtract(dt1);


            int years = ts.Days / 365; //no leap year accounting
            int months = (ts.Days % 365) / 30; //naive guess at month size
            int weeks = ((ts.Days % 365) % 30) / 7;
            int days = (((ts.Days % 365) % 30) % 7);

            int hours = 0;
            int minutes = 0;

            if (ts.TotalDays <= 2)
            {
                hours = (ts.Hours % 24);
                minutes = (ts.Minutes);
            }

            //  string _sDisplayMulti = "s";
            StringBuilder sb = new StringBuilder();

            if (years > 0)
                sb.Append(years.ToString() + " year" + (years > 1 ? "s" : "") + ", ");

            if (months > 0)
                sb.Append(months.ToString() + " month" + (months > 1 ? "s" : "") + ", ");

            if (weeks > 0)
                sb.Append(weeks.ToString() + " week" + (weeks > 1 ? "s" : "") + ", ");

            if (days > 0)
                sb.Append(days.ToString() + " day" + (days > 1 ? "s" : "") + ", ");


            if (days <= 1)
                if (months <= 0)
                    if (weeks <= 0)
                        if (years <= 0)
                        {
                            if (hours > 0)
                                sb.Append(hours.ToString() + " hour" + (hours > 1 ? "s" : "") + ", ");
                            if (minutes > 0)
                                sb.Append(minutes.ToString() + " minute" + (minutes > 1 ? "s" : "") + ", ");
                        }
            string _sTemp = sb.ToString();
            if (!string.IsNullOrEmpty(_sTemp))
                if (_sTemp.Substring(_sTemp.Length - 2, 2).Contains(", "))
                {
                    _sTemp = _sTemp.Substring(0, _sTemp.Length - 2) + ".";
                }

            return _sTemp;
        }

        #endregion

        #region Advanced

        public static bool IsDateBetween(DateTime dateTime, DateTime _dtStart, DateTime _dtEnd)
        {
            bool _bReturn = false;

            if ((dateTime <= _dtEnd && dateTime >= _dtStart))
            {
                _bReturn = true;
            }

            return _bReturn;
        }

        #endregion

        #region Private Methods

        private static long fix(double Number)
        {
            if (Number >= 0)
            {
                return (long)Math.Floor(Number);
            }
            return (long)Math.Ceiling(Number);
        }


        private static int WeekDay(DateTime dt)
        {
            // Set Year 
            int yyyy = dt.Year;
            // Set Month 
            int mm = dt.Month;
            // Set Day 
            int dd = dt.Day;
            // Declare other required variables 

            int DayOfYearNumber;
            int Jan1WeekDay;
            int WeekDay;
            int i, j, k, l;
            int[] Mnth = new int[12] { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };
            // Set DayofYear Number for yyyy mm dd 
            // DayOfYearNumber = dd + Mnth[mm - 1]; 
            DayOfYearNumber = 1 + Mnth[mm - 1];
            // Increase of Dayof Year Number by 1, if year is leapyear and month is february 
            if ((IsLeapYear(yyyy) == true) && (mm == 2))
                DayOfYearNumber += 1;
            // Find the Jan1WeekDay for year 
            i = (yyyy - 1) % 100;
            j = (yyyy - 1) - i;
            k = i + i / 4;
            Jan1WeekDay = 1 + (((((j / 100) % 4) * 5) + k) % 7);
            // Calcuate the WeekDay for the given date 
            l = DayOfYearNumber + (Jan1WeekDay - 1);
            WeekDay = 1 + ((l - 1) % 7);
            WeekDay = (dd + WeekDay) / 7;
            return (WeekDay + 1);

        }


        public static bool IsLeapYear(int yyyy)
        {
            if ((yyyy % 4 == 0 && yyyy % 100 != 0) || (yyyy % 400 == 0))
                return true;
            else
                return false;
        }


        #endregion

        #region Public Methods

        public bool AddSpecialDay(DateTime _dt, string _description)
        {
            PublicHolidayList.Add(new PublicHolidays { Date = _dt, Description = _description });
            return true;
        }

        #endregion

    }

    public class PublicHolidays
    {
        public DateTime Date { get; set; }
        public String Description { get; set; }
       
    }

    

}
