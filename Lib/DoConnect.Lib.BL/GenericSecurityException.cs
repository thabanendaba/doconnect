﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXO.Lib.BL
{
    public class GenericSecurityException :
     Exception
    {
        public GenericSecurityException(string message) :
            base(message)
        {

        }
    }
}
