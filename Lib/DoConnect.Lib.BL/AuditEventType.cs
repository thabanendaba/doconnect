﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXO.Lib.BL
{
    public enum AuditEventType
    {
        Insert,
        Modify,
        Delete
    }
}
