﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.Appointment;
using DoConnect.BL.Provider;
using DoConnect.BL.Provider.Security;

namespace PreSchool.BL.Provider.Schedule
{
    public class AppointmentProvider : DocConnectProvider, IAppointmentProvider
    {
        #region Constructor
        public AppointmentProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }
        #endregion

        #region Methods
        public Appointment GetAppointmentById(long id)
        {
            try
            {
                return DataContext.Appointment.SingleOrDefault(s => s.Id == id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Appointment> GetAppointments()
        {
            try
            {
                return DataContext.Appointment.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Appointment> GetAppointmentsByDate(DateTime date)
        {
            try
            {

                List<Appointment> lsAppointments = DataContext.Appointment?.ToList();

                if (lsAppointments.Any())
                {
                    foreach (Appointment appointment in lsAppointments)
                    {
                        if (appointment.From.Date != date.Date)
                            lsAppointments.Remove(appointment);
                    }
                }

                return lsAppointments;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<Appointment> GetAppointmentsByMonth(DateTime date)
        {
            try
            {

                List<Appointment> lsAppointments = DataContext.Appointment?.ToList();

                if (lsAppointments.Any())
                {
                    foreach (Appointment appointment in lsAppointments)
                    {
                        if (appointment.From.Year != date.Year && appointment.From.Month != date.Month)
                            lsAppointments.Remove(appointment);
                    }
                }

                return lsAppointments;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Appointment> GetAppointmentsByStatus(AppointmentStatus status)
        {
            try
            {
                return DataContext.Appointment.Where(a => a.Status == status).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Appointment SaveAppointment(Appointment appointment)
        {
            try
            {
                if (CheckIfAvailable(appointment))
                {
                    if (appointment.Id > 0)
                    {
                        //Update
                        Appointment updateAppointment = DataContext.Appointment.SingleOrDefault(a => a.Id == appointment.Id);

                        if (updateAppointment != null)
                        {
                            DataContext.Appointment.Attach(updateAppointment);
                            updateAppointment.Status = appointment.Status;
                            updateAppointment.From = appointment.From;
                            updateAppointment.To = appointment.To;
                            updateAppointment.Doctor = appointment.Doctor;
                            updateAppointment.DoctorUserIdentityId = appointment.DoctorUserIdentityId;
                            updateAppointment.Doctor = DataContext.DoctorSet.SingleOrDefault(d => d.Id == appointment.DoctorUserIdentityId);
                            updateAppointment.AppointmentType = appointment.AppointmentType;
                            //updateAppointment.DateStamp = appointment.DateStamp;
                            updateAppointment.Duration = appointment.To - appointment.From;

                            DataContext.SaveChanges();
                            return updateAppointment;
                        }
                    }
                    else
                    {
                        //New
                        Appointment newAppointment = new Appointment()
                        {
                            Duration = appointment.To - appointment.From,
                            From = appointment.From,
                            To = appointment.To,
                            DoctorUserIdentityId = appointment.DoctorUserIdentityId,
                            PatientUserIdentityId = appointment.PatientUserIdentityId,
                            Status = appointment.Status,
                            AppointmentType = appointment.AppointmentType,
                            DateStamp = appointment.DateStamp,
                            Patient = DataContext.PatientSet.SingleOrDefault(p => p.Id == appointment.PatientUserIdentityId),
                            Doctor = DataContext.DoctorSet.SingleOrDefault(d => d.Id == appointment.DoctorUserIdentityId)
                        };

                        newAppointment.Patient.Appointments.Add(newAppointment);
                        newAppointment.Doctor.Appointments.Add(newAppointment);
                        DataContext.Appointment.Add(newAppointment);
                        DataContext.SaveChanges();
                        return newAppointment;
                    }
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        public List<Appointment> GetMyAppointments(long Id)
        {
            try
            {
                return DataContext.Appointment.Where(a => a.DoctorUserIdentityId == Id || a.PatientUserIdentityId == Id).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Appointment> GetMyAppointmentsByDate(DateTime date, long staffId)
        {
            try
            {

                List<Appointment> lsAppointments = DataContext.Appointment?.ToList();

                if (lsAppointments.Any())
                {
                    foreach (Appointment appointment in lsAppointments)
                    {
                        if (appointment.From.Date != date.Date && appointment.PatientUserIdentityId != staffId || appointment.DoctorUserIdentityId != staffId)
                            lsAppointments.Remove(appointment);
                    }
                }

                return lsAppointments;

            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Appointment> GetMyAppointmentsByMonth(DateTime date, long staffId)
        {
            try
            {
                return DataContext.Appointment.Where(a => a.From.Month == date.Month && a.DoctorUserIdentityId == staffId).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool CheckIfAvailable(Appointment appointment)
        {
            try
            {
                List<Appointment> existingAppointments = DataContext.Appointment?.ToList();

                if (existingAppointments.Any())
                {
                    foreach (Appointment app in existingAppointments)
                    {
                        if (app.From.ToShortDateString() != appointment.From.ToShortDateString())
                            existingAppointments.Remove(app);
                    }
                    if (existingAppointments == null)
                        return true;
                    else
                    {
                        if (existingAppointments.Any(t => t.From.TimeOfDay == appointment.From.TimeOfDay))
                            return false;
                        else
                        {
                            foreach (Appointment existingAppointment in existingAppointments)
                            {
                                if (appointment.From.TimeOfDay > existingAppointment.From.TimeOfDay && appointment.From.TimeOfDay < existingAppointment.To.TimeOfDay)
                                    return false;
                                if (appointment.To.TimeOfDay > existingAppointment.To.TimeOfDay && appointment.To.TimeOfDay < existingAppointment.To.TimeOfDay)
                                    return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
