﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.Appointment;
using DoConnect.BL.Provider;

namespace PreSchool.BL.Provider.Schedule
{
    public interface IAppointmentProvider : IDocConnectProvider
    {
        #region Appointment

        Appointment GetAppointmentById(long id);
        List<Appointment> GetAppointments();
        List<Appointment> GetMyAppointments(long staffId);
        List<Appointment> GetMyAppointmentsByDate(DateTime date, long staffId);
        List<Appointment> GetMyAppointmentsByMonth(DateTime date, long staffId);
        List<Appointment> GetAppointmentsByDate(DateTime date);
        List<Appointment> GetAppointmentsByMonth(DateTime date);
        List<Appointment> GetAppointmentsByStatus(AppointmentStatus status);
        Appointment SaveAppointment(Appointment appointment);

        bool CheckIfAvailable(Appointment appointment);

        #endregion
    }
}
