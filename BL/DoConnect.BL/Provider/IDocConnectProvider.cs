﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.Security;
using EXO.Lib.BL;

namespace DoConnect.BL.Provider
{
    public interface IDocConnectProvider: IProviderBase<PrivilegeType>
    {
        DataContext DataContext { get; }
        ICurrentUser CurrentUser { get;  set; }
    }
}
