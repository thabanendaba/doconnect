﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.PanicData;
using DoConnect.BL.Provider.Security;
using DoConnect.BL.Entities.ChatData;
using EXO.Lib.Utility;

namespace DoConnect.BL.Provider.PanicData
{
    public class PanicProvider : DocConnectProvider,IPanicProvider
    {
        #region Constructor
        public PanicProvider(DataContext context,ICurrentUser currentUser)
            :base(context, currentUser)
        {

        }

        public PanicHistory SavePanic(double lat, double lon)
        {
            var result = new PanicHistory()
            {
                UserIdentityId = CurrentUser.Id,
                SendDateTime = DateTime.Now,
                Location = GeoUtils.CreatePoint(lat, lon)
            };

            DataContext.PanicHistory.Add(result);
            DataContextSaveChanges();
            return result;
        }
        #endregion


    }
}
