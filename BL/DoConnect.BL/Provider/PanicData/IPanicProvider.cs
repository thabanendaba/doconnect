﻿using DoConnect.BL.Entities.ChatData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.PanicData;

namespace DoConnect.BL.Provider.PanicData
{
    public interface IPanicProvider:IDocConnectProvider
    {
        PanicHistory SavePanic(double lat, double lon);
    }
}
