﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Provider.Security;

namespace DoConnect.BL.Provider.Email
{
    public class EmailProvider:DocConnectProvider,IEmailProvider
    {
        public EmailProvider(DataContext context, ICurrentUser user)
            :base(context,user)
        {

        }

        public EmailProvider(DataContext context)
            : base(context)
        {

        }

        public void SendPasswordResetEmail(Entities.SecurityData.UserIdentity user, string newPassword)
        {
        }
    }
}
