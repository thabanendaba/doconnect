﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.SecurityData;

namespace DoConnect.BL.Provider.Email
{
    public interface IEmailProvider
    {
        void SendPasswordResetEmail(UserIdentity user, string newPassword);
    }
}
