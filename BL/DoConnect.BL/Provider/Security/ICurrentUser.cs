﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Entities.Types;
using EXO.Lib.BL;

namespace DoConnect.BL.Provider.Security
{
    public interface ICurrentUser : IUserContext<PrivilegeType>
    {
        UserType UserType { get;  }

        string UserTypeString { get; }
    }
}
