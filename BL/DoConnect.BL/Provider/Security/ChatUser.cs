﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoConnect.BL.Provider.Security
{
    public class ChatUser
    {
        public long UserId { get; set; }

        public string UserName { get; set; }

        public string DisplayName { get; set; }
    }
}
