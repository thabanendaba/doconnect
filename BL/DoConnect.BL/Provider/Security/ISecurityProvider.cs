﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.Email;
using DoConnect.BL.Types;
using DoConnect.BL.Settings;

namespace DoConnect.BL.Provider.Security
{
    public interface ISecurityProvider:IDocConnectProvider
    {
        #region User Login

        ICurrentUser UserLogin(string userName, string password);
        ICurrentUser UserIdentityToCurrentUser(UserIdentity user);
        ICurrentUser GetCurrentUser(string userName);

        #endregion

        #region Roles
        Role SaveRole(long? roleId, string name, string description, StatusType status, List<PrivilegeType> privileges);

        Role GetRole(long roleId);

        IQueryable<Role> GetRoles();

        void DeleteRole(long roleId);
        void ArchiveRole(long id);

        #endregion

        #region User Maintenance

        UserIdentity GetUser(long userId);

        bool UserExists(string userName);

        bool ChangePassword(long userId, string password);

        UserIdentity SignUp(string userName, string password, string emailAddress,
                            string title, string firstName, string surname,
                            string idOrPassportNumber, GenderType gender,
                            string telephone, string addressLine1, string addressLine2,string city, string postalCode);

        UserIdentity SaveUser(long? id, string userName, string password, string emailAddress,
                              string title, string firstName, string surname,
                               string idOrPassportNumber, GenderType gender,
                              string telephone, string addressLine1, string addressLine2, string city, string postalCode, 
                              List<long> allowedRoles, bool isAdmin);

        UserIdentity DoctorEdit(long? id, string userName, string emailAddress, string title, string firstName, string surname,
                                    string idOrPassportNumber, GenderType gender, string telephone, string addressLine1, string addressLine2, string city,
                                    string postalCode);


     
        IQueryable<UserIdentity> GetUserList();

        UserIdentity LockAccount(long userId);
        UserIdentity UnlockAccount(long userId);

        UserIdentity DeactivateAccount(long userId);
        UserIdentity ActivateAccount(long userId);

        #endregion

        #region Manage Current Loggedin user
        
        //changed from ICurrentUser to UserIdentity
        UserIdentity ChangeMyPassword(string oldPassword, string newPassword);
   
        UserIdentity GetMyProfile();

        UserIdentity SaveMyProfile(GenderType gender, string email, string telephone, string title, string firstName, string surname, string phone, string firstAddress, string secondAddress, string city, string code);
      
        bool ResetMyPassword(string emailAddress, IEmailProvider emailProvider);


        #endregion

        #region SignalR

        UserIdentity SignalRConnected(string userName, string connectionId, string userAgent, bool connected);

        ICollection<string> SignalRConnectionIds(long userIdentityId);

        IQueryable<ChatUser> GetAllDoctors();


        #endregion
    }
}
