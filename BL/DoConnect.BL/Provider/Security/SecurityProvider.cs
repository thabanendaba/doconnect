﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EXO.Lib.BL;
 using DoConnect.BL.Settings;
 using System.Data.Entity;
 using DoConnect.BL.Context;
 using DoConnect.BL.Entities.SecurityData;
 using DoConnect.BL.Entities.Types;
 using DoConnect.BL.Provider.Email;
 using DoConnect.BL.Types;
 using DoConnect.BL.Util;
 using DoConnect.BL.Entities.PatientData;

namespace DoConnect.BL.Provider.Security
{
    public class SecurityProvider : DocConnectProvider, ISecurityProvider
    {
        #region Ctor

        public SecurityProvider(DataContext context)
            : this(context, null)
        {

        }

        public SecurityProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }
        #endregion

        public ICurrentUser UserLogin(string userName, string password)
        {
            var result = DataContext.UserIdentitySet
                .Include(a => a.Doctors)
                .Include(a => a.Patients)
                .Where(x => x.UserName == userName).SingleOrDefault();

            if (result == null || result.PasswordHash != Cipher.Encrypt(password))
            {
                if (result != null)
                {
                    result.FailedLoginAttempts = result.FailedLoginAttempts + 1;
                    //if (result.FailedLoginAttempts >= 3)
                    //    result.LockedOut = DateTime.Now;
                    DataContextSaveChanges();
                }

                throw new SecurityException("Invalid user name and password combination.");
            }

            if (result.Deactivated != null)
                throw new SecurityException("Account Deactivated!");

            if (result.LockedOut != null)
                throw new SecurityException("Account locked out!");

            if (result.Active == false)
                throw new SecurityException("Account Deactivated!");

            result.LastLogin = DateTime.Now;

            if (result.FailedLoginAttempts > 0)
                result.FailedLoginAttempts = 0;


            DataContextSaveChanges();

            var qPrivs = from userP in DataContext.UserIdentitySet
                         from role in userP.Roles
                         from privileges in role.Privileges
                         where userP.Id == result.Id
                         select privileges.Security;

            result.AllowedPrivileges = qPrivs.Distinct().ToList();

            if (result.IsSystemAdmin)
                result.AllowedPrivileges = DataContext.PrivilegeSet.Select(a => a.Security).ToList();

            CurrentUser = UserIdentityToCurrentUser(result);

            return CurrentUser;
        }

        public ICurrentUser GetCurrentUser(string userName)
        {
            var result = DataContext.UserIdentitySet.Where(x => x.UserName == userName).SingleOrDefault();

            if(result == null)
                throw new SecurityException("User not logged in!");

            if (result.Deactivated != null)
                throw new SecurityException("Account Deactivated!");

            if (result.LockedOut != null)
                throw new SecurityException("Account locked out!");

            if (result.Active == false)
                throw new SecurityException("Account Deactivated!");

            result.LastLogin = DateTime.Now;

            if (result.FailedLoginAttempts > 0)
                result.FailedLoginAttempts = 0;

            DataContextSaveChanges();

            var qPrivs = from userP in DataContext.UserIdentitySet
                         from role in userP.Roles
                         from privileges in role.Privileges
                         where userP.Id == result.Id
                         select privileges.Security;

            result.AllowedPrivileges = qPrivs.Distinct().ToList();

            if (result.IsSystemAdmin)
                result.AllowedPrivileges = DataContext.PrivilegeSet.Select(a => a.Security).ToList();

            CurrentUser = UserIdentityToCurrentUser(result);

            return CurrentUser;
        }


        public ICurrentUser UserIdentityToCurrentUser(UserIdentity user)
        {
            UserType type =  UserType.Admin;

            if (user.Doctors != null && user.Doctors.Count() > 0)
            {
                type = UserType.Doctor;
            }
            else if (user.Patients != null && user.Patients.Count() > 0)
            {
                type = UserType.Patient;
            }
           


            return new LoggedInUser()
            {
                Id = user.Id,
                UserName = user.UserName,
                DisplayName = user.FirstName + " " + user.Surname,
                AllowedPrivileges = user.AllowedPrivileges,
                IsSystemAdmin = user.IsSystemAdmin,
                UserType = type
            };
        }


        #region Role

        public Role SaveRole(long? roleId, string name, string description, StatusType status, List<PrivilegeType> privs)
        {
            Authenticate(PrivilegeType.RoleMaintenance);

            var pTypeArr = privs.ToArray();

            var role = DataContext.RoleSet.Include(a => a.Privileges).Where(r => r.RoleName == name).SingleOrDefault();

            if (role != null && role.Id != roleId)
                throw new SecurityException(name + " role already exist.");
            else
            {
                if (roleId != null)
                {
                    role = DataContext.RoleSet.Include(a => a.Privileges).Where(r => r.Id == roleId).Single();
                }
                else
                {
                    role = new Role();
                    DataContext.RoleSet.Add(role);
                }
            }

            role.Status = status;
            role.RoleName = name;
            role.Description = description;

            var securities = DataContext.PrivilegeSet.Where(a => pTypeArr.Contains(a.Security)).ToList();
            if (role.Privileges != null)
            {
                role.Privileges.Clear();
                foreach (var i in securities)
                    role.Privileges.Add(i);
            }
            else
            {
                role.Privileges = securities;
            }

            DataContextSaveChanges();

            return role;
        }

        public Role GetRole(long roleId)
        {
            Authenticate(PrivilegeType.RoleMaintenance);

            var role = new Role();

            var q = DataContext.RoleSet.Include(p => p.Privileges).Where(r => r.Id == roleId).SingleOrDefault();

            if (q == null)
                return null;

            role.Id = q.Id;
            role.Status = q.Status;
            role.RoleName = q.RoleName;
            role.Privileges = q.Privileges.ToList();
            role.Description = q.Description;

            return role;
        }

        public IQueryable<Role> GetRoles()
        {
            return DataContext.RoleSet.Include(a => a.Privileges);
        }

        public void DeleteRole(long roleId)
        {
            Authenticate(PrivilegeType.RoleMaintenance);

            var role = DataContext.RoleSet.Where(r => r.Id == roleId).Single();

            DataContext.RoleSet.Remove(role);

            DataContextSaveChanges();
        }

        public void ArchiveRole(long id)
        {
            Authenticate(PrivilegeType.RoleMaintenance);

            var role = DataContext.RoleSet.Where(r => r.Id == id).Single();

            if (role.Status == StatusType.Archive)
                throw new SecurityException("Role already archived.");

            role.Status = StatusType.Archive;

            DataContextSaveChanges();
        }

        #endregion

        public UserIdentity GetUser(long userId)
        {

            var userIdentity = DataContext.UserIdentitySet.Where(a => a.Id == userId).Single();

            if (userIdentity.Active == false)
                throw new SecurityException("User account is not active.");
            if (userIdentity.LockedOut != null)
                throw new SecurityException("User account is locked.");
            if (userIdentity.Deactivated != null)
                throw new SecurityException("User account was deactivated.");

            //obtain the user record          
            var u = DataContext.UserIdentitySet.Where(a => a.Id == userId).Single();

            //set the values 
            userIdentity.Title = u.Title;
            userIdentity.FirstName = u.FirstName;
            userIdentity.Surname = u.Surname;
            userIdentity.UserName = u.UserName;
            userIdentity.EmailAddress = u.EmailAddress;
            userIdentity.AddressLine1 = u.AddressLine1;
            userIdentity.AddressLine2 = u.AddressLine2;
            userIdentity.City = u.City;
            userIdentity.PostalCode = u.PostalCode;
            userIdentity.IdPassportNum = u.IdPassportNum;
            userIdentity.Gender = u.Gender;

            return userIdentity;
        }

        public bool UserExists(string userName)
        {

            var userExist = false;

            var thisUser = DataContext.UserIdentitySet.Where(u => u.UserName == userName).SingleOrDefault();

            if (thisUser != null)
                userExist = true;
            else
                userExist = false;

            return userExist;
        }

        public UserIdentity SignUp(string userName, string password, string emailAddress, string title, string firstName, string surname,
                                    string idOrPassportNumber, GenderType gender, string telephone, string addressLine1, string addressLine2,
                                    string city, string postalCode)
        {
            var curr = DataContext.UserIdentitySet
                .Include(a => a.Doctors)
                .Include(a => a.Patients)
                .Where(a => a.UserName == userName).SingleOrDefault();

            if (curr != null)
                throw new SecurityException("User already exists");

            var usr = new UserIdentity()
            {
                UserName = userName,
                Active = true,
                Title = title,
                FirstName = firstName,
                Surname = surname,
                IdPassportNum = idOrPassportNumber,
                Gender = gender,
                PasswordHash = Cipher.Encrypt(password),
                Telephone = telephone,
                EmailAddress = emailAddress,
                AddressLine1 = addressLine1,
                AddressLine2 = addressLine2,
                City = city,
                PostalCode = postalCode,
                FailedLoginAttempts = 0,
                IsSystemAdmin = false
            };

            DataContext.UserIdentitySet.Add(usr);

            DataContextSaveChanges();

            return usr;
        }

        public UserIdentity SaveUser(long? id, string userName, string password, string emailAddress, string title, string firstName, string surname,
                                    string idOrPassportNumber, GenderType gender, string telephone, string addressLine1, string addressLine2, string city,
                                    string postalCode, List<long> allowedRoles, bool isAdmin)
        {
            Authenticate(PrivilegeType.UserMaintenance);

            if (id == 0)
                id = null;

            var newUser = DataContext.UserIdentitySet.Include(a => a.Roles).Where(u => u.UserName == userName).SingleOrDefault();

            if (newUser != null)
            {
                if (newUser.Id == id)
                    newUser = DataContext.UserIdentitySet.Where(u => u.Id == id).Single();
                else
                    throw new SecurityException("User already exists!");
            }
            else
            {
                if (id == null)
                {
                    newUser = new UserIdentity();
                    DataContext.UserIdentitySet.Add(newUser);
                }
                else
                    throw new SecurityException("User not found!");
            }

            newUser.UserName = userName;
            newUser.Title = title;
            newUser.FirstName = firstName;
            newUser.Surname = surname;
            newUser.IdPassportNum = idOrPassportNumber;
            newUser.Telephone = telephone;
            newUser.EmailAddress = emailAddress;
            newUser.AddressLine1 = addressLine1;
            newUser.AddressLine2 = addressLine2;
            newUser.City = city;
            newUser.PostalCode = postalCode;
            newUser.IsSystemAdmin = isAdmin;

            var roles = DataContext.RoleSet.Where(a => allowedRoles.Contains(a.Id)).ToList();

            if (newUser.Roles != null)
            {
                newUser.Roles.Clear();
                foreach (var r in roles)
                    newUser.Roles.Add(r);
            }
            else
                newUser.Roles = roles;

            DataContextSaveChanges();

            return newUser;
        }

        public UserIdentity DoctorEdit(long? id, string userName, string emailAddress, string title, string firstName, string surname,
                                    string idOrPassportNumber, GenderType gender, string telephone, string addressLine1, string addressLine2, string city,
                                    string postalCode)
        {
            Authenticate(PrivilegeType.UserMaintenance);

            

            var newUser = DataContext.UserIdentitySet.Include(a => a.Roles).Where(u => u.UserName == userName).SingleOrDefault();

            
                if (newUser.Id == id)
                    newUser = DataContext.UserIdentitySet.Where(u => u.Id == id).Single();
                else
                    throw new SecurityException("User already exists!");
            
            

            //newUser.UserName = userName;
            newUser.Title = title;
            newUser.FirstName = firstName;
            newUser.Surname = surname;
            newUser.IdPassportNum = idOrPassportNumber;
            newUser.Telephone = telephone;
            newUser.EmailAddress = emailAddress;
            newUser.AddressLine1 = addressLine1;
            newUser.AddressLine2 = addressLine2;
            newUser.City = city;
            newUser.PostalCode = postalCode;

            DataContextSaveChanges();

            return newUser;
        }

        public bool ChangePassword(long userId, string password)
        {

            var result = false;

            var me = DataContext.UserIdentitySet.Where(m => m.Id == userId).Single();

            if (me.Deactivated != null)
                throw new SecurityException("User account is deactivated");
            else if (me.LockedOut != null)
                throw new SecurityException("User account is locked.");
            else
            {
                me.PasswordHash = Cipher.Encrypt(password);
                result = true;
            }

            DataContextSaveChanges();

            return result;
        }

        public IQueryable<UserIdentity> GetUserList()
        {

            var userList = from u in DataContext.UserIdentitySet
                           orderby u.Active, u.UserName, u.IsSystemAdmin ascending
                           select u;
            return userList;
        }

        public UserIdentity LockAccount(long userId)
        {
            Authenticate(PrivilegeType.UserMaintenance);

            var thisUser = DataContext.UserIdentitySet.Where(u => u.Id == userId).Single();

            if (thisUser.LockedOut != null)
                throw new SecurityException("Account already locked.");
            else
                thisUser.LockedOut = DateTime.Today;

            DataContextSaveChanges();

            return thisUser;
        }

        public UserIdentity UnlockAccount(long userId)
        {
            Authenticate(PrivilegeType.UserMaintenance);

            var thisUser = DataContext.UserIdentitySet.Where(u => u.Id == userId).Single();

            thisUser.LockedOut = null;

            DataContextSaveChanges();

            return thisUser;
        }

        public UserIdentity DeactivateAccount(long userId)
        {
            Authenticate(PrivilegeType.UserMaintenance);

            var thisUser = DataContext.UserIdentitySet.Where(u => u.Id == userId).Single();

            if (thisUser.Deactivated != null)
                throw new SecurityException("Account already deactivated.");
            else
                thisUser.Deactivated = DateTime.Today;

            DataContextSaveChanges();

            return thisUser;
        }

        public UserIdentity ActivateAccount(long userId)
        {
            Authenticate(PrivilegeType.UserMaintenance);

            var thisUser = DataContext.UserIdentitySet.Where(u => u.Id == userId).Single();

            thisUser.Deactivated = null;

            DataContextSaveChanges();

            return thisUser;
        }

        public UserIdentity ChangeMyPassword(string oldPassword, string newPassword)
        {

            var me = DataContext.UserIdentitySet.Where(m => m.Id == CurrentUser.Id).Single();

            if (me.PasswordHash != Cipher.Encrypt(oldPassword))
                throw new SecurityException("Old password is incorrect!");

            me.PasswordHash = Cipher.Encrypt(newPassword);

            DataContextSaveChanges();

            return me;
        }

        public UserIdentity GetMyProfile()
        {

            var profile = new UserIdentity();
            var me = DataContext.UserIdentitySet.Where(u => u.Id == CurrentUser.Id).Single();

            profile.FirstName = me.FirstName;
            profile.Title = me.Title;
            profile.Surname = me.Surname;
            profile.Telephone = me.Telephone;
            profile.UserName = me.UserName;
            profile.EmailAddress = me.EmailAddress;
            profile.AddressLine1 = me.AddressLine1;
            profile.AddressLine2 = me.AddressLine2;
            profile.City = me.City;
            profile.PostalCode = me.PostalCode;
            profile.IdPassportNum = me.IdPassportNum;
            profile.Gender = me.Gender;

            return profile;

        }

        public UserIdentity SaveMyProfile(GenderType gender, string email, string telephone, string title, string firstName, string surname, string phone, string firstAddress
                                        , string secondAddress, string city, string code)
        {

            var myProfile = DataContext.UserIdentitySet.Where(u => u.Id == CurrentUser.Id).Single();

            myProfile.Gender = gender;
            myProfile.EmailAddress = email;
            myProfile.Telephone = telephone;
            myProfile.Title = title;
            myProfile.FirstName = firstName;
            myProfile.Surname = surname;
            myProfile.AddressLine1 = firstAddress;
            myProfile.AddressLine2 = secondAddress;
            myProfile.City = city;
            myProfile.PostalCode = code;

            DataContextSaveChanges();

            return myProfile;
        }

        public bool ResetMyPassword(string emailAddress, IEmailProvider emailProvider)
        {
            var user = DataContext.UserIdentitySet.Where(h => h.EmailAddress == emailAddress).SingleOrDefault();

            var isReset = false;
            var resetPassword = "#Brain_Dead";
            var encryptedPassword = Cipher.Encrypt(resetPassword);

            if (user == null)
                throw new SecurityException("This email account " + emailAddress + " does not exist in our system.");


            if (user.Deactivated != null || user.LockedOut != null)
                throw new SecurityException("You account is not active, please contact the administrator.");

            user.PasswordHash = encryptedPassword;

            emailProvider.SendPasswordResetEmail(user, resetPassword);

            DataContextSaveChanges();

            isReset = true;

            return isReset;
        }


        public UserIdentity SignalRConnected(string userName, string connectionId,
            string userAgent, bool connected)
        {
            var user = DataContext.UserIdentitySet
                                  .Include(a => a.SignalRConnections)
                                  .Include(a => a.Doctors)
                                  .Include(a => a.Patients)
                                  .Where(a => a.UserName == userName).SingleOrDefault();
            CurrentUser = UserIdentityToCurrentUser(user);

            if (connected == false)
            {
                var dt = DateTime.Now.AddDays(-2);

                foreach (var connection in user.SignalRConnections.Where(a => a.ConnectionId == connectionId || a.LastUpdated < dt).ToList())
                {
                    DataContext.SignalRConnectionSet.Remove(connection);
                    user.SignalRConnections.Remove(connection);
                }
            }
            else
            {
                var current = user.SignalRConnections.Where(a => a.ConnectionId == connectionId).SingleOrDefault();
                if (current == null)
                {
                    current = new SignalRConnection()
                    {
                        ConnectionId = connectionId,
                        LastUpdated = DateTime.Now,
                        UserAgent = userAgent,
                        UserIdentityId = user.Id
                    };
                    user.SignalRConnections.Add(current);
                }
                current.LastUpdated = DateTime.Now;
            }
            DataContextSaveChanges();
            return user;
        }


        public ICollection<string> SignalRConnectionIds(long userIdentityId)
        {
            var dt = DateTime.Now.AddDays(-2);
            var q = from s in DataContext.SignalRConnectionSet
                    where s.UserIdentityId == userIdentityId
                       && s.LastUpdated > dt
                    select s.ConnectionId;

            return q.ToList();
        }

        public IQueryable<ChatUser> GetAllDoctors()
        {
            var dt = DateTime.Now.AddDays(-1);

            var q = from d in DataContext.DoctorSet
                    from s in d.UserIdentity.SignalRConnections
                    where s.LastUpdated > dt
                    select new ChatUser()
                    {
                        UserId = d.UserIdentity.Id,
                        UserName = d.UserIdentity.UserName,
                        DisplayName = d.UserIdentity.FirstName + " " + d.UserIdentity.Surname
                    };



            return q.Distinct().OrderBy(a => a.DisplayName);
        }

    }
}
