﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Entities.Types;
using EXO.Lib.BL;

namespace DoConnect.BL.Provider.Security
{
    public class LoggedInUser : ICurrentUser
    {
        public long Id { get; set; }

        public string UserName { get; set; }

        public string DisplayName { get; set; }

        public List<PrivilegeType> AllowedPrivileges { get; set; }

        public bool IsSystemAdmin { get; set; }

        public UserType UserType { get; set; }

        public string UserTypeString
        {
            get
            {
                return UserType.ToString();
            }
        }
    }
}
