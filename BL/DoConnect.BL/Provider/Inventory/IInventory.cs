﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoConnect.BL.Provider.Inventory
{
    public interface IInventory
    {
        /// <summary>
        /// Gets the inventory.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns></returns>
       Entities.Inventory.Inventory GetInventory(long Id);

        List<Entities.Inventory.Inventory> GetInventories();
    }
}
