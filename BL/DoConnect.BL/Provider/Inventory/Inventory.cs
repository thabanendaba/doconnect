﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Provider.Security;

namespace DoConnect.BL.Provider.Inventory
{
    public class Inventory : DocConnectProvider, IInventory
    {
        #region Constructor

        public Inventory(DataContext context, ICurrentUser currentUser)
            :base(context,currentUser)
        {

        }
        #endregion
        public Entities.Inventory.Inventory GetInventory(long Id)
        {
            return DataContext.Inventory.SingleOrDefault(i => i.Id == Id);
        }

        public List<Entities.Inventory.Inventory> GetInventories()
        {
            return DataContext.Inventory.ToList();
        }
    }
}
