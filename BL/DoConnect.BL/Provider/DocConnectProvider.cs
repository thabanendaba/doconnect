﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.Security;
using EXO.Lib.BL;

namespace DoConnect.BL.Provider
{
    public class DocConnectProvider : ProviderBase<PrivilegeType>, IDocConnectProvider
    {
        public DocConnectProvider(DataContext context)
            : this(context, null)
        {

        }

        public DocConnectProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }

        public DataContext DataContext
        {
            get { return this.AuditDBContext as DataContext; }
        }

        public ICurrentUser CurrentUser
        {
            get
            {
                return LoggedInUser as ICurrentUser;
            }
            set
            {
                LoggedInUser = value as IUserContext<PrivilegeType>;
            }
        }
    }
}
