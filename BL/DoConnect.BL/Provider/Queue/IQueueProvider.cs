﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoConnect.BL.Provider.Queue
{
    public interface IQueueProvider : IDocConnectProvider
    {
        List<BL.Entities.QueueData.Queue> GetQueueList();

        Entities.QueueData.Queue GetQueueById(long id);

        Entities.QueueData.Queue GetQueueByStatus(Entities.Types.QueueStatus status);

        Entities.QueueData.Queue SaveQueue(BL.Entities.QueueData.Queue queue);
    }
}
