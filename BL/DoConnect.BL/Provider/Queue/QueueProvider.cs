﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Provider.Security;

namespace DoConnect.BL.Provider.Queue
{
    public class QueueProvider : DocConnectProvider, IQueueProvider
    {
        #region Constructor
        public QueueProvider(DataContext context, ICurrentUser currentUser)
            :base(context, currentUser)
        {

        }


        #endregion

        #region Methods
        public List<Entities.QueueData.Queue> GetQueueList()
        {
            try
            {
                return DataContext.Queue?.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Entities.QueueData.Queue GetQueueById(long id)
        {
            try
            {
                return DataContext.Queue?.SingleOrDefault(q => q.Id == id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Entities.QueueData.Queue GetQueueByStatus(Entities.Types.QueueStatus status)
        {
            try
            {
                return DataContext.Queue?.SingleOrDefault(q => q.Status == status);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Entities.QueueData.Queue SaveQueue(Entities.QueueData.Queue queue)
        {
            try
            {
                if (queue.Id > 0)
                {
                    Entities.QueueData.Queue updateQueue = DataContext.Queue.SingleOrDefault(q => q.Id == queue.Id);

                    if (updateQueue != null)
                    {
                        DataContext.Queue.Attach(updateQueue);
                        updateQueue.DateTime = queue.DateTime;
                        updateQueue.PatientIdentityId = queue.PatientIdentityId;
                        updateQueue.Patient = DataContext.PatientSet.SingleOrDefault(p => p.Id == queue.Id);
                        updateQueue.DoctorIdentityId = queue.DoctorIdentityId;
                        updateQueue.Doctor = DataContext.DoctorSet.SingleOrDefault(d => d.Id == queue.Id);
                        updateQueue.Status = queue.Status;
                        updateQueue.Number = queue.Number;

                        DataContext.SaveChanges();
                        return updateQueue;
                    }
                }
                else
                {
                    Entities.QueueData.Queue newQueue = new Entities.QueueData.Queue()
                    {
                        DateTime = queue.DateTime,
                        DoctorIdentityId = queue.DoctorIdentityId,
                        Doctor = DataContext.DoctorSet.SingleOrDefault(d => d.Id == queue.Id),
                        PatientIdentityId = queue.PatientIdentityId,
                        Patient = DataContext.PatientSet.SingleOrDefault(p => p.Id == queue.Id),
                        Status = queue.Status,
                        Number = queue.Number
                    };
                    newQueue.Patient.Queues.Add(newQueue);
                    newQueue.Doctor.Queues.Add(newQueue);
                    DataContext.Queue.Add(newQueue);
                    DataContext.SaveChanges();
                    return newQueue;
                }
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        } 
        #endregion
    }
}
