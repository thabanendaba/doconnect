﻿using DoConnect.BL.Entities.PatientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.MasterData;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.PatientData;
using DoConnect.BL.Provider.Security;
using DoConnect.BL.Types;

namespace DoConnect.BL.Provider.MasterData
{
    public class MasterDataProvider : DocConnectProvider,IMasterDataProvider
    {
        public MasterDataProvider(DataContext context, ICurrentUser currentUser)
            :base(context,currentUser)
        {

        }

        public Biometric SaveBiometric(long?id, string name, decimal? maxValue, decimal? minValue, bool triggerAlert ,BiometricType biometricType)
        {
            Authenticate(PrivilegeType.BiometricMaintenance);

            Biometric saveBiometric = null;

            saveBiometric = DataContext.BiometricSet.Where(a => a.Name == name && a.Id!=id).SingleOrDefault();
            if (saveBiometric != null)
            {
                throw new MasterDataException("Biometric with the name : " + name + " already exists.");
            }

            if (id!=null && id>0)
            {
                saveBiometric = DataContext.BiometricSet.Where(a => a.Id == id).Single();
            }
            else
            {
                saveBiometric = new Biometric();
                saveBiometric.Name = name;
                saveBiometric.Status = StatusType.Active;
                DataContext.BiometricSet.Add(saveBiometric);
            }

            saveBiometric.Name = name;
            saveBiometric.MaxValue = maxValue;
            saveBiometric.BiometricType = biometricType;
            saveBiometric.MinValue = minValue;
            saveBiometric.TriggerAlert = triggerAlert;

            DataContextSaveChanges();

            return saveBiometric;
        }

        public Biometric ArchieveBiometric(long id)
        {
            Authenticate(PrivilegeType.BiometricMaintenance);

            var biometric = DataContext.BiometricSet.Where(a => a.Id == id).Single();

            DataContextSaveChanges();

            return biometric;
        }

        public IQueryable<Biometric>GetBiometric()
        {
            var objQuery = from b in DataContext.BiometricSet
                           orderby b.Name
                           select b;

            return objQuery;
        }


    }
}
