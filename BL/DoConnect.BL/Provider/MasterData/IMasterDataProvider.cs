﻿using DoConnect.BL.Entities.PatientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.MasterData;

namespace DoConnect.BL.Provider.MasterData
{
    public interface IMasterDataProvider : IDocConnectProvider
    {
        Biometric SaveBiometric(long? id, string name, decimal? maxValue, decimal? minValue, bool triggerAlert , BiometricType biometricType);

        Biometric ArchieveBiometric(long id);

        IQueryable<Biometric> GetBiometric();

    }
}
