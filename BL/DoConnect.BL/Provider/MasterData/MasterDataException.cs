﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoConnect.BL.Provider.PatientData
{
    public class MasterDataException : Exception
    {
        public MasterDataException(string message)
            :base(message)
        {

        }
    }
}
