﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoConnect.BL.Provider.PatientData
{
    public class PatientAlert
    {
        public string EmailAddress { get; internal set; }
        public string FirstName { get; internal set; }
        public DateTime LastUpdate { get; internal set; }
        public string Surname { get; internal set; }
        public string Telephone { get; internal set; }
        public long UserIdentityId { get; internal set; }
    }
}
