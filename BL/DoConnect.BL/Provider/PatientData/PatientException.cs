﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoConnect.BL.Provider.PatientData
{
    public class PatientException : Exception
    {
        public PatientException(string message)
            :base(message)
        {

        }
    }
}
