﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoConnect.BL.Provider.PatientData
{
    public class PatientBiometricException : Exception
    {
        public PatientBiometricException(string message)
            :base(message)
        {

        }
    }
}
