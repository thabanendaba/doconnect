﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoConnect.BL.Provider.PatientData
{
    public class PatientNotification
    {

        public string ConnectionId { get; set; }

        public long ReceiverUserIdentityId { get; set; }
    }
}
