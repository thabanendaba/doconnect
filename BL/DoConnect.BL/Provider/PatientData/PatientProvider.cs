﻿using DoConnect.BL.Entities.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.PatientData;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.Security;

namespace DoConnect.BL.Provider.PatientData
{
    public class PatientProvider : DocConnectProvider, IPatientProvider
    {
        #region Constructor
        public PatientProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }
        #endregion

        #region Patient

        public Patient SavePatient(long? id, long userIdentityId)
        {
            var savePatient = DataContext.PatientSet.Where(a => a.UserIdentityId == userIdentityId).SingleOrDefault();

            if (savePatient != null)
            {
                throw new PatientException("Patient with the user id: " + userIdentityId + " already exists.");
            }
            else
            {
                savePatient = new Patient();
                DataContext.PatientSet.Add(savePatient);
            }

            savePatient.UserIdentityId = userIdentityId;

            DataContextSaveChanges();

            return savePatient;

        }

        public Patient ArchivePatient(long id)
        {
            Authenticate(PrivilegeType.PatientMaintenance);

            var patient = DataContext.PatientSet.Where(a => a.Id == id).Single();

            DataContextSaveChanges();

            return patient;
        }

        public IQueryable<Patient> GetPatient()
        {
            var objQuery = from p in DataContext.PatientSet
                           orderby p.UserIdentityId
                           select p;

            return objQuery;
            //throw new NotImplementedException();
        }

        #endregion

        #region PatientBiometric
        public PatientBiometric SavePatientBiometric(long userIdentityId, long biometricId, decimal? numericValue, string stringValue, bool? yesNoValue)
        {
            var patient = DataContext.PatientSet
                                     .Where(a => a.UserIdentityId == userIdentityId)
                                    .Include(a => a.UserIdentity).Single();

            if (CurrentUser.Id != patient.UserIdentityId)
                Authenticate(PrivilegeType.BiometricMaintenance);

            var savePatientBiometric = DataContext.PatientBiometricSet.Where(a => a.PatientId == patient.Id && a.BiometricId == biometricId).SingleOrDefault();

            if (savePatientBiometric == null)
            {
                savePatientBiometric = new PatientBiometric();
                savePatientBiometric.PatientId = patient.Id;
                savePatientBiometric.BiometricId = biometricId;
                DataContext.PatientBiometricSet.Add(savePatientBiometric);
            }

            savePatientBiometric.NumericValue = numericValue;
            savePatientBiometric.StringValue = stringValue;
            savePatientBiometric.YesNoValue = yesNoValue;
            savePatientBiometric.LastUpdateDate = DateTime.Now;
            savePatientBiometric.NotificationSentDate = null;

            DataContextSaveChanges();

            return savePatientBiometric;
        }

        public PatientBiometric ArchieveBiometric(long id)
        {
            Authenticate(PrivilegeType.PatientMaintenance);

            var patientBiometric = DataContext.PatientBiometricSet.Where(a => a.Id == id).Single();

            DataContextSaveChanges();

            return patientBiometric;
        }

        public IQueryable<PatientBiometric> GetPatientBiometric()
        {
            var objQuery = from p in DataContext.PatientBiometricSet
                           orderby p.PatientId
                           select p;

            return objQuery;
        }
        #endregion


        public List<PatientNotification> GetAllPatientsWithProblems()
        {
            var dt = DateTime.Now.AddDays(-2);

            var q = from p in DataContext.PatientSet
                    from s in p.UserIdentity.SignalRConnections
                    from b in p.PatientBiometrics
                    where s.LastUpdated > dt
                    && b.Biometric.TriggerAlert == true
                    //&& b.NotificationSentDate == null
                    && b.LastUpdateDate > dt
                    && b.Biometric.BiometricType == Entities.MasterData.BiometricType.NumericValue
                    && (b.NumericValue >= b.Biometric.MaxValue || b.NumericValue <= b.Biometric.MinValue)
                    orderby b.Id
                    select new
                    {
                        UserIdentityId = p.UserIdentityId,
                        PatientId = p.Id
                    };

            var result = q.Distinct().Take(50).ToList();

            var todaysDate = DateTime.Now;

            foreach (var notification in result)
            {
                DataContext.ResetPatientNotification(notification.PatientId);
            }

            var usersIds = result.Select(a => a.UserIdentityId).ToArray();

            var qUsers = from s in DataContext.SignalRConnectionSet
                         where usersIds.Contains(s.UserIdentityId)
                         select new PatientNotification
                         {
                             ReceiverUserIdentityId = s.UserIdentityId,
                             ConnectionId = s.ConnectionId
                         };

            return qUsers.ToList();
        }

        public IQueryable<PatientAlert> GetAllPatientsWithAlerts()
        {

            var dt = DateTime.Now.AddDays(-2);

            var q = from p in DataContext.PatientSet
                    from s in p.UserIdentity.SignalRConnections
                    from b in p.PatientBiometrics
                    where s.LastUpdated > dt
                    && b.Biometric.TriggerAlert == true
                    && b.LastUpdateDate > dt
                    && b.NotificationSentDate == null
                    && b.Biometric.BiometricType == Entities.MasterData.BiometricType.NumericValue
                    && (b.NumericValue >= b.Biometric.MaxValue || b.NumericValue <= b.Biometric.MinValue)
                    group b by new
                    {
                        p.UserIdentity.Id,
                        p.UserIdentity.FirstName,
                        p.UserIdentity.Surname,
                        p.UserIdentity.Telephone,
                        p.UserIdentity.EmailAddress,
                    } into g
                    select new PatientAlert
                    {
                        UserIdentityId = g.Key.Id,
                        FirstName = g.Key.FirstName,
                        Surname = g.Key.Surname,
                        Telephone = g.Key.Telephone,
                        EmailAddress = g.Key.EmailAddress,
                        LastUpdate = g.Max(a => a.LastUpdateDate)
                    };

            return q.OrderBy(a => a.LastUpdate);



        }

        public void ResetMyBiometrics(long patientUserIdentityId)
        {
            var patientId = DataContext.PatientSet.Where(a => a.UserIdentityId == patientUserIdentityId).Select(a => a.Id).Single();

            DataContext.ResetPatientNotification(patientId);
        }

        public IQueryable<PatientBiometric> GetMyBiometric()
        {
            long id = DataContext.PatientSet.Where(p => p.UserIdentityId == CurrentUser.Id).Select(u => u.Id).Single();

            var objQuery = from b in DataContext.PatientBiometricSet
                           where b.PatientId == id
                           orderby b.Biometric.Name
                           select b;

            return objQuery;
        }

        public IQueryable<PatientBiometric> GetBiometricsOutOfRangeForPatient(long id)
        {
            var returnValues = DataContext.PatientBiometricSet.Include(a => a.Biometric)
                .Where(a => a.Patient.UserIdentityId == id )
                .OrderBy(a => a.Biometric.Name);


            return returnValues;
        }
    }
}
