﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.PatientData;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.PatientData;
using DoConnect.BL.Entities.Types;

namespace DoConnect.BL.Provider.PatientData
{
    public interface IPatientProvider:IDocConnectProvider
    {
        #region Patient
        Patient SavePatient(long? id, long userIdentityId);
        Patient ArchivePatient(long id);

        IQueryable<Patient> GetPatient();
        #endregion

        #region PatientBiometrics
        PatientBiometric SavePatientBiometric(long userIdentityId, long biometricId, decimal? numericValue, string stringValue, bool? yesNoValue);

        PatientBiometric ArchieveBiometric(long id);

        IQueryable<PatientBiometric> GetPatientBiometric();
        #endregion

        List<PatientNotification> GetAllPatientsWithProblems();

        IQueryable<PatientAlert> GetAllPatientsWithAlerts();

        void ResetMyBiometrics(long patientUserIdentityId);
        IQueryable<PatientBiometric> GetMyBiometric();

        IQueryable<PatientBiometric> GetBiometricsOutOfRangeForPatient(long id);

    }
}
