﻿using DoConnect.BL.Provider.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.ChatData;

namespace DoConnect.BL.Provider.ChatData
{
    public class ChatProvider : DocConnectProvider,IChatProvider
    {
        #region Constructor
        public ChatProvider(DataContext context)
            :base(context,null)
        {

        }


        #endregion
        public ChatHistory SaveChat(long senderUserId, long? receiverUserId, string chatMessage)
        {
            var result = new ChatHistory()
            {
                SenderUserIdentityId = senderUserId,
                ReceiverUserIdentityId = receiverUserId,
                MessageText = chatMessage,
                SendDateTime = DateTime.Now
            };
            DataContext.ChatHistorySet.Add(result);
            DataContextSaveChanges();
            return result;
        }

    }
}
