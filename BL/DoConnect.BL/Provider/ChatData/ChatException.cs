﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoConnect.BL.Provider.ChatData
{
    class ChatException : Exception
    {
        public ChatException(string message)
            :base(message)
        {

        }
    }
}
