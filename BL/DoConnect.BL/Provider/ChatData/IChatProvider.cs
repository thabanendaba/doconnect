﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.ChatData;

namespace DoConnect.BL.Provider.ChatData
{
    public interface IChatProvider:IDocConnectProvider
    {
        ChatHistory SaveChat(long senderUserId, long? receiverUserId, string chatMessage);
    }
}
