﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.DoctorData;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.Security;

namespace DoConnect.BL.Provider.DoctorData
{
    public class DoctorProvider:DocConnectProvider,IDoctorProvider
    {
        #region Constructor

        public DoctorProvider(DataContext context,ICurrentUser currentUser)
            :base(context,currentUser)
        {

        }
        #endregion


        public Doctor SaveDoctor(long? id, long userIdentityId)
        {
            Authenticate(PrivilegeType.DoctorMaintenance);

            var saveDoctor = DataContext.DoctorSet.Where(a => a.UserIdentityId == userIdentityId).SingleOrDefault();

            if (saveDoctor!=null)
            {
                throw new DoctoreException("Doctor with id : " + userIdentityId + " already exists");
            }
            else
            {
                saveDoctor = new Doctor();
                DataContext.DoctorSet.Add(saveDoctor);
            }

            saveDoctor.UserIdentityId = userIdentityId;

            DataContextSaveChanges();

            return saveDoctor;
            //throw new NotImplementedException();
        }

        public Doctor ArchiveDoctor(long id)
        {
            Authenticate(PrivilegeType.DoctorMaintenance);

            var doctor = DataContext.DoctorSet.Where(a => a.Id == id).Single();

            DataContextSaveChanges();

            return doctor;
            //throw new NotImplementedException();
        }

        public IQueryable<Doctor> GetDoctor()
        {
            var objQuery = from d in DataContext.DoctorSet
                           orderby d.Id
                           select d;

            return objQuery;
            //throw new NotImplementedException();
        }

        public IQueryable<UserIdentity> DoctorIdentityList()
        {
            Authenticate(PrivilegeType.UserMaintenance);

            var doctotList = from u in DataContext.UserIdentitySet
                           orderby u.Active, u.UserName, u.IsSystemAdmin ascending
                           where u.Doctors.Any()
                           select u;
            return doctotList;
        }
    }
}
