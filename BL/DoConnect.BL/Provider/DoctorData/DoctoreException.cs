﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoConnect.BL.Provider.DoctorData
{
    class DoctoreException : Exception
    {
        public DoctoreException(string message)
            :base(message)
        {

        }
    }
}
