﻿using DoConnect.BL.Entities.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.DoctorData;
using DoConnect.BL.Entities.SecurityData;

namespace DoConnect.BL.Provider.DoctorData
{
    public interface IDoctorProvider
    {
        #region Doctor
        Doctor SaveDoctor(long? id, long userIdentityId);

        Doctor ArchiveDoctor(long id);

        IQueryable<Doctor> GetDoctor();


        IQueryable<UserIdentity> DoctorIdentityList();

        #endregion
    }
}
