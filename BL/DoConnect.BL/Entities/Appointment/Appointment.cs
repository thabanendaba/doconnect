﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.DoctorData;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Entities.Types;
using DoConnect.BL.Types;
using DoConnect.BL.Entities.PatientData;

namespace DoConnect.BL.Entities.Appointment
{
    [Table("Appointment")]
    public class Appointment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Required]
        public DateTime  From { get; set; }

        [Required]
        public DateTime To { get; set; }

        public TimeSpan Duration { get; set; }

        [Required]
        public DateTime DateStamp { get; set; }

        [Index("IDX_PatientUserIdentityId", IsUnique = true)]
        public virtual long PatientUserIdentityId { get; set; }

        [ForeignKey("PatientUserIdentityId")]
        public virtual Patient Patient { get; set; }
        [Index("IDX_DoctorUserIdentityId", IsUnique = true)]
        public virtual long DoctorUserIdentityId { get; set; }

        [ForeignKey("DoctorUserIdentityId")]
        public virtual Doctor Doctor { get; set; }

        public AppointmentType AppointmentType { get; set; }

        public AppointmentStatus Status { get; set; }
    }
}
