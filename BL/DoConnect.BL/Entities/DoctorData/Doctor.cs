﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Entities.Types;

namespace DoConnect.BL.Entities.DoctorData
{
    [Table("Doctor")]
    public class Doctor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Index("IDX_DoctorUserIdentityId", IsUnique = true)]
        public virtual long UserIdentityId { get; set; }
        [ForeignKey("UserIdentityId")]
        public virtual UserIdentity UserIdentity { get; set; }

        public IdType IdType { get; set; }

        public virtual ICollection<Appointment.Appointment> Appointments { get; set; }

        public virtual ICollection<QueueData.Queue> Queues { get; set; }

    }
}
