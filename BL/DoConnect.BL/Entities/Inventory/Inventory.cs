﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Entities.Types;

namespace DoConnect.BL.Entities.Inventory
{
    [Table("Inventory")]
    public class Inventory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        public virtual long UserIdentityId { get; set; }
        [ForeignKey("UserIdentityId")]
        public virtual UserIdentity UserIdentity { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        public double Quantity { get; set; }

        [Required]
        public DateTime DateStamp { get; set; }


        public InvenoryType InvenoryType { get; set; }

    }
}
