﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.MasterData;
using DoConnect.BL.Provider.PatientData;

namespace DoConnect.BL.Entities.PatientData
{
    [Table("PatientBiometric")]
    public class PatientBiometric
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        public virtual long PatientId { get; set; }
        [ForeignKey("PatientId")]
        public virtual Patient Patient { get; set; }

        public virtual long BiometricId { get; set; }
        [ForeignKey("BiometricId")]
        public virtual Biometric  Biometric { get; set; }

        public virtual decimal? NumericValue { get; set; }

        public virtual string StringValue { get; set; }

        public virtual bool? YesNoValue { get; set; }

        public virtual DateTime LastUpdateDate { get; set; }

        public virtual DateTime? NotificationSentDate { get; set; }
    }
}
