﻿using DoConnect.BL.Entities.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using DoConnect.BL.Entities.SecurityData;

namespace DoConnect.BL.Entities.PatientData
{
    [Table("Patient")]
    public class Patient
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Index("IDX_PatientUserIdentityId", IsUnique = true)]
        public virtual long UserIdentityId { get; set; }
        [ForeignKey("UserIdentityId")]
        public virtual UserIdentity UserIdentity { get; set; }

        public virtual ICollection<PatientBiometric> PatientBiometrics { get; set; }

        public virtual ICollection<Appointment.Appointment> Appointments { get; set; }

        public virtual ICollection<QueueData.Queue> Queues { get; set; }

    }
}
