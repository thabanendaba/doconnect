﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.SecurityData;

namespace DoConnect.BL.Entities.ChatData
{
    [Table("ChatHistory")]
    public class ChatHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual Guid Id { get; set; }

        public virtual long SenderUserIdentityId { get; set; }
        [ForeignKey("SenderUserIdentityId")]
        public virtual UserIdentity SenderUserIdentity { get; set; }

        public virtual long? ReceiverUserIdentityId { get; set; }
        [ForeignKey("ReceiverUserIdentityId")]
        public virtual UserIdentity ReceiverUserIdentity { get; set; }

        public DateTime SendDateTime { get; set; }

        public string MessageText { get; set; }

    }
}
