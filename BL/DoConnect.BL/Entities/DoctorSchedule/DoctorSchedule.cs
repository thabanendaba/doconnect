﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Entities.Types;

namespace DoConnect.BL.Entities.DoctorSchedule
{
    [Table("DoctorSchedule")]
    public class DoctorSchedule
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Index("IDX_DoctorUserIdentityId", IsUnique = true)]
        public virtual long UserIdentityId { get; set; }

        [Required]
        public DayOfWeek Day { get; set; }

        public TimeSpan TimeFrom { get; set; }

        public TimeSpan TimeTo { get; set; }

        public TimeSpan LunchFrom { get; set; }

        public TimeSpan LunchTo { get; set; }

        [ForeignKey("UserIdentityId")]

        public virtual UserIdentity UserIdentity { get; set; }

        public IdType IdType { get; set; }

    }
}
