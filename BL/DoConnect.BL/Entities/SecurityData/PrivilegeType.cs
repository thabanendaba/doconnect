﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoConnect.BL.Entities.SecurityData
{
    [Serializable]
    public enum PrivilegeType
    {
        BiometricMaintenance,
        RoleMaintenance,
        UserMaintenance,
        PatientMaintenance,
        DoctorMaintenance
    }
}

