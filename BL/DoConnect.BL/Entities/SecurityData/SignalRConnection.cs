﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoConnect.BL.Entities.SecurityData
{
    [Table("SignalRConnection")]
    public class SignalRConnection
    {
        [Required]
        [Key]
        [MaxLength(200)]
        public string ConnectionId { get; set; }

        public long UserIdentityId { get; set; }
        [ForeignKey("UserIdentityId")]
        public UserIdentity UserIdentity { get; set; }

        [MaxLength(200)]
        public string UserAgent { get; set; }

        public DateTime LastUpdated { get; set; }
    }
}
