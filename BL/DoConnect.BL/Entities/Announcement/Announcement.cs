﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Entities.Types;

namespace DoConnect.BL.Entities.Announcement
{
    [Table("Announcement")]
    public class Announcement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Index("IDX_UserIdentityId", IsUnique = true)]
        public virtual long UserIdentityId { get; set; }

        [Required]
        [MaxLength(200)]
        public string Title { get; set; }

        [Required]
        [MaxLength(5000)]
        public string Text { get; set; }

        [Required]
        public DateTime DateStamp { get; set; }


        [ForeignKey("UserIdentityId")]
        public virtual UserIdentity UserIdentity { get; set; }
    }
}
