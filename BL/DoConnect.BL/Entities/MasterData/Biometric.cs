﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.PatientData;
using DoConnect.BL.Types;

namespace DoConnect.BL.Entities.MasterData
{
    [Table("Biometric")]
    public class Biometric
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        public virtual StatusType Status { get; set; }

        [MaxLength(200)]
        [Index("IDX_BiometricName",IsUnique=true)]
        [Required]
        public string Name { get; set; }

        public virtual decimal? MaxValue { get; set; }

        public virtual decimal? MinValue { get; set; }

        public virtual bool TriggerAlert { get; set; }

        public virtual BiometricType BiometricType { get; set; }

        public virtual ICollection<PatientBiometric> PatientBiometrics {get;set;}
    }
}
