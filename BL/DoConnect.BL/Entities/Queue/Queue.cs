﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Entities.Types;

namespace DoConnect.BL.Entities.QueueData
{
    [Table("Queue")]
    public class Queue
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }
        [Required]
        public virtual long DoctorIdentityId { get; set; }
        [ForeignKey("PatientIdentityId")]
        public virtual DoctorData.Doctor Doctor { get; set; }

        [Required]
        public DateTime DateTime { get; set; }

        [Required]
        public QueueStatus Status { get; set; }

        public int Number { get; set; }


        public virtual  long PatientIdentityId { get; set; }
        [ForeignKey("PatientIdentityId")]
        public virtual PatientData.Patient Patient { get; set; }

    }
}
