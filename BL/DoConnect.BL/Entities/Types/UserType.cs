﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoConnect.BL.Entities.Types
{
    [Serializable]
    public enum UserType
    {
         Admin,
         Patient,
         Doctor
    }
}
