﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoConnect.BL.Types
{
    public enum AppointmentType
    {
        Checkup,
        Visit,
        Surgery
    }
}
