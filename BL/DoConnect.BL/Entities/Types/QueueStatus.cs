﻿namespace DoConnect.BL.Entities.Types
{
    public enum QueueStatus
    {
        waiting,
        consulted,
        away
    }
}