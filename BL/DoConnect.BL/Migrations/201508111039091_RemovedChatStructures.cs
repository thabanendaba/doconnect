namespace DoConnect.BL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedChatStructures : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ChatMessage", "ChatId", "dbo.Chat");
            DropForeignKey("dbo.ChatMessage", "DoctorId", "dbo.Doctor");
            DropForeignKey("dbo.Device", "UserIdentityId", "dbo.UserIdentity");
            DropForeignKey("dbo.ChatMessage", "PatientId", "dbo.Patient");
            DropIndex("dbo.ChatMessage", new[] { "DoctorId" });
            DropIndex("dbo.ChatMessage", new[] { "PatientId" });
            DropIndex("dbo.ChatMessage", new[] { "ChatId" });
            DropIndex("dbo.Device", new[] { "UserIdentityId" });
            DropTable("dbo.ChatMessage");
            DropTable("dbo.Chat");
            DropTable("dbo.Device");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Device",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserIdentityId = c.Long(nullable: false),
                        DeviceId = c.String(),
                        DeviceType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Chat",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Subject = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ChatMessage",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DoctorId = c.Long(nullable: false),
                        PatientId = c.Long(nullable: false),
                        ChatId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Device", "UserIdentityId");
            CreateIndex("dbo.ChatMessage", "ChatId");
            CreateIndex("dbo.ChatMessage", "PatientId");
            CreateIndex("dbo.ChatMessage", "DoctorId");
            AddForeignKey("dbo.ChatMessage", "PatientId", "dbo.Patient", "Id");
            AddForeignKey("dbo.Device", "UserIdentityId", "dbo.UserIdentity", "Id");
            AddForeignKey("dbo.ChatMessage", "DoctorId", "dbo.Doctor", "Id");
            AddForeignKey("dbo.ChatMessage", "ChatId", "dbo.Chat", "Id");
        }
    }
}
