namespace DoConnect.BL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChatStructure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChatHistory",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        SenderUserIdentityId = c.Long(nullable: false),
                        ReceiverUserIdentityId = c.Long(),
                        SendDateTime = c.DateTime(nullable: false),
                        MessageText = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserIdentity", t => t.ReceiverUserIdentityId)
                .ForeignKey("dbo.UserIdentity", t => t.SenderUserIdentityId)
                .Index(t => t.SenderUserIdentityId)
                .Index(t => t.ReceiverUserIdentityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChatHistory", "SenderUserIdentityId", "dbo.UserIdentity");
            DropForeignKey("dbo.ChatHistory", "ReceiverUserIdentityId", "dbo.UserIdentity");
            DropIndex("dbo.ChatHistory", new[] { "ReceiverUserIdentityId" });
            DropIndex("dbo.ChatHistory", new[] { "SenderUserIdentityId" });
            DropTable("dbo.ChatHistory");
        }
    }
}
