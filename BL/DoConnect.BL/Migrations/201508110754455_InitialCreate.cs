namespace DoConnect.BL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AuditLog",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Long(),
                        UserName = c.String(maxLength: 200),
                        EventDate = c.DateTime(nullable: false),
                        EventType = c.Int(nullable: false),
                        TableName = c.String(nullable: false, maxLength: 200),
                        RecordId = c.Long(nullable: false),
                        ColumnName = c.String(nullable: false, maxLength: 200),
                        OriginalValue = c.String(),
                        NewValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.UserName, t.EventDate, t.EventType, t.TableName }, name: "IDX_AuditLog")
                .Index(t => t.UserName, name: "IDX_AuditUser")
                .Index(t => t.EventDate, name: "IDX_AuditDate")
                .Index(t => t.EventType, name: "IDX_EventType")
                .Index(t => t.TableName, name: "IDX_AuditTable");
            
            CreateTable(
                "dbo.Biometric",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Status = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        MaxValue = c.Decimal(precision: 18, scale: 2),
                        MinValue = c.Decimal(precision: 18, scale: 2),
                        TriggerAlert = c.Boolean(nullable: false),
                        BiometricType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IDX_BiometricName");
            
            CreateTable(
                "dbo.PatientBiometric",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PatientId = c.Long(nullable: false),
                        BiometricId = c.Long(nullable: false),
                        NumericValue = c.Decimal(precision: 18, scale: 2),
                        StringValue = c.String(),
                        YesNoValue = c.Boolean(),
                        LastUpdateDate = c.DateTime(nullable: false),
                        NotificationSentDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Biometric", t => t.BiometricId)
                .ForeignKey("dbo.Patient", t => t.PatientId)
                .Index(t => t.PatientId)
                .Index(t => t.BiometricId);
            
            CreateTable(
                "dbo.Patient",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserIdentityId = c.Long(nullable: false),
                        DateOfBirth = c.DateTime(nullable: false),
                        IdType = c.Int(nullable: false),
                        BloodType = c.Int(nullable: false),
                        Allergies = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserIdentity", t => t.UserIdentityId)
                .Index(t => t.UserIdentityId, unique: true, name: "IDX_PatientUserIdentityId");
            
            CreateTable(
                "dbo.ChatMessage",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DoctorId = c.Long(nullable: false),
                        PatientId = c.Long(nullable: false),
                        ChatId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chat", t => t.ChatId)
                .ForeignKey("dbo.Doctor", t => t.DoctorId)
                .ForeignKey("dbo.Patient", t => t.PatientId)
                .Index(t => t.DoctorId)
                .Index(t => t.PatientId)
                .Index(t => t.ChatId);
            
            CreateTable(
                "dbo.Chat",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Subject = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Doctor",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserIdentityId = c.Long(nullable: false),
                        IdType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserIdentity", t => t.UserIdentityId)
                .Index(t => t.UserIdentityId, unique: true, name: "IDX_DoctorUserIdentityId");
            
            CreateTable(
                "dbo.UserIdentity",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 50),
                        Active = c.Boolean(nullable: false),
                        Title = c.String(nullable: false, maxLength: 50),
                        FirstName = c.String(nullable: false, maxLength: 200),
                        Surname = c.String(nullable: false, maxLength: 200),
                        IdPassportNum = c.String(nullable: false, maxLength: 200),
                        Gender = c.Int(nullable: false),
                        PasswordHash = c.String(nullable: false, maxLength: 200),
                        Telephone = c.String(nullable: false, maxLength: 20),
                        EmailAddress = c.String(nullable: false, maxLength: 200),
                        AddressLine1 = c.String(maxLength: 200),
                        AddressLine2 = c.String(maxLength: 200),
                        City = c.String(maxLength: 200),
                        PostalCode = c.String(maxLength: 50),
                        FailedLoginAttempts = c.Int(nullable: false),
                        LockedOut = c.DateTime(),
                        Deactivated = c.DateTime(),
                        LastLogin = c.DateTime(),
                        IsSystemAdmin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "IDX_UserName");
            
            CreateTable(
                "dbo.Device",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserIdentityId = c.Long(nullable: false),
                        DeviceId = c.String(),
                        DeviceType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserIdentity", t => t.UserIdentityId)
                .Index(t => t.UserIdentityId);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RoleName = c.String(nullable: false, maxLength: 100),
                        Description = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Privilege",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Security = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Security, unique: true, name: "IDX_PrivilegeSecurity");
            
            CreateTable(
                "dbo.SignalRConnection",
                c => new
                    {
                        ConnectionId = c.String(nullable: false, maxLength: 200),
                        UserIdentityId = c.Long(nullable: false),
                        UserAgent = c.String(maxLength: 200),
                        LastUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ConnectionId)
                .ForeignKey("dbo.UserIdentity", t => t.UserIdentityId)
                .Index(t => t.UserIdentityId);
            
            CreateTable(
                "dbo.SystemLogs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        EventTime = c.DateTime(nullable: false),
                        Sender = c.String(),
                        UserIdentityId = c.Long(),
                        EventType = c.Int(nullable: false),
                        Message = c.String(),
                        StackTrace = c.String(),
                        InnerException = c.String(),
                        InnerExceptionStackTrace = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RolePrivilege",
                c => new
                    {
                        RoleId = c.Long(nullable: false),
                        PrivilegeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoleId, t.PrivilegeId })
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Privilege", t => t.PrivilegeId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.PrivilegeId);
            
            CreateTable(
                "dbo.UserIdentityRole",
                c => new
                    {
                        UserIdentityId = c.Long(nullable: false),
                        RoleId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserIdentityId, t.RoleId })
                .ForeignKey("dbo.UserIdentity", t => t.UserIdentityId, cascadeDelete: true)
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserIdentityId)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PatientBiometric", "PatientId", "dbo.Patient");
            DropForeignKey("dbo.ChatMessage", "PatientId", "dbo.Patient");
            DropForeignKey("dbo.SignalRConnection", "UserIdentityId", "dbo.UserIdentity");
            DropForeignKey("dbo.UserIdentityRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.UserIdentityRole", "UserIdentityId", "dbo.UserIdentity");
            DropForeignKey("dbo.RolePrivilege", "PrivilegeId", "dbo.Privilege");
            DropForeignKey("dbo.RolePrivilege", "RoleId", "dbo.Role");
            DropForeignKey("dbo.Patient", "UserIdentityId", "dbo.UserIdentity");
            DropForeignKey("dbo.Doctor", "UserIdentityId", "dbo.UserIdentity");
            DropForeignKey("dbo.Device", "UserIdentityId", "dbo.UserIdentity");
            DropForeignKey("dbo.ChatMessage", "DoctorId", "dbo.Doctor");
            DropForeignKey("dbo.ChatMessage", "ChatId", "dbo.Chat");
            DropForeignKey("dbo.PatientBiometric", "BiometricId", "dbo.Biometric");
            DropIndex("dbo.UserIdentityRole", new[] { "RoleId" });
            DropIndex("dbo.UserIdentityRole", new[] { "UserIdentityId" });
            DropIndex("dbo.RolePrivilege", new[] { "PrivilegeId" });
            DropIndex("dbo.RolePrivilege", new[] { "RoleId" });
            DropIndex("dbo.SignalRConnection", new[] { "UserIdentityId" });
            DropIndex("dbo.Privilege", "IDX_PrivilegeSecurity");
            DropIndex("dbo.Device", new[] { "UserIdentityId" });
            DropIndex("dbo.UserIdentity", "IDX_UserName");
            DropIndex("dbo.Doctor", "IDX_DoctorUserIdentityId");
            DropIndex("dbo.ChatMessage", new[] { "ChatId" });
            DropIndex("dbo.ChatMessage", new[] { "PatientId" });
            DropIndex("dbo.ChatMessage", new[] { "DoctorId" });
            DropIndex("dbo.Patient", "IDX_PatientUserIdentityId");
            DropIndex("dbo.PatientBiometric", new[] { "BiometricId" });
            DropIndex("dbo.PatientBiometric", new[] { "PatientId" });
            DropIndex("dbo.Biometric", "IDX_BiometricName");
            DropIndex("dbo.AuditLog", "IDX_AuditTable");
            DropIndex("dbo.AuditLog", "IDX_EventType");
            DropIndex("dbo.AuditLog", "IDX_AuditDate");
            DropIndex("dbo.AuditLog", "IDX_AuditUser");
            DropIndex("dbo.AuditLog", "IDX_AuditLog");
            DropTable("dbo.UserIdentityRole");
            DropTable("dbo.RolePrivilege");
            DropTable("dbo.SystemLogs");
            DropTable("dbo.SignalRConnection");
            DropTable("dbo.Privilege");
            DropTable("dbo.Role");
            DropTable("dbo.Device");
            DropTable("dbo.UserIdentity");
            DropTable("dbo.Doctor");
            DropTable("dbo.Chat");
            DropTable("dbo.ChatMessage");
            DropTable("dbo.Patient");
            DropTable("dbo.PatientBiometric");
            DropTable("dbo.Biometric");
            DropTable("dbo.AuditLog");
        }
    }
}
