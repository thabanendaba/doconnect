namespace DoConnect.BL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CleanupPatientStructure : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Patient", "DateOfBirth");
            DropColumn("dbo.Patient", "IdType");
            DropColumn("dbo.Patient", "BloodType");
            DropColumn("dbo.Patient", "Allergies");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Patient", "Allergies", c => c.String());
            AddColumn("dbo.Patient", "BloodType", c => c.Int(nullable: false));
            AddColumn("dbo.Patient", "IdType", c => c.Int(nullable: false));
            AddColumn("dbo.Patient", "DateOfBirth", c => c.DateTime(nullable: false));
        }
    }
}
