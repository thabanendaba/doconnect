namespace DoConnect.BL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPanicData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PanicHistory",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserIdentityId = c.Long(nullable: false),
                        SendDateTime = c.DateTime(nullable: false),
                        Location = c.Geography(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserIdentity", t => t.UserIdentityId)
                .Index(t => t.UserIdentityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PanicHistory", "UserIdentityId", "dbo.UserIdentity");
            DropIndex("dbo.PanicHistory", new[] { "UserIdentityId" });
            DropTable("dbo.PanicHistory");
        }
    }
}
