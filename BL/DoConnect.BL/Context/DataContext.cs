﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EXO.Lib.BL;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using EXO.Lib.Utility;
using System.Reflection;
using DoConnect.BL.Entities.Announcement;
using DoConnect.BL.Entities.Appointment;
using DoConnect.BL.Entities.ChatData;
using DoConnect.BL.Entities.DoctorData;
using DoConnect.BL.Entities.Inventory;
using DoConnect.BL.Entities.Logging;
using DoConnect.BL.Entities.MasterData;
using DoConnect.BL.Entities.PanicData;
using DoConnect.BL.Entities.PatientData;
using DoConnect.BL.Entities.QueueData;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Migrations;
using EXO.Lib.SQL;

namespace DoConnect.BL.Context
{
    public class DataContext : DbContext, IAuditDBContext<PrivilegeType>
    {
        #region Ctor

        public DataContext()
            : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
        }

        public DataContext(DbConnection connection)
            : base(connection, true)
        {

        }

        public static void Setup()
        {
            var connection = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"];
            using (DbConnection setupCon = new SqlConnection(connection.ConnectionString))
            {
                DataContext.Setup(setupCon);
            }
        }

        public static void Setup(DbConnection dbConnection, bool initDefaultData = true)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, Configuration>());
            using (var context = new DataContext(dbConnection))
            {
                try
                {
                    context.Database.Initialize(true);
                }
                catch (System.InvalidOperationException ex)
                {
                    throw ex;
                }
                catch (System.Data.Entity.ModelConfiguration.ModelValidationException ex)
                {
                    throw ex;
                }


                List<Privilege> privileges = context.PrivilegeSet.ToList();
                foreach (PrivilegeType ptype in Enum.GetValues(typeof(PrivilegeType)))
                {
                    var curr = privileges.Where(a => a.Security == ptype).SingleOrDefault();
                    if (curr == null)
                    {
                        curr = new Privilege()
                        {
                            Description = NameSplitting.SplitCamelCase(ptype),
                            Security = ptype
                        };
                        context.PrivilegeSet.Add(curr);
                    }
                }
                context.SaveChanges();

                context.Database.ExecuteSqlCommand("delete from signalrconnection");
            }

        }

        #endregion

        #region DBSets


        public DbSet<Role> RoleSet { get; set; }
        public DbSet<Privilege> PrivilegeSet { get; set; }
        public DbSet<UserIdentity> UserIdentitySet { get; set; }
        public DbSet<AuditLog> AuditLogSet { get; set; }
        public DbSet<Patient> PatientSet { get; set; }
        public DbSet<Biometric> BiometricSet { get; set; }
        public DbSet<PatientBiometric> PatientBiometricSet { get; set; }
        public DbSet<Doctor> DoctorSet { get; set; }
        public DbSet<SystemLog> SystemLogSet { get; set; }
        public DbSet<SignalRConnection> SignalRConnectionSet { get; set; }
        public DbSet<ChatHistory> ChatHistorySet { get; set; }
        public DbSet<PanicHistory> PanicHistory { get; set; }
        public DbSet<Announcement> AnnounceMent { get; set; }
        public DbSet<Inventory> Inventory { get; set; }
        public DbSet<Appointment> Appointment { get; set; }
        public DbSet<Queue> Queue { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
     //       modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();


            modelBuilder.Entity<Role>()
                         .HasMany(x => x.Privileges)
                         .WithMany(a => a.Roles)
                         .Map(x =>
                         {
                             x.ToTable("RolePrivilege");
                             x.MapLeftKey("RoleId");
                             x.MapRightKey("PrivilegeId");
                         });


            modelBuilder.Entity<UserIdentity>()
                         .HasMany(x => x.Roles)
                         .WithMany(a => a.UserIdentities)
                         .Map(x =>
                         {
                             x.ToTable("UserIdentityRole");
                             x.MapLeftKey("UserIdentityId");
                             x.MapRightKey("RoleId");
                         });

        }

        public void AddSystemLogEntry(object sender, Guid guid, long? currentUserId,
            LogEventType logEventType, string message,
            string stackTrace = null, string innerExceptionMessage = null, string innerExceptionStackTrace = null)
        {
            try
            {
                //create a fresh context because the current context is in an error state
                using (var dbContext = new DataContext())
                {
                    var syslogEntry = new SystemLog();
                    dbContext.SystemLogSet.Add(syslogEntry);

                    syslogEntry.EventTime = DateTime.Now;
                    syslogEntry.Sender = sender.ToString();
                    syslogEntry.Id = guid;
                    syslogEntry.UserIdentityId = currentUserId;
                    syslogEntry.EventType = logEventType;
                    syslogEntry.Message = message;
                    syslogEntry.StackTrace = stackTrace;
                    syslogEntry.InnerException = innerExceptionMessage;
                    syslogEntry.InnerExceptionStackTrace = innerExceptionStackTrace;

                    dbContext.SaveChanges();
                }
            }
            catch
            {
                //empty exception never allowed!
                //in this case we are unable to hit the db to log an exception
                //therefore we do not want to change the actual error that happend in the handler
            }
        }


        #region Audit


        public int SaveChanges(IUserContext<PrivilegeType> currentUser)
        {
            if (currentUser != null)
                return AuditHandler.SaveChanges<AuditLog>(this, AuditLogSet, currentUser.UserName, currentUser.Id);
            return base.SaveChanges();
        }

        #endregion

        #region Custom Scripts

        public string ScriptPath
        {
            get
            {
                return "DocConnect.BL.Context.SQL";
            }
        }

        public void ResetPatientNotification(long patientId)
        {
            var assembly = Assembly.GetExecutingAssembly();
            string command = SQLUtilities.ReadResourceScript(assembly, ScriptPath + ".ResetPatientNotification.sql");
            var parms = new SqlParameter[] 
            {
                new SqlParameter("@PatientId", patientId),
            };

            this.Database.ExecuteSqlCommand(command, parms);
        }

        #endregion


   
    }
}
