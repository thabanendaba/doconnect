﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL.Provider.Security;
using DoConnect.BL.Util;
using EXO.Lib.Utility;

namespace DoConnect.BL.Test
{
    public static class SeedData
    {
        #region Security

        public static ICurrentUser CreateAdminUser(DataContext context, string userName = "AdminUser", string password = "password@1")
        {
            var result = CreateUser(context, userName, password);
            var role = CreateRole(context);
            var curr = result.Roles.Where(a => a.RoleName == role.RoleName).SingleOrDefault();
            if (curr == null)
            {
                result.Roles.Add(role);
                context.SaveChanges();
            }
            var qPrivs = from userP in context.UserIdentitySet
                         from r in userP.Roles
                         from privileges in r.Privileges
                         where userP.Id == result.Id
                         select privileges.Security;
            result.AllowedPrivileges = qPrivs.Distinct().ToList();

            var provider = new SecurityProvider(context);
            return provider.UserIdentityToCurrentUser(result);
        }

        public static Role CreateRole(DataContext context, string roleName = "admin", bool hasAllPrivileged = true)
        {
            Role r = context.RoleSet.Include(a => a.Privileges).Where(a => a.RoleName == roleName).SingleOrDefault();
            if (r == null)
            {
                r = new Role()
                {
                    RoleName = roleName,
                    Description = "My Role Description",
                    Privileges = new List<Privilege>()
                };
                context.RoleSet.Add(r);
            }
            if (hasAllPrivileged)
            {
                foreach (PrivilegeType p in Enum.GetValues(typeof(PrivilegeType)))
                {
                    var priv = context.PrivilegeSet.Where(a => a.Security == p).SingleOrDefault();
                    if (priv == null)
                    {
                        priv = new Privilege()
                        {
                            Description = NameSplitting.SplitCamelCase(p),
                            Security = p
                        };
                        context.PrivilegeSet.Add(priv);
                    }
                    var curr = r.Privileges.Where(a => a.Security == priv.Security).SingleOrDefault();
                    if (curr == null)
                        r.Privileges.Add(priv);
                }
            }
            context.SaveChanges();
            return r;
        }

        public static UserIdentity CreateUser(DataContext context, string userName = "James", string password = "Password",
        string title = "Mr", string firstName = "James", string surname = "Mccall", string idNumber = "7005245602074", GenderType gender = GenderType.Male,
        string Telephone = "0723257865", string email = "james@pede.org", string streetName = "3559 Tempor Street", string city = "Gianco", string postalCode = "1899")
        {
            try
            {
                //fetch according to unique index
                var current = context.UserIdentitySet
                                         .Include(a => a.Roles)
                                         .Where(a => a.UserName == userName).SingleOrDefault(); //check against unique index
                if (current == null) //if not found in db 
                {
                    //create instance of entity and insert to table.
                    current = new Entities.SecurityData.UserIdentity();
                    current.Roles = new List<Role>();
                    context.UserIdentitySet.Add(current);
                }

                //set all attributes - this will update if existing, or insert if new
                current.UserName = userName;
                current.Title = title;
                current.FirstName = firstName;
                current.Surname = surname;
                current.IdPassportNum = idNumber;
                current.Gender = gender;
                current.PasswordHash = Cipher.Encrypt(password);
                current.Telephone = Telephone;
                current.EmailAddress = email;
                current.AddressLine1 = streetName;
                current.City = city;
                current.PostalCode = postalCode;
                current.Active = true;

                context.SaveChanges(); //commit changes
                return current;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                throw e;
            }
        }


        #endregion

        //#region Timesheet
        //public static Timesheet CreateTimesheet(DataContext context,long userIdentityId, DateTime startDate, DateTime endDate)
        //{
        //    try
        //    {
        //        var current = context.TimesheetSet.Where(a => a.UserIdentityId == userIdentityId
        //                                                       && a.StartDate == startDate
        //                                                       && a.EndDate == endDate).SingleOrDefault(); //check against unique index
        //        if (current == null) //if not found in db 
        //        {
        //            //create instance of entity and insert to table.
        //            current = new Timesheet();
        //            context.TimesheetSet.Add(current);
        //        }

        //        current.UserIdentityId = userIdentityId;
        //        current.StartDate = startDate;
        //        current.EndDate = endDate;
        //        current.Created = DateTime.Now;

        //        context.SaveChanges();

        //        return current;
        //    }
        //    catch(DbUpdateException e)
        //    {
        //        throw e;
        //    }
        //}

        //public static TimesheetItem CreateTimesheetItem(DataContext context, long timesheetId, long projectId, DateTime itemDate, decimal timeSpent, string taskName)
        //{
        //    var currentTimeSheetItem = context.TimesheetItemSet.Where(a => a.TimesheetId == timesheetId).SingleOrDefault();
        //    if (currentTimeSheetItem ==null)
        //    {
        //        currentTimeSheetItem = new TimesheetItem();
        //        context.TimesheetItemSet.Add(currentTimeSheetItem);
        //    }
        //    currentTimeSheetItem.TimesheetId = timesheetId;
        //    currentTimeSheetItem.ProjectId = projectId;
        //    currentTimeSheetItem.ItemDate = itemDate;
        //    currentTimeSheetItem.TimeSpent = timeSpent;
        //    currentTimeSheetItem.TaskName = taskName;

        //    context.SaveChanges();
        //    return currentTimeSheetItem;
        //}

        //public static Timesheet ApproveTimesheet(DataContext context, long id)
        //{
        //    var currentTimesheet = context.TimesheetSet.Where(x => x.Id == id).SingleOrDefault();
        //    if (currentTimesheet == null)
        //    {
        //        currentTimesheet = new Timesheet();
        //        context.TimesheetSet.Add(currentTimesheet);
        //    }
        //    currentTimesheet.Approved = DateTime.Today;

        //    context.SaveChanges();

        //    return currentTimesheet;
        //}
        //#endregion

        //#region Project and Client
        //public static Client CreateClient(DataContext context, string clientName =  "Game Stores", 
        //    string email = "mail@mail.com", string address = "123 Blair Street", string telephone = "+27820000000",
        //    string city = "Centurion", string postalCode = "0157")
        //{
        //    var currentCreateClient = context.ClientSet.Where(a => a.ClientName == clientName).SingleOrDefault();
        //    if (currentCreateClient == null)
        //    {
        //        currentCreateClient = new Client();
        //        context.ClientSet.Add(currentCreateClient);
        //    }

        //    currentCreateClient.ClientName = clientName;
        //    currentCreateClient.EmailAddress = email;
        //    currentCreateClient.AddressLine1 = address;
        //    currentCreateClient.Status = Types.StatusType.Active;
        //    currentCreateClient.Telephone = telephone;
        //    currentCreateClient.City = city;
        //    currentCreateClient.PostalCode = postalCode;

        //    context.SaveChanges();

        //    return currentCreateClient;
        //}
        
        //public static Project CreateProject(DataContext context, long clientId, string projectName = "My project", string description = "This is a test project", string costcode = "CC01")
        //{
        //    var currentCProject = context.ProjectSet.Where(a => a.ClientId == clientId).SingleOrDefault();
        //    if (currentCProject ==null)
        //    {
        //        currentCProject = new Project();
        //        context.ProjectSet.Add(currentCProject);
        //    }

        //    currentCProject.ClientId = clientId;
        //    currentCProject.ProjectName = projectName;
        //    currentCProject.Description = description;
        //    currentCProject.CostCode = costcode;

        //    context.SaveChanges();
        //    return currentCProject;
        //}

        //public static ProjectTask CreateProjectTask(DataContext context, long projectId, string taskName = "my task", string description = "This task has a default description")
        //{
        //    var currentTask = context.ProjectTaskSet.Where(a => a.ProjectId == projectId && a.TaskName == taskName).SingleOrDefault();
        //    if (currentTask == null)
        //    {
        //        currentTask = new ProjectTask();
        //        context.ProjectTaskSet.Add(currentTask);
        //    }

        //    currentTask.ProjectId = projectId;
        //    currentTask.TaskName = taskName;
        //    currentTask.Description = description;

        //    context.SaveChanges();
        //    return currentTask;
        //}

        //#endregion

        //#region Meeting

        //public static Attendee CreateAttendee(DataContext context,long userId,long meetingId)
        //{
        //    var current = context.AttendeeSet.Where(a => a.UserIdentityId == userId
        //                                        && a.MeetingId==meetingId).SingleOrDefault();
        //    if (current == null)
        //    {
        //        current = new Attendee();
        //        context.AttendeeSet.Add(current);
        //    }
        //    current.UserIdentityId = userId;
        //    current.MeetingId = meetingId;

        //    context.SaveChanges();
        //    return current;
        //}

        //public static AgendaItem CreateAgenda(DataContext context, long meetingId, string subject = "Some Subject")
        //{
        //    var current = context.AgendaItemSet.Where(a => a.MeetingId == meetingId && a.Subject == subject).SingleOrDefault();
        //    if (current == null)
        //    {
        //        current = new AgendaItem();
        //        context.AgendaItemSet.Add(current);
        //    }
        //    current.MeetingId = meetingId;
        //    current.Subject = subject;

        //    context.SaveChanges();
        //    return current;
        //}

        //public static MinuteItem CreateMinute(DataContext context,long agendaId, string comments = "We have the comments")
        //{
        //    var current = new MinuteItem();
        //    context.MinuteItemSet.Add(current);

        //    current.AgendaId = agendaId;
        //    current.Comments = comments;

        //    context.SaveChanges();

        //    return current;
        //}

        //public static Meeting CreateMeeting(DataContext context,long userIdentityId, string venue, DateTime startDate,DateTime endDate, string subject = "Some Subject")
        //{
        //    var current = context.MeetingSet.Where(a => a.Venue == venue
        //                                            && a.StartDateTime == startDate
        //                                            && a.EndDateTime == endDate).SingleOrDefault();
        //    if (current ==null)
        //    {
        //        current = new Meeting();
        //        context.MeetingSet.Add(current);
        //    }

        //    current.Venue = venue;
        //    current.StartDateTime = startDate;
        //    current.EndDateTime = endDate;
        //    current.Subject = subject;
        //    current.UserIdentityId = userIdentityId;

        //    context.SaveChanges();
        //    return current;
        //}

        //#endregion

       // #region Leave

        //public static LeaveItem CreateLeaveItem(DataContext context,long userId,long leaveTypeId,DateTime LeaveStartDate,DateTime endDate,string reasonForLeave)
        //{
        //    var current = context.LeaveItemSet.Where(a => a.UserIdentityId == userId && a.StartDate == LeaveStartDate).SingleOrDefault();
        //                                                 //a=>a.StartDate == LeaveStartDate)
        //    try
        //    {
        //        if (current == null)
        //        {
        //         current = new LeaveItem();
        //        context.LeaveItemSet.Add(current);
        //         }

        //        current.StartDate = LeaveStartDate;
        //        current.UserIdentityId = userId;
        //        current.LeaveTypeId = leaveTypeId;
        //        current.EndDate = endDate;
        //        current.ReasonForLeave = reasonForLeave;

        //        context.SaveChanges();
        //        return current;
        //    }
        //    catch(System.Data.Entity.Infrastructure.DbUpdateException ex)
        //    {
        //        throw ex;
        //    }
        //}
     
        //public static LeaveType CreateLeaveType(DataContext context, string leaveTypeCode,string description)
        //{
        //    var current = context.LeaveTypeSet.Where(a => a.Code == leaveTypeCode).SingleOrDefault();

        //    if (current == null)
        //    {
        //      current = new LeaveType();
        //        context.LeaveTypeSet.Add(current);
        //    }
        //        current.Code = leaveTypeCode;
        //        current.Description = description;

        //    context.SaveChanges();
        //    return current;
        //}
     
        //public static LeaveAttachment CreateLeaveAttachment(DataContext context,long leaveItemId,string fileName)
        //{
        //    var current = context.LeaveAttachmentSet.Where(a => a.LeaveItemId == leaveItemId && a.FileName == fileName).SingleOrDefault();

        //    if (current == null)
        //    {
        //        current = new LeaveAttachment();
        //        context.LeaveAttachmentSet.Add(current);
        //    }
        //        current.LeaveItemId = leaveItemId;
        //        current.FileName = fileName;
        //        current.FileData = new byte[] { 1, 2, 3, 4 };

        //    context.SaveChanges();
        //    return current;
        //}

        //#endregion

        public static List<Privilege> GetPrivileges(DataContext context)
        {
            var p = context.PrivilegeSet.OrderBy(x => x.Id).Take(2);

            return p.ToList();
        }

        public static List<long> GetRoles(DataContext context)
        {
            var r = context.RoleSet.OrderBy(a => a.Id).Select(x=>x.Id).Take(2);

            return r.ToList();
        }
    }
}
