﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Context;
using DoConnect.BL.Entities.SecurityData;
using DoConnect.BL;


namespace DoConnect.BL.Test
{
    /*
     * This class contains 1 running test method.
     * The execution of this method will be slow as it attempts to create and seed a database with allot of data in it
     * this data can be used for report development.
     * 
     * I have commented out the [TestMethod] attribute for quick testing.
     
     */
    [TestClass]
    public class ContextTest : ProviderTestBase
    {
        [TestMethod]
        [TestCategory("Utilities")]
        public void TestCreateDB()
        {
            DataContext.Setup();
        }

      //  [TestMethod]
        [TestCategory("Utilities")]
        public void SeedRandomDB()
        {
            using (var ctx = new DataContext())
            {
                //List<Project> myProjectList = new List<Project>();
                for (int clientCount = 1; clientCount <= 10; clientCount++)
                {
                    //var myClient = SeedData.CreateClient(ctx, "Client" + clientCount.ToString(),
                       // "client" + clientCount.ToString() + "@mail.com", clientCount.ToString() + " Tempor Street", "+277200000" + clientCount.ToString().PadLeft(2, '0'));
                    for (int projectCount = 1; projectCount <= 10; projectCount++)
                    {
                      //  Project myProject = SeedData.CreateProject(ctx, myClient.Id, "Project " + projectCount.ToString(),"This is the description for project " + projectCount.ToString(),"CC"+projectCount.ToString());
                      //  myProjectList.Add(myProject);
                    }
                }
                //create 10 clients
                //create 10 projects
                //List<Attendee> myAttendeeList = new List<Attendee>();
                List<UserIdentity> myUsers = new List<UserIdentity>();

                for (int userCount = 1; userCount <= 10; userCount++)
                {
                    string userName = "User_" + userCount.ToString();
                    var myUser = SeedData.CreateUser(ctx, userName,"password", "Mr", userName,userName, userCount.ToString(), GenderType.Male, "Password" + userCount.ToString(), "+277200000" + userCount.ToString().PadLeft(2, '0'),userName+"@mail.com",userCount.ToString() + " Tempor Street");
                    myUsers.Add(myUser);
                    var startDate = new DateTime(2015, 05, 05);
                    var endDate = new DateTime(2015, 05, 11);


                    for (int timesheetCount = 1; timesheetCount <= 50; timesheetCount++)
                    {
                        startDate = startDate.AddDays(7);
                        endDate = endDate.AddDays(7);

                       // var myTimesheet = SeedData.CreateTimesheet(ctx, myUser.Id, startDate, endDate);


                        for (int timesheetItemCount = 0; timesheetItemCount <= 4; timesheetItemCount++)
                        {
                            //int randomProjectNumber = new Random().Next(0, myProjectList.Count - 1);
                            //SeedData.CreateTimesheetItem(ctx, myTimesheet.Id, myProjectList[randomProjectNumber].Id, startDate.AddDays(timesheetCount), 8, "Task for day " + startDate.AddDays(timesheetCount).ToString("ddd"));
                            //now create timesheet items for myTimesheet
                        }
                    }
                }
               // List<Meeting> myMeetingList = new List<Meeting>();
                for (int meetingcount = 0; meetingcount <= 10; meetingcount++)
                {
                    int randomCreator = new Random().Next(0, myUsers.Count - 1);
                    var createorUser = myUsers[randomCreator];

                    var venue = "Meeting Room Number " + meetingcount.ToString();
                    var date = new DateTime(2015, 05, 29);
                    //var myMeeting = SeedData.CreateMeeting(ctx,createorUser.Id, venue, date,date, "Meeting " + meetingcount.ToString());

                    //myMeetingList.Add(myMeeting);

                    //for (int attendee = 0; attendee <= 5; attendee++)
                    //{
                    //    int randomUser = new Random().Next(0, myUsers.Count - 1);
                    //    var userId = myUsers[randomUser];

                    //    var myAttendee = SeedData.CreateAttendee(ctx, userId.Id, myMeeting.Id);

                    //    for (int agendaCount = 0; agendaCount <= 10; agendaCount++)
                    //    {
                    //        var myAgenda = SeedData.CreateAgenda(ctx, myMeeting.Id, "Agenda Item Number " + agendaCount.ToString());

                    //        for (int minutesAgenda = 0; minutesAgenda <= 10; minutesAgenda++)
                    //        {
                    //            var myMinutes = SeedData.CreateMinute(ctx, myAgenda.Id, "Minute for agenda number " + minutesAgenda.ToString());
                    //        }
                    //    }
                    //}
                }
                //List<LeaveItem> myLeave = new List<LeaveItem>();
                //List<LeaveType>myLeaveTypeID = new List<LeaveType>();
                //var description = "The Leave Description ";
                //var myLeaveType = SeedData.CreateLeaveType(ctx, "LTC",description);
                //for (int leaveItem = 0; leaveItem <=10 ; leaveItem++)
                //{
                //    var date = new DateTime(2015,12,23);
                //    var endate = new DateTime(2016, 01, 05);
                //    var reasonToleave = "The reason ";

                //    int randomUser = new Random().Next(0, myUsers.Count - 1);
                //    var userId = myUsers[randomUser];
                //    var myLeaveItem = SeedData.CreateLeaveItem(ctx, userId.Id, myLeaveType.Id, date, endate, reasonToleave);
                //   // myLeave.Add(myLeaveItem);

                //    for (int leaveTypeCount = 0; leaveTypeCount <=10 ; leaveTypeCount++)
                //    {
                      
                //        //myLeaveTypeID.Add(myLeaveType);

                //        for (int leaveAttachmentCount = 0; leaveAttachmentCount <=10 ; leaveAttachmentCount++)
                //        {
                            
                //           // int randomLeaveAttachment = new Random().Next(0,myLeave.Count -1);
                //           // var leaveItemId = myLeave[randomLeaveAttachment];
                //           // var myLeaveAttachment = SeedData.CreateLeaveAttachment(ctx, leaveItemId.Id, "my File");
                //        }
                //    }
                //}
            }
        }

        //private void CreateUserAndTimesheets(DataContext ctx, UserIdentity user)
        //{
        //    user = SeedData.CreateUser(ctx);

        //    //List<Timesheet> ts = new List<Timesheet>
        //    //{
        //    //    new Timesheet{UserIdentityId=user.Id, StartDate=new DateTime(2015,05,05),EndDate=new DateTime(2015,05,11)},
        //    //    new Timesheet{UserIdentityId=user.Id, StartDate=new DateTime(2015,05,12),EndDate=new DateTime(2015,05,17)}

        //    //};

        //    var ts = SeedData.CreateTimesheet(ctx, user.Id, new DateTime(2015, 05, 26), new DateTime(2015, 05, 26));


        //    //user = SeedData.CreateUser(ctx);
        //   // var user1 = SeedData.CreateUser(ctx,);
        //    //var user2 = SeedData.CreateUser(ctx);
        //    //var user3 = SeedData.CreateUser(ctx);
        //    //var user4 = SeedData.CreateUser(ctx);
        //    //var user5 = SeedData.CreateUser(ctx);
        //    //var user6 = SeedData.CreateUser(ctx);
        //    //var user7 = SeedData.CreateUser(ctx);
        //    //var user8 = SeedData.CreateUser(ctx);
        //    //var user9 = SeedData.CreateUser(ctx);
        //    //var user10 = SeedData.CreateUser(ctx);

        //    //var timesheet = new Timesheet();
        //    //var obj = SeedData.CreateTimesheet(ctx, user1.Id, new DateTime(2015, 04, 27), new DateTime(2015, 05, 04));

        //    //List<Timesheet> ts = new List<Timesheet>();

        //    // ts.Add(new SeedData.CreateTimesheet(){ctx, user1.Id, new DateTime(2015, 05, 04), new DateTime(2015, 05, 10)});
        //    //     //var ts1 = SeedData.CreateTimesheet(ctx, user1.Id, new DateTime(2015, 05, 04), new DateTime(2015, 05, 10));

        //    //foreach(SeedData.CreateTimesheet() ct in ts)
        //    //    ctx.TimesheetSet.Add(ct);

        //    //var ts1 = SeedData.CreateTimesheet(ctx, user1.Id, new DateTime(2015, 05, 01), new DateTime(2015, 05, 10));
        //    //var ts2 = SeedData.CreateTimesheet(ctx, user1.Id, new DateTime(2015, 05, 02), new DateTime(2015, 05, 10));
        //    //var ts3 = SeedData.CreateTimesheet(ctx, user2.Id, new DateTime(2015, 05, 03), new DateTime(2015, 05, 17));
        //    //var ts4 = SeedData.CreateTimesheet(ctx, user2.Id, new DateTime(2015, 05, 04), new DateTime(2015, 05, 17));

        //    //var ts5 = SeedData.CreateTimesheet(ctx, user.Id, new DateTime(2015, 05, 05), new DateTime(2015, 05, 10));
        //    //var ts6 = SeedData.CreateTimesheet(ctx, user1.Id, new DateTime(2015, 05, 06), new DateTime(2015, 05, 10));
        //    //var ts7 = SeedData.CreateTimesheet(ctx, user2.Id, new DateTime(2015, 05, 07), new DateTime(2015, 05, 17));
        //    //var ts8 = SeedData.CreateTimesheet(ctx, user2.Id, new DateTime(2015, 05, 08), new DateTime(2015, 05, 17));
        //    //List<Timesheet> ts = new List<Timesheet>
        //    //{
        //    //    new Timesheet{UserIdentityId=user1.Id, StartDate=new DateTime(2015,05,05),EndDate=new DateTime(2015,05,11)},
        //    //    new Timesheet{UserIdentityId=user1.Id, StartDate=new DateTime(2015,05,12),EndDate=new DateTime(2015,05,17)}

        //    //};
        //    ////var ts9 = SeedData.CreateTimesheet(ctx, user2.Id, new DateTime(2015, 05, 09), new DateTime(2015, 05, 17));
        //    //foreach (var item in ts)
        //    //    ctx.TimesheetSet.Add(item);

        //    //var ts10 = SeedData.CreateTimesheet(ctx, ts.FirstOrDefault().Id, ts.FirstOrDefault().EndDate, ts.FirstOrDefault().EndDate);

        //    //var ts1 = new[]
        //    //{
        //    //   // new timesheet {ctx, user1.Id, new DateTime(2015, 04, 27), new DateTime(2015, 05, 04)},
        //    //   new SeedData.CreateTimesheet(new DateTime(2015, 04, 27), new DateTime(2015, 05, 04))
        //    //    //new SeedData{ctx, user1.Id, new DateTime(2015, 05, 11), new DateTime(2015, 05, 17)},
        //    //    //new SeedData.CreateTimesheets{ctx, user1.Id, new DateTime(2015, 05, 18), new DateTime(2015, 05, 24)},
        //    //    //new SeedData.CreateTimesheet{ctx, user1.Id, new DateTime(2015, 05, 25), new DateTime(2015, 06, 01)}
        //    //};
        //    //timesheet.Add(new SeedData.CreateTimesheet(){ctx, user1.Id, new DateTime(2015, 05, 11), new DateTime(2015, 05, 17)});

        //    //foreach (timesheet ts in ts1)
        //    //    ctx.TimesheetSet.Add(ts1);

        //    //var ts2 = SeedData.CreateTimesheet(ctx, user1.Id, new DateTime(2015, 04, 27), new DateTime(2015, 05, 04));
        //    //var ts3 = SeedData.CreateTimesheet(ctx, user1.Id, new DateTime(2015, 05, 11), new DateTime(2015, 05, 17));
        //    //var ts4 = SeedData.CreateTimesheet(ctx, user1.Id, new DateTime(2015, 05, 18), new DateTime(2015, 05, 24));
        //    //var ts5 = SeedData.CreateTimesheet(ctx, user1.Id, new DateTime(2015, 05, 25), new DateTime(2015, 06, 01));

        //}
    }
}
