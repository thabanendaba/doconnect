﻿using DoConnect.BL.Entities.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoConnect.BL.Provider.DoctorData;
using SoftwareApproach.TestingExtensions;

namespace DoConnect.BL.Test.Providers
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class DoctorProviderTest:ProviderTestBase
    {
        [TestMethod]
        [TestCategory("Provider.Doctor")]
        public void SaveDoctor()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Lawrence");

            IDoctorProvider provider = new DoctorProvider(Context, accountUser);

            var doctor = provider.SaveDoctor(null, anotherUser.Id);

            doctor.ShouldNotBeNull();
            doctor.Id.ShouldBeGreaterThan(0);
        }

        [TestMethod]
        [TestCategory("Provider.Doctor")]
        public void ArchiveDoctor()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "James");

            IDoctorProvider provider = new DoctorProvider(Context, accountUser);

            var doctor = provider.SaveDoctor(null, anotherUser.Id);

            provider.ArchiveDoctor(doctor.Id);

            var testDoctor = provider.GetDoctor().Where(a => a.Id == doctor.Id).Single();
            testDoctor.Id.ShouldBeGreaterThan(0);
        }

        [TestMethod]
        [TestCategory("Provider.Doctor")]
        public void DoctorListTest()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Chris");
            var user = SeedData.CreateAdminUser(Context, "Mtho");

            IDoctorProvider provider = new DoctorProvider(Context, accountUser);

            var doctor = provider.SaveDoctor(null, user.Id);
            var doctor1 = provider.SaveDoctor(null, anotherUser.Id);

            var x = provider.GetDoctor().Count();
            x.ShouldEqual(2);
        }
    }
}
