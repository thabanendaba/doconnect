﻿using DoConnect.BL.Provider;
using DoConnect.BL.Provider.PatientData;
using DoConnect.BL.Test;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftwareApproach.TestingExtensions;
using DoConnect.BL.Entities.Types;
using DoConnect.BL.Provider.MasterData;
using DoConnect.BL.Entities.MasterData;

namespace DocConnect.BL.Test.Providers
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class PatientProviderTest : ProviderTestBase
    {
        #region Patient
        [TestMethod]
        [TestCategory("Provider.Patient")]
        public void SavePatient()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Lawrence");
            IPatientProvider provider = new PatientProvider(Context,accountUser);

            var patient = provider.SavePatient(null, anotherUser.Id);

            patient.ShouldNotBeNull();
            patient.Id.ShouldBeGreaterThan(0);
        }

        [TestMethod]
        [TestCategory("Provider.Patient")]
        public void ArchievePatient()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "James");

            IPatientProvider provider = new PatientProvider(Context, accountUser);

            var patient = provider.SavePatient(null, anotherUser.Id);

            provider.ArchivePatient(patient.Id);

            var testPatient = provider.GetPatient().Where(a => a.Id == patient.Id).Single();
            testPatient.Id.ShouldBeGreaterThan(0);
        }

        [TestMethod]
        [TestCategory("Provider.Patient")]
        public void PatientListTest()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Chris");
            var user = SeedData.CreateAdminUser(Context, "Mtho");

            IPatientProvider provider = new PatientProvider(Context, accountUser);

            var patient1 = provider.SavePatient(null, anotherUser.Id);
            var patient2 = provider.SavePatient(null, user.Id);

            var x = provider.GetPatient().Count();

            x.ShouldEqual(2);
        }
#endregion

        #region Biometric
        [TestMethod]
        [TestCategory("Provider.Biometric")]
        public void SaveBiometric()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            //IPatientProvider provider = new PatientProvider(Context,accountUser);
            IMasterDataProvider biometricProvider = new MasterDataProvider(Context, accountUser);

            //var patient = provider.SavePatient(null, "jbekker@joebekkker.me", "Joe", "Bekker", "12-05-1979", DocConnect.BL.Entities.SecurityData.GenderType.Male, DocConnect.BL.Entities.Patient.IdType.RSAIDType, "27728523698", DocConnect.BL.Entities.Patient.BloodType.TypeO, "None");
            var biometric = biometricProvider.SaveBiometric(null, "Blood Type", 120, 80, true, BiometricType.NumericValue);

        }

        [TestMethod]
        [TestCategory("Provider.Biometric")]
        public void ArchieveBiometric()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            IMasterDataProvider provider = new MasterDataProvider(Context, accountUser);

            var biometric = provider.SaveBiometric(null, "Weight", 100, 50, true, BiometricType.NumericValue);

            provider.ArchieveBiometric(biometric.Id);

            var testBiometric = provider.GetBiometric().Where(a => a.Id == biometric.Id).Single();
            testBiometric.Id.ShouldBeGreaterThan(0);
        }

        [TestMethod]
        [TestCategory("Provider.Biometric")]
        public void TestBiometric()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            IMasterDataProvider provider = new MasterDataProvider(Context, accountUser);

            var biometric1 = provider.SaveBiometric(null, "Blood Pressure", 120, 80, true, BiometricType.NumericValue);
            var biometric2 = provider.SaveBiometric(null, "Cholesterol", 0, 0, true, BiometricType.NumericValue);

            var x = provider.GetBiometric().Count();

            x.ShouldEqual(2);
        }
        #endregion

        #region PatientBiometric

        [TestMethod]
        [TestCategory("Provider.PatientBiometric")]
        public void SavePatientBiometric()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "AdminUser");

            IPatientProvider patientProvider = new PatientProvider(Context, accountUser);
            IMasterDataProvider biometricProvider = new MasterDataProvider(Context, accountUser);
            // IPatientProvider patientBiometricProvider = new PatientProvider(Context, accountUser);

            var patient = patientProvider.SavePatient(null, anotherUser.Id);
            var biometric = biometricProvider.SaveBiometric(null, "Blood Type", 120, 80, true, BiometricType.NumericValue );

            var patientBiometric = patientProvider.SavePatientBiometric(patient.Id, biometric.Id, null, "heartAttack", null);
        }

        [TestMethod]
        [TestCategory("Provider.PatientBiometric")]
        public void ArchievePatientBiometric()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "AdminUser");

            IPatientProvider patientProvider = new PatientProvider(Context, accountUser);
            IMasterDataProvider biometricProvider = new MasterDataProvider(Context, accountUser);
            //IPatientProvider patientBiometricProvider = new PatientProvider(Context, accountUser);

            var patient = patientProvider.SavePatient(null, anotherUser.Id);
            var biometric = biometricProvider.SaveBiometric(null, "Blood Type", 120, 80, true, BiometricType.NumericValue);

            var patientBiometric = patientProvider.SavePatientBiometric(patient.Id, biometric.Id, null ,"heartAttack", null);
        }

        [TestMethod]
        [TestCategory("Provider.PatientBiometric")]
        public void PatientBiometricTest()
        {
            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "James");
            var user = SeedData.CreateAdminUser(Context, "AdminUser");

            IPatientProvider provider = new PatientProvider(Context, accountUser);
            //IPatientProvider patientProvider = new PatientProvider(Context, accountUser);
            IMasterDataProvider biometricProvider = new MasterDataProvider(Context, accountUser);

            var patient1 = provider.SavePatient(null, anotherUser.Id);
            var patient2 = provider.SavePatient(null, accountUser.Id);

            var biometric = biometricProvider.SaveBiometric(null, "Blood Type", 120, 80, true, BiometricType.NumericValue);
            var biometric1 = biometricProvider.SaveBiometric(null, "Weight", 100, 50, true, BiometricType.NumericValue);

            var patientBiometric = provider.SavePatientBiometric(patient1.Id, biometric.Id,null, "heartAttack", null);
            var patientBiometric1 = provider.SavePatientBiometric(patient2.Id, biometric1.Id, null, "heartAttack", null);

            var x = provider.GetPatientBiometric().Count();

            x.ShouldEqual(2);
        }
        #endregion
    }
}
