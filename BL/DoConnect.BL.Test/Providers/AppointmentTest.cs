﻿using DoConnect.BL.Entities.Appointment;
using DoConnect.BL.Provider.DoctorData;
using DoConnect.BL.Provider.PatientData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PreSchool.BL.Provider.Schedule;
using SoftwareApproach.TestingExtensions;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoConnect.BL.Test.Providers
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class AppointmentTest:ProviderTestBase
    {
        [TestMethod]
        public void SaveAppointment()
        {

            var accountUser = SeedData.CreateUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Easto");
            IPatientProvider provider = new PatientProvider(Context, anotherUser);
            IDoctorProvider DocProcider = new DoctorProvider(Context, anotherUser);
            IAppointmentProvider AppProvider = new AppointmentProvider(Context, anotherUser);

            var patient = provider.SavePatient(null, accountUser.Id);
            var doctor = DocProcider.SaveDoctor(null, anotherUser.Id);

            Appointment Appointment = new Appointment()
            {
                AppointmentType = AppointmentType.Checkup,
                DateStamp = DateTime.Now,
                DoctorUserIdentityId = doctor.Id,
                From = DateTime.Now.AddDays(1),
                To = DateTime.Now.AddDays(1).AddMinutes(30),
                PatientUserIdentityId = patient.Id,
                Status = AppointmentStatus.Pending,
            };

            AppProvider.SaveAppointment(Appointment);

            Appointment.ShouldNotBeNull();
            Appointment.Status.ShouldEqual(AppointmentStatus.Pending);
        }

        [TestMethod]
        public void UpdateAppointment()
        {

            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Easto");
            IPatientProvider provider = new PatientProvider(Context, accountUser);
            IDoctorProvider DocProcider = new DoctorProvider(Context, anotherUser);
            IAppointmentProvider AppProvider = new AppointmentProvider(Context, accountUser);

            var patient = provider.SavePatient(null, accountUser.Id);
            var doctor = DocProcider.SaveDoctor(null, anotherUser.Id);

            Appointment Appointment = new Appointment()
            {
                AppointmentType = AppointmentType.Checkup,
                DateStamp = DateTime.Now,
                DoctorUserIdentityId = doctor.Id,
                From = DateTime.Now.AddDays(1),
                To = DateTime.Now.AddDays(1).AddMinutes(30),
                PatientUserIdentityId = patient.Id,
                Status = AppointmentStatus.Pending,
            };

            Appointment =  AppProvider.SaveAppointment(Appointment);

            Appointment UpdateAppointment = new Appointment() {
                Id = Appointment.Id,
                To = Appointment.To,
                From = Appointment.From,
                AppointmentType = Appointment.AppointmentType,
                DateStamp = Appointment.DateStamp,
                Doctor = Appointment.Doctor,
                DoctorUserIdentityId = Appointment.DoctorUserIdentityId,
                Duration = Appointment.To - Appointment.From,
                Patient = Appointment.Patient,
                PatientUserIdentityId = Appointment.PatientUserIdentityId,
                Status = Appointment.Status
            };


            UpdateAppointment.From = UpdateAppointment.From.AddHours(1);
            UpdateAppointment.To = UpdateAppointment.To.AddHours(1);
            UpdateAppointment.Status = AppointmentStatus.Approved;

            UpdateAppointment = AppProvider.SaveAppointment(UpdateAppointment);

            UpdateAppointment.ShouldNotBeNull();
            UpdateAppointment.Status.ShouldEqual(AppointmentStatus.Approved);
        }

        [TestMethod]
        public void GetAppointmentById()
        {

            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Easto");
            IPatientProvider provider = new PatientProvider(Context, accountUser);
            IDoctorProvider DocProcider = new DoctorProvider(Context, anotherUser);
            IAppointmentProvider AppProvider = new AppointmentProvider(Context, accountUser);

            var patient = provider.SavePatient(null, accountUser.Id);
            var doctor = DocProcider.SaveDoctor(null, anotherUser.Id);

            Appointment Appointment = new Appointment()
            {
                AppointmentType = AppointmentType.Checkup,
                DateStamp = DateTime.Now,
                DoctorUserIdentityId = doctor.Id,
                From = DateTime.Now.AddDays(1),
                To = DateTime.Now.AddDays(1).AddMinutes(30),
                PatientUserIdentityId = patient.Id,
                Status = AppointmentStatus.Pending,
            };

            Appointment = AppProvider.SaveAppointment(Appointment);

            Appointment appointment = AppProvider.GetAppointmentById(Appointment.Id);

            appointment.ShouldNotBeNull();
            appointment.Status.ShouldEqual(AppointmentStatus.Pending);
        }

        [TestMethod]
        public void GetAppointmentsByDate()
        {

            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Easto");
            IPatientProvider provider = new PatientProvider(Context, accountUser);
            IDoctorProvider DocProcider = new DoctorProvider(Context, anotherUser);
            IAppointmentProvider AppProvider = new AppointmentProvider(Context, accountUser);

            var patient = provider.SavePatient(null, accountUser.Id);
            var doctor = DocProcider.SaveDoctor(null, anotherUser.Id);

            Appointment Appointment = new Appointment()
            {
                AppointmentType = AppointmentType.Checkup,
                DateStamp = DateTime.Now,
                DoctorUserIdentityId = doctor.Id,
                From = DateTime.Now.AddDays(1),
                To = DateTime.Now.AddDays(1).AddMinutes(30),
                PatientUserIdentityId = patient.Id,
                Status = AppointmentStatus.Pending,
            };

            Appointment = AppProvider.SaveAppointment(Appointment);

            List<Appointment> lsAppointments = AppProvider.GetAppointmentsByDate(Appointment.From.Date);

            lsAppointments.ShouldNotBeNull();
        }

        [TestMethod]
        public void GetAppointmentsByMonth()
        {

            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Easto");
            IPatientProvider provider = new PatientProvider(Context, accountUser);
            IDoctorProvider DocProcider = new DoctorProvider(Context, anotherUser);
            IAppointmentProvider AppProvider = new AppointmentProvider(Context, accountUser);

            var patient = provider.SavePatient(null, accountUser.Id);
            var doctor = DocProcider.SaveDoctor(null, anotherUser.Id);

            Appointment Appointment = new Appointment()
            {
                AppointmentType = AppointmentType.Checkup,
                DateStamp = DateTime.Now,
                DoctorUserIdentityId = doctor.Id,
                From = DateTime.Now.AddDays(1),
                To = DateTime.Now.AddDays(1).AddMinutes(30),
                PatientUserIdentityId = patient.Id,
                Status = AppointmentStatus.Pending,
            };

            Appointment = AppProvider.SaveAppointment(Appointment);

            List<Appointment> lsAppointments = AppProvider.GetAppointmentsByMonth(Appointment.From);

            lsAppointments.ShouldNotBeNull();
        }


        [TestMethod]
        public void GetAppointmentsByDateNId()
        {

            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Easto");
            IPatientProvider provider = new PatientProvider(Context, accountUser);
            IDoctorProvider DocProcider = new DoctorProvider(Context, anotherUser);
            IAppointmentProvider AppProvider = new AppointmentProvider(Context, accountUser);

            var patient = provider.SavePatient(null, accountUser.Id);
            var doctor = DocProcider.SaveDoctor(null, anotherUser.Id);

            Appointment Appointment = new Appointment()
            {
                AppointmentType = AppointmentType.Checkup,
                DateStamp = DateTime.Now,
                DoctorUserIdentityId = doctor.Id,
                From = DateTime.Now.AddDays(1),
                To = DateTime.Now.AddDays(1).AddMinutes(30),
                PatientUserIdentityId = patient.Id,
                Status = AppointmentStatus.Pending,
            };

            Appointment = AppProvider.SaveAppointment(Appointment);

            List<Appointment> lsAppointments = AppProvider.GetMyAppointmentsByDate(Appointment.From.Date, patient.Id);

            lsAppointments.ShouldNotBeNull();
        }

        [TestMethod]
        public void GetAppointmentsByMonthNId()
        {

            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Easto");
            IPatientProvider provider = new PatientProvider(Context, accountUser);
            IDoctorProvider DocProcider = new DoctorProvider(Context, anotherUser);
            IAppointmentProvider AppProvider = new AppointmentProvider(Context, accountUser);

            var patient = provider.SavePatient(null, accountUser.Id);
            var doctor = DocProcider.SaveDoctor(null, anotherUser.Id);

            Appointment Appointment = new Appointment()
            {
                AppointmentType = AppointmentType.Checkup,
                DateStamp = DateTime.Now,
                DoctorUserIdentityId = doctor.Id,
                From = DateTime.Now.AddDays(1),
                To = DateTime.Now.AddDays(1).AddMinutes(30),
                PatientUserIdentityId = patient.Id,
                Status = AppointmentStatus.Pending,
            };

            Appointment = AppProvider.SaveAppointment(Appointment);

            List<Appointment> lsAppointments = AppProvider.GetMyAppointmentsByMonth(Appointment.From, patient.Id);

            lsAppointments.ShouldNotBeNull();
        }
        [TestMethod]
        public void GetAppointmenstByStatus()
        {

            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Easto");
            IPatientProvider provider = new PatientProvider(Context, accountUser);
            IDoctorProvider DocProcider = new DoctorProvider(Context, anotherUser);
            IAppointmentProvider AppProvider = new AppointmentProvider(Context, accountUser);

            var patient = provider.SavePatient(null, accountUser.Id);
            var doctor = DocProcider.SaveDoctor(null, anotherUser.Id);

            Appointment Appointment = new Appointment()
            {
                AppointmentType = AppointmentType.Checkup,
                DateStamp = DateTime.Now,
                DoctorUserIdentityId = doctor.Id,
                From = DateTime.Now.AddDays(1),
                To = DateTime.Now.AddDays(1).AddMinutes(30),
                PatientUserIdentityId = patient.Id,
                Status = AppointmentStatus.Pending,
            };

            Appointment = AppProvider.SaveAppointment(Appointment);

            List<Appointment> lsAppointments = AppProvider.GetAppointmentsByStatus(AppointmentStatus.Pending);

            lsAppointments.ShouldNotBeNull();
        }
        [TestMethod]
        public void GetMyAppointments()
        {

            var accountUser = SeedData.CreateAdminUser(Context);
            var anotherUser = SeedData.CreateAdminUser(Context, "Easto");
            IPatientProvider provider = new PatientProvider(Context, accountUser);
            IDoctorProvider DocProcider = new DoctorProvider(Context, anotherUser);
            IAppointmentProvider AppProvider = new AppointmentProvider(Context, accountUser);

            var patient = provider.SavePatient(null, accountUser.Id);
            var doctor = DocProcider.SaveDoctor(null, anotherUser.Id);

            Appointment Appointment = new Appointment()
            {
                AppointmentType = AppointmentType.Checkup,
                DateStamp = DateTime.Now,
                DoctorUserIdentityId = doctor.Id,
                From = DateTime.Now.AddDays(1),
                To = DateTime.Now.AddDays(1).AddMinutes(30),
                PatientUserIdentityId = patient.Id,
                Status = AppointmentStatus.Pending,
            };

            Appointment = AppProvider.SaveAppointment(Appointment);

            List<Appointment> lsAppointments = AppProvider.GetMyAppointments(patient.Id);

            lsAppointments.ShouldNotBeNull();
        }
    }
}
